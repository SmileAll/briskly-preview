---
apps:
  title: Скачайте приложение B-Pay
  google:
    link: https://play.google.com/store/apps/details?id=online.briskly
    image: googleplay.png
  apple:
    link: https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8
    image: appstore.png
contact:
  title: Пишите, звоните, подписывайтесь
  button: Заказать звонок
  phone: 8-800-222-61-09
  phoneAction: tel:+78002226109
socials: 
  -
    link: https://www.tiktok.com/@briskly_house
    icon: '#social-tiktok'
  -
    link: https://t.me/Brskl
    icon: '#social-telegram'
  -
    link: https://vk.com/briskly.online
    icon: '#social-vk'
navigation:
  -
    title: 'Продукты'
    link: null
    icon: null
    dropdown:
      -
        title: 'Микромаркеты'
        desc: 'Умные автономные холодильники'
        icon: 'dropdown-nav-icon-1.png'
        link: '/{lang}/micromarket/'
        target: '_blank'
      -
        title: 'Кофепоинт'
        desc: 'Холодильник и кофемашина в красивом боксе'
        icon: 'dropdown-nav-icon-2.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-381015742171-korner-one'
        target: '_blank'
      -
        title: 'Приложение B-Pay'
        desc: 'Для оплаты товаров без очередей'
        icon: 'dropdown-nav-icon-3.png'
        link: 'https://app.briskly.online/'
        target: '_blank'
      -
        title: 'Модуль Briskly'
        desc: 'Сделает автономным любой холодильник'
        icon: 'dropdown-nav-icon-4.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-768167350561-modul-briskly'
        target: '_blank'
      -
        title: 'Касса в смартфоне'
        desc: 'Подключите Scan&Go в ваш магазин'
        icon: 'dropdown-nav-icon-5.png'
        link: 'https://briskly.online/briskly-for-shops/'
        target: '_blank'
      -
        title: 'Терминал Briskly'
        desc: 'Для оплаты картой в микромаркетах'
        icon: 'dropdown-nav-icon-6.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-820173015811-terminal-beckontaktnoi-oplati-20-briskly'
        target: '_blank'
      -
        title: 'Кофемашина'
        desc: 'Продает кофе через приложение B-Pay'
        icon: 'dropdown-nav-icon-7.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-302073347981-kofemashina-briskly'
        target: '_blank'
      -
        title: 'Каталог товаров'
        desc: 'Смотреть цены и описание устройств'
        icon: 'dropdown-nav-icon-17.png'
        link: 'https://linnafrost.ru/catalog'
        target: '_blank'
      # -
      #   title: 'Briskly Business',
      #   desc: 'Кабинет для управления торговой точкой',
      #   icon: 'dropdown-nav-icon-8.png',
      #   link: 'https://bbo.briskly.online/'
  -
    title: 'Стать партнером'
    link: 'https://briskly.online/partners'
    icon: null
    dropdown: null
    target: ''
  -
    title: 'О компании'
    link: null
    icon: null
    dropdown:
      -
        title: 'О нас'
        desc: 'История компании Briskly'
        icon: 'dropdown-nav-icon-about-1.png'
        link: 'https://briskly.online/story'
        target: '_blank'
      -
        title: 'Инвесторам'
        desc: 'Актуальная информация'
        icon: 'dropdown-nav-icon-about-2.png'
        link: 'https://briskly.online/investors'
        target: '_blank'
      -
        title: 'Завод Briskly Factory'
        desc: 'Выборгский завод холодильной техники, в прошлом - Linnafrost'
        icon: 'dropdown-nav-icon-about-3.png'
        link: 'https://linnafrost.ru/'
        target: '_blank'
      -
        title: 'Вакансии'
        desc: 'Присоединяйся к крутейшей команде Briskly'
        icon: 'dropdown-nav-icon-about-4.png'
        link: 'https://briskly.online/vacancy'
        target: '_blank'
      -
        title: 'Мы в СМИ'
        desc: 'Новости и статьи о продуктах'
        icon: 'dropdown-nav-icon-about-5.png'
        link: 'https://briskly.online/media'
        target: '_blank'
      # -
      #   title: 'Брискли журнал',
      #   desc: 'Научим вас зарабатывать на микромаркетах',
      #   icon: 'dropdown-nav-icon-about-6.png',
      #   link: ''
  -
    title: 'Личный кабинет'
    link: 'https://briskly.business/'
    icon: 'icon-lock'
    dropdown: null
    target: '_blank'
---
