---
index: 1
image: starter-popular.png
title: Умный холодильник <br> под ключ
price: 134 000 ₽
button: Подать заявку
link: '#section-feedback'
note: Популярный формат
---

<ul class="starter-list list is-light">
  <li>Микромаркет Briskly M4</li>
  <li>ДxШxВ: 595&times;600&times;2064&nbsp;мм</li>
  <li>Брендинг микромаркета</li>
  <li>Электронный замок</li>
  <li>Подключение к <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Интеграция ассортимента</li>
  <li>Датчики температуры</li>
  <li>Камера наблюдения</li>
</ul>