---
index: 2
image: starter-coffee.png
title: Кофепойнт <br> с холодильником <br> и кофемашиной
price: 690 000 ₽
button: Подать заявку
link: '#section-feedback'
note: null
---

<ul class="starter-list list is-light">
  <li>Микромаркет Briskly M5</li>
  <li>Кофемашина с оплатой через приложение</li>
  <li>Деревянный бокс с влагостойкой защитой</li>
  <li>Подключение к <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Электронный замок</li>
  <li>Камеры наблюдения</li>
</ul>