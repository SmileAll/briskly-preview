---
title: Наши продукты <span class="has-text-primary">продают сами</span>
description: Мы производим 54 вида оборудования на нашем заводе в Выборге.
link: 'https://linnafrost.ru/catalog'
button: Смотреть каталог
items:
  -
    title: Постамат
    description: Принимает, хранит и выдает заказы
    image: 'section-catalog-image-postamat.png'
    link: 'https://linnafrost.ru/catalog?tfc_storepartuid[280683011]=%D0%9F%D0%BE%D1%81%D1%82%D0%B0%D0%BC%D0%B0%D1%82%D1%8B+/+%D0%9F%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D1%8B'
    button: Подробнее
  -
    title: Микромаркет
    description: Автономная торговая точка
    image: 'section-catalog-image-micromarket.png'
    link: 'https://linnafrost.ru/catalog?tfc_storepartuid[280683011]=%D0%A3%D0%BC%D0%BD%D1%8B%D0%B5+%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82%D1%8B'
    button: Подробнее
  -
    title: Briskly Модуль
    description: Мозг и сердце автономной торговли
    image: 'section-catalog-image-module.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-768167350561-modul-briskly'
    button: Подробнее
  -
    title: Кофепоинт
    description: Все решения Briskly в одной коробке
    image: 'section-catalog-image-coffee-point.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-381015742171-korner-one'
    button: Подробнее
  -
    title: Кофемашина
    description: Полноценная замена бариста
    image: 'section-catalog-image-coffee-machine.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-302073347981-kofemashina-briskly'
    button: Подробнее
  -
    title: Briskly Терминал
    description: Для тех, кто хочет платить привычным способом
    image: 'section-catalog-image-terminal.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-820173015811-terminal-beckontaktnoi-oplati-20-briskly'
    button: Подробнее
---