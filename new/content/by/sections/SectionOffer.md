---
visible: true
title: 
  <span class="section-offer-title-procent">8%</span><br>
  <span class="section-offer-title-service">стоимость сервиса,</span><br>
  <span class="section-offer-title-acquiring">включая эквайринг</span>
description: 'Единая комиссия&nbsp;8% с&nbsp;каждого платежа&nbsp;&mdash; вместо зарплаты кассиров, большой аренды, охраны и&nbsp;прочих трат классического ритейла. Перечисляем 92% напрямую с продаж на ваш расчетный счет в течение 1-3 дней. В&nbsp;стоимость входит:'
button: Связаться с нами
link: '#section-feedback'
items:
  - Встроенный эквайринг
  - Техническая поддержка
  - Встроенная программа лояльности
  - Приложение для курьеров <a href="https://apps.apple.com/ru/app/%D1%87%D0%B5%D0%BA-%D0%B5%D1%80-%D0%BA%D0%B0%D1%81%D1%81%D0%B0-%D1%82%D0%B5%D1%80%D0%BC%D0%B8%D0%BD%D0%B0%D0%BB/id1441125241" target="_blank" class="link">ЧЕК-ЕР</a>
  - Отправка чеков в&nbsp;ОФД и&nbsp;клиенту
  - Личный кабинет Briskly Business
  - Маркетинг
---