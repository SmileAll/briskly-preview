---
title: Какая розница — <span class="has-text-primary">без разницы</span>
description: 'Наши программные решения и устройства для автономной торговли используют в ритейле разного масштаба: от 1 кв. м до полноценного беспилотного магазина.'
tabs: 
  -
    title: Все
    key: all
  -
    title: Ритейлерам
    key: retailers
  -
    title: Предпринимателям
    key: entrepreneurs
  - 
    title: Производителям еды
    key: food
items: 
  -
    key: [retailers, entrepreneurs, food]
    index: 1
    title: Умные автономные <br> микромаркеты
    description: Самый маленький вид ритейла. Позволяют открыть магазин без персонала в офисе, жилом комплексе или любом другом месте.
    link: '/{lang}/micromarket'
    button: Подробнее
    image: section-soft-fridge.png
  - key: [retailers]
    index: 2
    title: Нейросетевая аналитика
    description: Нейросеть распознаёт, когда покупатель автономной торговой точки берёт товар и списывает средства с банковской карты
    link: null
    button: Подробнее
    image: null
  - key: [entrepreneurs, retailers, food]
    index: 3
    title: Бизнес-кабинет для управления торговой точкой
    description: Меняйте ассортимент, снимайте показатели продаж, следите за состоянием оборудования и данными телеметрии в личном кабинете Briskly Business
    link: 'https://bbo.briskly.online/auth'
    button: Подробнее
    image: null
  - key: [retailers]
    index: 4
    title: Мобильное приложение <br>с функцией Scan&Go
    description: Это касса в смартфоне, которая позволяет экономить время и не стоять в очередях. Клиенты сканируют штрихкоды и оплачивают товары без очередей, в магазинах и микромаркетах.
    link: null
    button: Подробнее
    image: section-soft-phone.png
  - key: [retailers, food]
    index: 5
    title: Холодильное оборудование
    description: Завод Briskly в Выборге производит 16 видов коммерческих холодильников, которые используются в ритейле и не предприятиях общественного питания.
    link: 'https://linnafrost.ru/catalog'
    button: Подробнее
    image: null
  - key: [retailers, food]
    index: 6
    title: Умная кофемашина
    description: Автономная зерновая кофемашина с возможностью оплаты кофе через приложение. Готовит до 25 видов напитков.
    link: 'https://linnafrost.ru/catalog/tproduct/1-302073347981-kofemashina-briskly'
    button: Подробнее
    image: null
---