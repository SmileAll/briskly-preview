---
title: Briskly Micromarket | Smart coolers with self-checkout system
description: Start selling your products or ready meals from suppliers in smart micro markets that work without staff. 
keywords: briskly, scan&go, b-pay, micromarkets, micromarket, autonomous trading
FirstScreen:
  title: Briskly
  description: Autonomous Retail Technologies
  text: With our app, customers buy fresh food and goods without queues.<br>Retailers use our hardware and software to launch low-cost autonomous stores.
  background: firstscreen2-index.png
  action: '#SectionStatistics'
  button: More
  items:
    -
      title: Smart micromarkets
      link: '#SectionSoft'
    -
      title: Scan&Go App
      link: '#SectionSoft'
    -
      title: Autonomous retail
      link: '#SectionSoft'
    -
      title: Neural network analytics
      link: '#SectionSoft'
---