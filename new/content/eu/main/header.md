---
apps:
  title: Download the B-Pay app
  google:
    link: https://play.google.com/store/apps/details?id=online.briskly
    image: googleplay.png
  apple:
    link: https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8
    image: appstore.png
contact:
  title: Write, call, subscribe
  button: Order a call
  phone: +442045771195
  phoneAction: tel:+442045771195
socials: 
  -
    link: https://www.tiktok.com/@briskly_house
    icon: '#social-tiktok'
  -
    link: https://t.me/Brskl
    icon: '#social-telegram'
  -
    link: https://vk.com/briskly.online
    icon: '#social-vk'
navigation:
  -
    title: 'Products'
    link: null
    icon: null
    dropdown:
      -
        title: 'Micromarkets'
        desc: 'Smart coolers with self-checkout system'
        icon: 'dropdown-nav-icon-1.png'
        link: '/{lang}/micromarket'
        target: '_blank'
      -
        title: 'Coffee Point'
        desc: 'Cooler + Coffeemachine at the box'
        icon: 'dropdown-nav-icon-2.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-381015742171-korner-one'
        target: '_blank'
      -
        title: 'B-Pay Mobile App'
        desc: 'Scan barcodes and pay without queues'
        icon: 'dropdown-nav-icon-3.png'
        link: 'https://app.briskly.online/'
        target: '_blank'
      -
        title: 'Briskly Module'
        desc: 'Will make any cooler smart'
        icon: 'dropdown-nav-icon-4.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-768167350561-modul-briskly'
        target: '_blank'
      -
        title: 'Connect Checkout in a Smartphone'
        desc: 'Connect Scan&Go at your store and get rid of queues'
        icon: 'dropdown-nav-icon-5.png'
        link: 'https://briskly.online/{lang}/briskly-for-shops/'
        target: '_blank'
      -
        title: 'POS-Terminal Briskly'
        desc: 'Pay in smart coolers via card'
        icon: 'dropdown-nav-icon-6.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-820173015811-terminal-beckontaktnoi-oplati-20-briskly'
        target: '_blank'
      -
        title: 'Autonomous Coffee Machine'
        desc: 'Сoffee with fresh milk through the app'
        icon: 'dropdown-nav-icon-7.png'
        link: 'https://linnafrost.ru/catalog/tproduct/1-302073347981-kofemashina-briskly'
        target: '_blank'
      -
        title: 'Product catalog'
        desc: 'View prices and device descriptions'
        icon: 'dropdown-nav-icon-17.png'
        link: 'https://linnafrost.ru/catalog'
        target: '_blank'
      # -
      #   title: 'Briskly Business Dashboard',
      #   desc: 'Personal business panel for your autonomous store',
      #   icon: 'dropdown-nav-icon-8.png',
      #   link: 'https://bbo.briskly.online/'
  -
    title: 'Become a dealer'
    link: 'https://briskly.online/eu/partners'
    icon: null
    dropdown: null
    target: ''
  -
    title: 'About the company'
    link: null
    icon: null
    dropdown:
      -
        title: 'About us'
        desc: 'Company history Briskly'
        icon: 'dropdown-nav-icon-about-1.png'
        link: 'https://briskly.online/story'
        target: '_blank'
      -
        title: 'Investors'
        desc: 'Current information'
        icon: 'dropdown-nav-icon-about-2.png'
        link: 'https://briskly.online/investors/'
        target: '_blank'
      -
        title: 'Briskly Factory'
        desc: 'Producing refrigerators near the Finland border at Vyborg city'
        icon: 'dropdown-nav-icon-about-3.png'
        link: 'https://linnafrost.ru/'
        target: '_blank'
      -
        title: 'Careers'
        desc: 'Join the Briskly team'
        icon: 'dropdown-nav-icon-about-4.png'
        link: 'https://briskly.online/vacancy/'
        target: '_blank'
      -
        title: 'Media'
        desc: 'News and articles about products'
        icon: 'dropdown-nav-icon-about-5.png'
        link: 'https://briskly.online/media/'
        target: '_blank'
      # -
      #   title: 'Briskly Knowledge Center',
      #   desc: 'How to make money on micromarkets',
      #   icon: 'dropdown-nav-icon-about-6.png',
      #   link: ''
  -
    title: 'Login'
    link: 'https://briskly.business/'
    icon: 'icon-lock'
    dropdown: null
    target: '_blank'
---
