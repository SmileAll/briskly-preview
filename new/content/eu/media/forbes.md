---
index: 0
image: 'section-media-image-forbes.png'
title: 'forbes.ru'
content: 'How do stores work without cashiers and how much does it cost?'
note: ''
link: 'https://www.forbes.ru/biznes/417191-kak-rabotayut-magaziny-bez-kassirov-pyaterochki-i-azbuki-vkusa-i-skolko-eto-stoit'
button: Read
---