---
index: 3
image: starter-only-coffee.png
title: Coffee machine
price: On demand
button: Submit an application
link: '#section-feedback'
note: null
---

<ul class="starter-list list is-light">
  <li>Ordering drinks via the mobile app <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Setting up your assortment</li>
  <li>Built-in acquiring</li>
  <li>Customer technical support</li>
  <li>Online platform for point of sale management</li>
  <li>Mobile service maintenance</li>
</ul>