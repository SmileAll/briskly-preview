---
index: 4
image: starter-frozen.png
title: Freezer cabinet
price: 6050 €
button: Submit an application
link: '#section-feedback'
note: null
---

<ul class="starter-list list is-light">
  <li>Temperature range: -18 to -24</li>
  <li>Integration with the accounting system</li>
  <li>Electronic lock</li>
  <li>Mobile Application <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Technical support</li>
  <li>Acquiring</li>
  <li>Surveillance camera</li>
</ul>
