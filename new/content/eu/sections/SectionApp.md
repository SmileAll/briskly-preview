---
title: New shopping experience
description:
  'The process is based on a smartphone. Our self-checkout app <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a> combines the functions of a barcode scanner and a payment terminal.
  <br><br>
  <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay App</a> shows the micromarket on map, unlocks the door, allows you to pay via card or Apple/Google Pay and apply discounts and promotions. The receipt is available in profile.'
items: 
  - Unlock the cooler by scanning the QR code on the door.
  - Scan the barcode of goods.
  - Pay with the bank card or use Apple/Google Pay.
---
<div class="section-app-apps">
  <a href="https://play.google.com/store/apps/details?id=online.briskly" target="_blank">
    <img src="googleplay.png" alt="">
  </a>
  <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" target="_blank">
    <img src="appstore.png" alt="">
  </a>
</div>