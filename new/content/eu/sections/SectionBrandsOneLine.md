---
title: Our technology is used
brands:
  - 
    direction: left
    images: ['brands/mts.png', 'brands/azbukavkusa.png', 'brands/utkonos.png', 'brands/aeromar.png', 'brands/kulinarium.png', 'brands/greenbox.png', 'brands/befit.png', 'brands/parusa.png', 'brands/gastronomist.png', 'brands/freshpoint.png', 'brands/pole.png', 'brands/gusto.png', 'brands/stolcafe.png', 'brands/vkusperekus.png', 'brands/andy.png']
---