---
title: Our <span class="has-text-primary">products</span>
description: We produce 54 types of equipment at our factory in Vyborg.
link: 'https://linnafrost.ru/catalog'
button: View the catalog
items:
  -
    title: Parcel Locker
    description: Accepts, stores and issues orders
    image: 'section-catalog-image-postamat.png'
    link: 'https://linnafrost.ru/catalog?tfc_storepartuid[280683011]=%D0%9F%D0%BE%D1%81%D1%82%D0%B0%D0%BC%D0%B0%D1%82%D1%8B+/+%D0%9F%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D1%8B'
    button: More details
  -
    title: Micromarket
    description: Smallest autonomous retail
    image: 'section-catalog-image-micromarket.png'
    link: 'https://linnafrost.ru/catalog?tfc_storepartuid[280683011]=%D0%A3%D0%BC%D0%BD%D1%8B%D0%B5+%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82%D1%8B'
    button: More details
  -
    title: Briskly Module
    description: The brain and heart of Autonomous Retail
    image: 'section-catalog-image-module.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-768167350561-modul-briskly'
    button: More details
  -
    title: Coffee Point
    description: All Briskly solutions in one box
    image: 'section-catalog-image-coffee-point.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-381015742171-korner-one'
    button: More details
  -
    title: Coffee machine
    description: A full-fledged barista replacement
    image: 'section-catalog-image-coffee-machine.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-302073347981-kofemashina-briskly'
    button: More details
  -
    title: POS-Terminal Briskly
    description: For those who want to pay in the usual way
    image: 'section-catalog-image-terminal.png'
    link: 'https://linnafrost.ru/catalog/tproduct/1-820173015811-terminal-beckontaktnoi-oplati-20-briskly'
    button: More details
---