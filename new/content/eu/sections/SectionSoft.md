---
title: Who are <span class="has-text-primary">our solutions for?</span>
description: 'Our software and hardware for autonomous retail are used in retail of different scales: from 1 square meter to a full-sized unmanned store.'
tabs: 
  -
    title: All
    key: all
  -
    title: Retailers
    key: retailers
  -
    title: Entrepreneurs
    key: entrepreneurs
  - 
    title: Food Providers
    key: food
  - 
    title: Vending
    key: vending
items: 
  -
    key: [retailers, entrepreneurs, food, vending]
    index: 1
    title: Smart autonomous <br> micromarkets
    description: The smallest type of retail. It allows you to open a store without staff in an office or any other place.
    link: '/{lang}/micromarket'
    button: More detailed
    image: section-soft-fridge.png
  - key: [retailers]
    index: 2
    title: Neural network analytics
    description: The neural network recognizes when the customer in a store takes the goods and withdraws money from their bank card.
    link: null
    image: null
    button: More detailed
  - key: [entrepreneurs, retailers, food, vending]
    index: 3
    title: Business Dashboard
    description: Change the assortment of goods, download reports, monitor the condition of equipment and telemetry data in your personal panel.
    link: 'https://bbo.briskly.online/auth'
    button: More detailed
    image: null
  - key: [retailers]
    index: 4
    title: Scan&Go Mobile <br> App "B-Pay"
    description: This is a self-checkout app, which allows you to save time and pay without queues. Customers scan barcodes and pay for goods in our stores and micromarkets.
    link: null
    button: More detailed
    image: section-soft-phone.png
---