---
title: No queues.  No staff. <span class="has-text-primary">Safe.</span>
description: Briskly is an autonomous retail technology provider. We produce low-cost micromarkets – small safe autonomous retail stores, – where customers buy goods through mobile app or a POS-terminal. A customer opens the door via app, scans the barcode of a product and pays with a bank card. Retailers also use our app in the supermarkets to launch cheap self-checkout. 
items:
  -
    title: 8 countries
    text: Where we<br>are presented
    image: section-statistics-image-full-1.png
  -
    title: 1 000 000
    text: Autonomous <br>transactions
    image: section-statistics-image-full-2.png
  -
    title: 1 500+
    text: Micromarkets <br>sold to date
    image: section-statistics-image-full-3.png
images:
  -
    title: Winners of the contest <br> VISA Everywhere Initiative 2018
    image: section-statistics-image-visa.png
  -
    title: Winners of United Nations Contest <br> "Good Food For All"
    image: section-statistics-image-good-food.png
  -
    title: Holders of the <br> security certificate
    image: section-statistics-image-pci.png
---