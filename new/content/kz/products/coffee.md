---
index: 3
image: starter-only-coffee.png
title: Кофемашина
price: 337 000 ₽
button: Подать заявку
link: '#section-feedback'
note: null
---

<ul class="starter-list list is-light">
  <li>Заказ напитков через мобильное приложение <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Настройка вашего ассортимента</li>
  <li>Встроенный эквайринг</li>
  <li>Техническая поддержка покупателей</li>
  <li>Онлайн-платформа для управления торговой точкой</li>
  <li>Обслуживание мобильного сервиса</li>
</ul>