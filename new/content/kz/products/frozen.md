---
index: 4
image: starter-frozen.png
title: Морозильный шкаф
price: 223 000 ₽
button: Подать заявку
link: '#section-feedback'
note: null
---

<ul class="starter-list list is-light">
  <li>Темп. режим: от -18 до -24</li>
  <li>Интеграция с системой учёта</li>
  <li>Электронный замок</li>
  <li>Мобильное приложение <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Техническая поддержка</li>
  <li>Эквайринг</li>
  <li>Камера наблюдения</li>
</ul>
