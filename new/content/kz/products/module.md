---
index: 0
image: starter-module.png
title: Установка системы <br> в ваш холодильник
price: 82 000 ₽
button: Подать заявку
link: '#section-feedback'
note: null
---

<ul class="starter-list list is-light">
  <li>Установка электронного замка</li>
  <li>Подключение к <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Интеграция ассортимента</li>
  <li>Датчики температуры</li>
  <li>Камера наблюдения</li>
  <li>Briskly Модуль - собственная разработка компании</li>
</ul>