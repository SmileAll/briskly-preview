---
index: 6
image: starter-terminal.png
title: Терминал&nbsp;2.0 для <br> оплаты картой
price: 77 000 ₽
button: Подать заявку
link: '#section-feedback'
note: null
---

<span class="starter-list-description">
  Дополнительный терминал для оплаты без приложения
</span>
<div class="starter-list-subtitle">
  Включает:
</div>
<ul class="starter-list list is-light">
  <li>Сканер штрихкодов товаров</li>
  <li>Приём банковских карт</li>
  <li>Эквайринг</li>
</ul>