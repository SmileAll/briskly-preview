---
title: Производим микромаркеты на&nbsp;<span class="has-text-primary">собственном заводе</span> в&nbsp;Выборге
description: История завода насчитывает 105 лет. С 1916 года финн Хейкки Хелкама производил здесь бытовую технику под брендом Helkama. В 2021 году Briskly приобрели завод у банка Траст, и наладили полный цикл производства микромаркетов.
video: 'https://www.youtube.com/watch?v=NZVEUzD-zzE'
image: multi-fridge.png
preview: player.jpg
---