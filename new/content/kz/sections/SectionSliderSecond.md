---
visible: true
title: Нет своей еды? <br> <span class="has-text-primary">Выберите проверенного поставщика:</span>
note: Перейти на сайт
items:
  -
    image: 'slider-second-image14.jpg'
    title: 'АэроМар'
    content: 'Готовое производство еды в Москве'
    note: ''
    link: 'https://aeromar.ru/'
  -
    image: 'slider-second-image1.jpg'
    title: 'Кулинариум'
    content: 'Готовое производство еды в Москве'
    note: ''
    link: 'https://kylinarium.ru/'
  -
    image: 'slider-second-image2.jpg'
    title: 'Славянская трапеза'
    content: 'Готовое производство еды в Москве'
    note: ''
    link: 'http://st-pk.ru/'
  -
    image: 'slider-second-image3.jpg'
    title: 'Just Food Pro'
    content: 'Готовое производство еды в Москве'
    note: ''
    link: 'https://www.justfood.pro/'
  -
    image: 'slider-second-image4.jpg'
    title: 'Шеф на районе'
    content: 'Готовое производство еды в Москве'
    note: ''
    link: ''
  -
    image: 'slider-second-image5.jpg'
    title: 'MyFruit'
    content: 'Готовое производство еды в Москве'
    note: ''
    link: ''
  -
    image: 'slider-second-image6.jpg'
    title: 'City food'
    content: 'Производство сендвичей, салатов, сладкого'
    note: ''
    link: 'https://cityfood.pro/'
  -
    image: 'slider-second-image7.jpg'
    title: 'Gastronomist'
    content: 'Производство готовой еды в Санкт-Петербурге'
    note: ''
    link: 'http://gastronomist.pro/'
  -
    image: 'slider-second-image8.jpg'
    title: 'Натурово'
    content: 'Производство готовой еды в Калининграде'
    note: ''
    link: 'https://naturovo.ru/'
  -
    image: 'slider-second-image9.jpg'
    title: 'Обедов'
    content: 'Производство готовой еды в Екатеринбурге'
    note: ''
    link: 'http://obedov66.ru/'
  -
    image: 'slider-second-image10.jpg'
    title: 'Olimpfood'
    content: 'Производство готовой еды в Нижнем Новгороде, Красноярске'
    note: ''
    link: 'https://olimpfood.com/'
  -
    image: 'slider-second-image11.jpg'
    title: 'My fit eat'
    content: 'Производство готовой еды в Санкт-Петербурге'
    note: ''
    link: 'https://myfiteat.ru/'
  -
    image: 'slider-second-image12.jpg'
    title: 'Superfood'
    content: 'Производство готовой еды в Краснодаре'
    note: ''
    link: ''
  -
    image: 'slider-second-image13.jpg'
    title: 'Mr. Food'
    content: 'Производство готовой еды в Москве'
    note: ''
    link: ''
---