---
items:
  -
    note: Шаг 1
    title: Покупатель <br> входит в магазин
    image: section-steps-step-1.png
  -
    note: Шаг 2
    title: Сканирует <br> штрихкоды
    image: section-steps-step-2.png
  -
    note: Шаг 3
    title: Оплачивает <br> покупку
    image: section-steps-step-3.png
---