---
title: 'Briskly | Технологии для торговли без персонала'
description: 'Софт и оборудование для автономной торговли: микромаркеты, умные холодильники, приложение с функцией «сканируй и покупай». Решения для ритейлеров и производителей еды.'
keywords: 'брискли, briskly, сканируй и покупай, касса в смартфоне, микромаркет, scan&go, приложение b-pay, брискли онлайн, магазин без кассира, микромаркеты briskly, умный холодильник, магазины без кассира, briskly, киоск самообслуживания, микромаркет, вендинг автоматы, вендинговые автоматы, вендинговые аппараты, торговый автомат, автомат в торговом центре, торговый аппарат, вендинговая продажа, новые бизнес идеи, открыть новый бизнес, куда можно вложить деньги, франшиза под ключ, бизнес с быстрой окупаемостью, лучшие бизнес идеи, купить вендинговый, b pay, b-pay, брискли, еда в микромакете"'
FirstScreen:
  title: Briskly
  description: Мы&nbsp;создаем технологии для&nbsp;торговли&nbsp;без персонала
  text: С помощью наших решений покупатели обслуживают себя сами.<br>Без очередей. Без кассиров. Безопасно.
  background: firstscreen-index.jpg
  action: '#SectionStatistics'
  button: Подробнее
  items:
    -
      title: Умные микромаркеты
      link: '#SectionSoft'
    - 
      title: Касса в смартфоне
      link: '#SectionSoft'
    - 
      title: Автономные магазины
      link: '#SectionSoft'
    - 
      title: Нейросетевая аналитика
      link: '#SectionSoft'
---
