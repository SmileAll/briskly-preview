---
title: Новый опыт покупок
description:
  'В основе процесса — мобильное приложение <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a>. Это касса в смартфоне, которая совмещает функции сканера штрихкодов и терминала оплаты.
  <br><br>
  <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a> показывает покупателю ближайший киоск, открывает замок холодильника, позволяет применять скидки и акции. Чек ОФД доступен покупателю
  в приложении <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a>.'
items: 
  - Покупатель открывает микромаркет, сканируя QR-код.
  - Сканирует штрихкоды на товарах.
  - Оплачивает банковской картой или Apple Pay.
---
<div class="section-app-apps">
  <a href="https://play.google.com/store/apps/details?id=online.briskly" target="_blank">
    <img src="googleplay.png" alt="">
  </a>
  <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" target="_blank">
    <img src="appstore.png" alt="">
  </a>
</div>