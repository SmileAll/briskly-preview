---
title: Безопасные <br> продажи
list:
  - Камера наблюдения записывает все операции, которые проводятся с&nbsp;микромаркетом
  - Сомнительные операции отображаются в&nbsp;специальном разделе в&nbsp;личном кабинете
  - Пользователи идентифицируются по&nbsp;телефону и&nbsp;карте, дополнительно&nbsp;&mdash; по&nbsp;паспорту
  - В&nbsp;случае мошенничества деньги списываются с&nbsp;карты удаленно по&nbsp;запросу владельца микромаркета
items:
  -
    title: Меньше&nbsp;2%
    description: покупателей забирают товар <br> без оплаты
    image: section-safe-image-1.png
  -
    title: 0,3% краж
    description: Случается в закрытых помещениях, офисах.
    image: section-safe-image-2.png
  -
    title: 120°
    description: угол обзора камер наблюдения в&nbsp;микромаркете
    image: section-safe-image-3.png

gallery:
  - 'gallery-safe/safe-full-0.jpg'
  - 'gallery-safe/safe-full-1.jpg'
  - 'gallery-safe/safe-full-2.jpg'
  - 'gallery-safe/safe-full-3.jpg'
  - 'gallery-safe/safe-full-4.jpg'
  - 'gallery-safe/safe-full-5.jpg'
  - 'gallery-safe/safe-full-6.jpg'
  - 'gallery-safe/safe-full-7.jpg'
  - 'gallery-safe/safe-full-9.jpg'
thumbs:
  - 'gallery-safe/safe-preview-0.png'
  - 'gallery-safe/safe-preview-1.png'
  - 'gallery-safe/safe-preview-2.png'
  - 'gallery-safe/safe-preview-3.png'
  - 'gallery-safe/safe-preview-4.png'
  - 'gallery-safe/safe-preview-5.png'
  - 'gallery-safe/safe-preview-6.png'
  - 'gallery-safe/safe-preview-7.png'
  - 'gallery-safe/safe-preview-9.png'
---
