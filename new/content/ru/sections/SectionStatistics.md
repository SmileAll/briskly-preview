---
title: Без очередей. Без кассиров. <span class="has-text-primary">Безопасно.</span>
description: В торговых точках, которые подключены к платформе Briskly, покупатели обслуживают себя сами. Они сканируют штрихкоды товаров и оплачивают покупки через кассу в смартфоне — приложение B-Pay. Также, Briskly производит микромаркеты и другое оборудование для автономной торговли на собственном заводе в Выборге.
items:
  -
    title: 8 стран
    text: Где работает <br> технология <br>
    image: section-statistics-image-full-1.png
  -
    title: 1 000 000
    text: Покупок <br>без кассира
    image: section-statistics-image-full-2.png
  -
    title: 1 500+
    text: Автономных <br> микромаркетов продано
    image: section-statistics-image-full-3.png
images:
  -
    title: Победители конкурса <br> VISA Everywhere Initiative 2018
    image: section-statistics-image-visa.png
  -
    title: Победители конкурса ООН <br> "Good Food For All"
    image: section-statistics-image-good-food.png
  -
    title: Обладатели сертификата <br> безопасности
    image: section-statistics-image-pci.png
---