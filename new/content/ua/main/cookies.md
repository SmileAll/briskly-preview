---
title: Куки
description: Этот веб-сайт использует файлы <a href="https://briskly.online/user_doc/cookies.pdf" target="_blank" class="link">Cookie</a>. Продолжая использовать наш сайт, вы автоматически соглашаетесь с использованием данных технологий.
button: OK
---