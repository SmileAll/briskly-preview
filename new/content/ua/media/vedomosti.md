---
index: 0
image: 'section-media-image-vedomosti.png'
title: 'vedomosti.ru'
content: 'Российские продуктовые магазины вводят оплату мимо кассы'
note: ''
link: 'https://www.vedomosti.ru/business/articles/2020/02/13/822968-rossiiskie-produktovie-magazini-vvodyat'
button: Читать
---