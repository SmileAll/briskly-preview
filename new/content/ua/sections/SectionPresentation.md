---
title: Отправим презентацию и&nbsp;перезвоним
description: Получите всё сейчас, решите потом. <br> Оставьте телефон, чтобы получить ещё и финмодель.
preview: 'youtube-presentation.png'
video: 'https://www.youtube.com/watch?v=wHyvyqfE0vo'
about: Микромаркет самообслуживания — новый способ торговли <br> свежей готовой едой в общественных местах.
---

<script data-b24-form="inline/51/ukchhi" data-skip-moving="true">
	(function(w,d,u){
		var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://cdn-ru.bitrix24.ru/b6486705/crm/form/loader_51.js');
</script>