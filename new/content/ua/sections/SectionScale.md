---
note: Ближе к делу
title: Производим оборудование с нуля
description: Собираем микромаркеты и холодильники на конвеере, <br>проверяем качество и отгружаем в страны Европы и СНГ
link: 'https://linnafrost.ru/catalog'
button: В каталог завода
items:
  -
    title: 2,4 гектара
    description: площадь завода в Выборге
    image: section-scale-point.png
  -
    title: 18 000
    description: холодильников в год — мощность завода
    image: section-scale-micromarket.png
  -
    title: 54 вида
    description: оборудования производит завод
    image: section-scale-tools.png
---