---
title: Сомневаетесь? <br> <span class="has-text-primary">У них уже получилось:</span>
note: 'Читать'
items:
  -
    image: 'slider-first-image-kazan.jpg'
    title: 'Айрат Кашапов в Казани'
    content: 'Ушел с работы и теперь расширяет свою сеть микромаркетов.'
    note: ''
    link: 'https://www.instagram.com/p/CQ-7ynwFnhH/'
  -
    image: 'slider-first-image-orenburg.jpg'
    title: 'Руслан Мухарямов в Оренбурге'
    content: 'Допустил ошибки с выбором товаров и локаций, но принял верное решение и увеличил доход.'
    note: ''
    link: 'https://www.instagram.com/p/CSodm14pqbA/'
  -
    image: 'slider-first-image1.jpg'
    title: 'Микромаркеты “Точка вкуса” в Иркутске'
    content: 'Наталья Селенкова и Анна Логинова рискнули и попробовали новую нишу'
    note: ''
    link: 'https://www.instagram.com/p/CMKJRfKlKzF/'
  -
    image: 'slider-first-image2.jpg'
    title: 'Владимир Казанцев в Барнауле'
    content: 'Владимир продал клининговую компанию и с головой окунулся в автономную торговлю'
    note: ''
    link: 'https://www.instagram.com/p/CK83wI2laCi/'
  -
    image: 'slider-first-image4.jpg'
    title: 'Шеф Алексей Гулякин в Сургуте'
    content: 'Закрыл ресторан и открыл сеть микромаркетов. И не прогадал!'
    note: ''
    link: 'https://www.instagram.com/p/CEO8uJqjwg7/'
  -
    image: 'slider-first-image3.jpg'
    title: 'Антон Улитин в Санкт-Петербурге'
    content: 'В прошлом - сотрудник АЗС, а сегодня - владелец сети из 6 точек'
    note: ''
    link: 'https://www.instagram.com/p/CJLim2hlhU2/'
  -
    image: 'greenbox.png'
    title: 'GreenBox в Санкт-Петербурге'
    content: 'Сеть кафе здорового питания поставила в офисах уже 60 микромаркетов.'
    note: ''
    link: 'https://www.instagram.com/p/CQOiboUImZr/'
  -
    image: 'av.png'
    title: 'Азбука Вкуса в Москве'
    content: 'Премиум-супермаркет пробует новый формат. Люди в офисах счастливы.'
    note: ''
    link: 'https://av.ru/lp/micromarket/'
  -
    image: 'fermabot.png'
    title: 'Фермоботы “Ключевское” в Москве'
    content: 'Можно ли продавать фермерские продукты в микромаркетах? Нужно!'
    note: ''
    link: 'https://www.instagram.com/p/COPwmtxlizO/'
---