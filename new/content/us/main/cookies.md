---
title: Cookies
description: This website uses files <a href="https://briskly.online/user_doc/cookies.pdf" target="_blank" class="link">Cookie</a>. By continuing to use our site, you automatically agree to the use of these technologies.
button: OK
---