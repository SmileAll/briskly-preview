---
description: Autonomous Retail Technology Platform
certificates: 'Development certificates are registered: <br> 2020619020, 2020619021, 2020618645, <br> 2020618646, 2020618899, 2020618422.'
info: null
year: © Briskly LTD, 2022
contact:
  button: Order call
  phone: 8-800-222-61-09
  phoneAction: tel:+78002226109
  link: '#section-feedback'
additionalLogo:
  image: 'img/social-networks/social-sk.png'
  link: 'http://www.sk.ru/'
  text: null
icons:
  - mastercard.png
  - visa.png
  - google-play.png
  - sbp.png
  - pci.png
  - iso.png

copy: 'Google Pay and&nbsp;logotype Google Pay are trademarks of the corporation Google Inc. Logotype MasterCard is a trademark of the company MasterCard. Logotype Visa is a trademark of the company Visa. Logotype PCI is a registered trademark of the company PCI. Logotype ISO is a trademark of the company ISO.'

bottomText: Visit&nbsp;the B&#8209;Pay application website to pay for goods without queues
bottomLink: https://app.briskly.online/

social:
  -
    icon: social-tiktok
    link: https://www.tiktok.com/@briskly_house
  -
    icon: social-telegram
    link: https://t.me/Brskl
  -
    icon: social-vk
    link: https://vk.com/briskly.online

additionalPhones:
  -
    number: '+442045771195'
    action: 'tel:+442045771195'
    country: 'Great Britain'
  -
    number: '+13235405719'
    action: 'tel:+13235405719'
    country: 'USA'

navigationOne:
  -
    link: https://b-pay.online/
    title: B-Pay Mobile App
    target: _blank
  -
    link: https://briskly.business/
    title: Login Business Dashboard
    target: _blank
  -
    link: https://linnafrost.ru/
    title: Briskly Factory
    target: _blank
  -
    link: https://linnafrost.ru/catalog
    title: Catalog
    target: _blank
  -
    link: https://briskly.online/en/briskly-for-shops/
    title: Launch Scan&Go at the store
    target: _blank

  -
    link: /micromarket/
    title: Smart Coolers
    target: _blank

navigationTwo:
  -
    link: https://briskly.online/eu/partners
    title: Become a dealer
    target: _blank
  -
    link: https://t.me/Brskl
    title: Company News RSS
    target: _blank
  -
    link: https://briskly.online/user_doc
    title: Privacy Policy
    target: _blank
  -
    link: https://briskly.online/wp-content/uploads/2019/08/privacy.pdf
    title: Processing of personal data
    target: _blank
  -
    link: https://briskly.online/user_doc
    title: User Agreement
    target: _blank
  -
    link: https://briskly.online/contacts
    title: Contacts
    target: _blank
---
