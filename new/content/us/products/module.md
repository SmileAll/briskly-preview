---
index: 0
image: starter-module.png
title: Installing the system <br> in your refrigerator
price: On demand
button: Submit an application
link: '#section-feedback'
note: null
---

<ul class="starter-list list is-light">
  <li>Installing an electronic lock</li>
  <li>Connecting to <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a></li>
  <li>Product range integration</li>
  <li>Temperature sensors</li>
  <li>Surveillance camera</li>
  <li>Briskly Module is the company's own development</li>
</ul>