---
index: 6
image: starter-terminal.png
title: Terminal 2.0 for <br> card payment
price: On demand
button: Submit an application
link: '#section-feedback'
note: null
---

<span class="starter-list-description">
  Additional terminal for payment without an app
</span>
<div class="starter-list-subtitle">
  Includes:
</div>
<ul class="starter-list list is-light">
  <li>Barcode scanner of goods</li>
  <li>Accepting bank cards</li>
  <li>Acquiring</li>
</ul>