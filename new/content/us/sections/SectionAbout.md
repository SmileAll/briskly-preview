---
image: section-about-preview.jpg
title: This is Briskly Micromarket 
description: Micromarket is the smallest type of retail where goods are sold without staff <br><br> In 2019 we got an idea to add a smart Module into the usual cooler to sell goods via our <a href="https://app.briskly.online/" target="_blank">B-Pay App</a> and pay for them via smartphone.
about: 'Briskly smart coolers are produced at <a href="https://linnafrost.ru/" target="_blank" class="link">our Factory</a> near Finland Border, at Vyborg city.'
button: Our solutions
link: '#section-starter'
gallery: [
  'gallery-about/about-full-0.jpg',
  'gallery-about/about-full-1.jpg',
  'gallery-about/about-full-2.jpg',
  'gallery-about/about-full-3.jpg',
  'gallery-about/about-full-4.jpg',
  'gallery-about/about-full-5.jpg',
  'gallery-about/about-full-6.jpg',
  'gallery-about/about-full-7.jpg',
  'gallery-about/about-full-8.jpg',
  'gallery-about/about-full-9.jpg',
  'gallery-about/about-full-10.jpg',
  'gallery-about/about-full-11.jpg',
  'gallery-about/about-full-12.jpg',
  'gallery-about/about-full-13.jpg',
  'gallery-about/about-full-14.jpg',
  'gallery-about/about-full-15.jpg',
  'gallery-about/about-full-16.jpg',
  'gallery-about/about-full-17.jpg',
  'gallery-about/about-full-18.jpg',
  'gallery-about/about-full-19.jpg',
  'gallery-about/about-full-30.jpg',
  'gallery-about/about-full-31.jpg',
  'gallery-about/about-full-32.jpg',
  'gallery-about/about-full-33.jpg',
  'gallery-about/about-full-34.jpg',
  'gallery-about/about-full-35.jpg',
  'gallery-about/about-full-36.jpg',
  'gallery-about/about-full-37.jpg',
  'gallery-about/about-full-38.jpg',
  'gallery-about/about-full-39.jpg',
  'gallery-about/about-full-40.jpg'
]
thumbs: [
  'gallery-about/about-preview-0.png',
  'gallery-about/about-preview-1.png',
  'gallery-about/about-preview-2.png',
  'gallery-about/about-preview-3.png',
  'gallery-about/about-preview-4.png',
  'gallery-about/about-preview-5.png',
  'gallery-about/about-preview-6.png',
  'gallery-about/about-preview-7.png',
  'gallery-about/about-preview-8.png',
  'gallery-about/about-preview-9.png',
  'gallery-about/about-preview-10.png',
  'gallery-about/about-preview-11.png',
  'gallery-about/about-preview-12.png',
  'gallery-about/about-preview-13.png',
  'gallery-about/about-preview-14.png',
  'gallery-about/about-preview-15.png',
  'gallery-about/about-preview-16.png',
  'gallery-about/about-preview-17.png',
  'gallery-about/about-preview-18.png',
  'gallery-about/about-preview-19.png',
  'gallery-about/about-preview-30.png',
  'gallery-about/about-preview-31.png',
  'gallery-about/about-preview-32.png',
  'gallery-about/about-preview-33.png',
  'gallery-about/about-preview-34.png',
  'gallery-about/about-preview-35.png',
  'gallery-about/about-preview-36.png',
  'gallery-about/about-preview-37.png',
  'gallery-about/about-preview-38.png',
  'gallery-about/about-preview-39.png',
  'gallery-about/about-preview-40.png'
]
---

<div class="section-about-lists">
  <div class="section-about-list">
    <h5 class="section-about-list-title">
      What to sell?
    </h5>
    <ul class="list">
      <li>Food</li>
      <li>Snacks</li>
      <li>Drinks</li>
	<li>All kind of goods</li>
    </ul>
  </div>
  <div class="section-about-list">
    <h5 class="section-about-list-title">
      Where to sell?
    </h5>
    <ul class="list">
      <li>Office</li>
	<li>Business Centers</li>
      <li>Fitness Clubs</li>
      <li>Departure area</li>
	<li>Universities</li>
    </ul>
  </div>
</div>