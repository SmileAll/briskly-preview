---
title: In these companies <br> Briskly works
brands:
  - 
    direction: left
    images: ['brands/aero.png', 'brands/azbuka.png', 'brands/CocaCola.png', 'brands/freshpoint.png', 'brands/Green.png', 'brands/pole.png']
  -
    direction: right
    images: ['brands/gusto.png', 'brands/komus.png', 'brands/kulinarium2.png', 'brands/vkusperekus.png', 'brands/andy.png', 'brands/stolcafe.png']
  -
    direction: left
    images: ['brands/befit.png', 'brands/family2.png', 'brands/gastronomist.png', 'brands/HABEGU.png', 'brands/parusa.png']
---