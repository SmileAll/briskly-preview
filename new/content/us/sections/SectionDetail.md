---
title: Briskly Micromarket  in&nbsp;numbers
items:
  -
    title: Initial investments
    description: Micromarket, Briskly pasting, Lock control unit, Surveillance camera, Temperature sensors, Door opening sensors, Application connection <a href="https://app.briskly.online/" target="_blank" class="link">B-Pay</a>, personal account Briskly Business, product integration.
    price: 5000 $
  -
    title: Turnover per month
    description: When placing a kiosk in the office of a company with a staff of 130 people or more.
    price: 1750 $
  -
    title: Service commission Briskly
    description: 'The service includes: built-in acquiring, cloud checkout, sending checks to the OFD and the client, technical support, support for discounts and promo codes. <br><br> As well as support for the Briskly Business personal account and the courier - CHECK-ER application.'
    price: 135 $ per month
  -
    title: Payback period
    description: On average, it takes one month to find a location and launch.
    price: from 3 months
  -
    title: Net profit
    description: What is the possible income? It all depends on the location and the margin on the assortment.
    price: 680 $
---