---
title: We produce micromarkets on&nbsp;<span class="has-text-primary">our own factory</span> near Finland border, in&nbsp;Vyborg city
description: The history of the ex-Helkama refrigerators factory counts 105 years. Since 1916, Finn Heikki Helkama produced household appliances here under the Helkama brand. In 2021, Briskly acquired the factory from the "Trust Bank", and established a full cycle of production of smart micromarkets.
video: 'https://www.youtube.com/watch?v=NZVEUzD-zzE'
image: multi-fridge.png
preview: player.jpg
---