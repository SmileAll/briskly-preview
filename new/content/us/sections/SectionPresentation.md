---
title: We'll send the presentation and write you back.
description: Get all now, decide later. <br> Leave your phone if you prefer to contact via WhatsApp. 
preview: 'youtube-presentation.png'
video: 'https://www.youtube.com/watch?v=wHyvyqfE0vo'
about: Smart сooler is a new way to sell fresh food in public areas.
---

<script data-b24-form="inline/49/xhgvzk" data-skip-moving="true">
	(function(w,d,u){
		var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://cdn-ru.bitrix24.ru/b6486705/crm/form/loader_49.js');
</script>