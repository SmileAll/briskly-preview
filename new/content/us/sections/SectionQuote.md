---
quote: Briskly's mission is to help people bring closer the future in which people are doing more valuable things than waiting in queues.
person:
  image: quote-ceo.png
  name: Gleb Kharitonov
  position: Founder Briskly
---