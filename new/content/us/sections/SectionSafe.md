---
title: Safe <br> sales
list:
  - Сamera records all operations in the micromarket
  - Suspicious transactions are seen in the business dashboard
  - Users are identified by phone and card
  - In case of a fraud, money is withdrawn from the card at the request of the owner of the micromarket
items:
  -
    title: Less than&nbsp;2%
    description: of goods are picked <br> without payment
    image: section-safe-image-1.png
  -
    title: 0,3% fraud 
    description: Exist in safe locations like offices.
    image: section-safe-image-2.png
  -
    title: 120°
    description: viewing angle of camera
    image: section-safe-image-3.png

gallery:
  - 'gallery-safe/safe-full-0.jpg'
  - 'gallery-safe/safe-full-1.jpg'
  - 'gallery-safe/safe-full-2.jpg'
  - 'gallery-safe/safe-full-3.jpg'
  - 'gallery-safe/safe-full-4.jpg'
  - 'gallery-safe/safe-full-5.jpg'
  - 'gallery-safe/safe-full-6.jpg'
  - 'gallery-safe/safe-full-7.jpg'
  - 'gallery-safe/safe-full-9.jpg'
thumbs:
  - 'gallery-safe/safe-preview-0.png'
  - 'gallery-safe/safe-preview-1.png'
  - 'gallery-safe/safe-preview-2.png'
  - 'gallery-safe/safe-preview-3.png'
  - 'gallery-safe/safe-preview-4.png'
  - 'gallery-safe/safe-preview-5.png'
  - 'gallery-safe/safe-preview-6.png'
  - 'gallery-safe/safe-preview-7.png'
  - 'gallery-safe/safe-preview-9.png'
---
