---
note: The equipment we produce
title: We produce equipment from scratch
description: We assemble micromarkets and refrigerators on the conveyor,<br>check the quality and ship to European and CIS countries
link: 'https://linnafrost.ru/catalog'
button: To the catalog
items:
  -
    title: 2,4 hectares
    description: the area of the plant in Vyborg
    image: section-scale-point.png
  -
    title: 18 000
    description: coolers per year — production capacity
    image: section-scale-micromarket.png
  -
    title: 54 types
    description: of the equipment are produced
    image: section-scale-tools.png
---