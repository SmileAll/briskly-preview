---
title: Any doubts? <br> <span class="has-text-primary">Look, who has already succeeded:</span>
note: 'Read'
items:
  -
    image: 'slider-first-image-kazan.jpg'
    title: 'Ayrat Kashapov in Kazan, Russia'
    content: 'Quit his job and now expands his chain of micromarkets.'
    note: ''
    link: 'https://www.instagram.com/p/CQ-7ynwFnhH/'
  -
    image: 'slider-first-image-orenburg.jpg'
    title: 'Ruslan Mukharyamov in Orenburg, Russia'
    content: 'I made mistakes with the choice of goods and locations, but then I made the right decision and increased my income.'
    note: ''
    link: 'https://www.instagram.com/p/CSodm14pqbA/'
  -
    image: 'slider-first-image1.jpg'
    title: 'Micromarkets "Tasty Point” in Irkutsk'
    content: 'Natalia Selenkova and Anna Loginova took a chance and tried a new business'
    note: ''
    link: 'https://www.instagram.com/p/CMKJRfKlKzF/'
  -
    image: 'slider-first-image2.jpg'
    title: 'Vladimir Kazantsev in Barnaul'
    content: 'Vladimir sold the cleaning company and plunged headlong into autonomous trading'
    note: ''
    link: 'https://www.instagram.com/p/CK83wI2laCi/'
  -
    image: 'slider-first-image4.jpg'
    title: 'Chef Alexey Gulyakin in Surgut, Russia'
    content: 'He closed the restaurant and opened a micromarket chain.'
    note: ''
    link: 'https://www.instagram.com/p/CEO8uJqjwg7/'
  -
    image: 'slider-first-image3.jpg'
    title: 'Anton Ulitin in St. Petersburg, Russia'
    content: 'In the past - a gas station employee, and today - the owner of a network of 6 micromarkets'
    note: ''
    link: 'https://www.instagram.com/p/CJLim2hlhU2/'
  -
    image: 'greenbox.png'
    title: 'Green Box in St. Petersburg, Russia'
    content: 'The chain of healthy food cafes has already put 60 micromarkets in the offices.'
    note: ''
    link: 'https://www.instagram.com/p/CQOiboUImZr/'
  -
    image: 'av.png'
    title: 'Azbuka Vkusa in Moscow'
    content: 'Premium supermarket is trying a new format. Office workers are happy.'
    note: ''
    link: 'https://av.ru/lp/micromarket/'
  -
    image: 'fermabot.png'
    title: '"Farm Microbots" "Klyuchevskoe" in Moscow'
    content: 'Is it possible to sell farm products in micro markets? Need to!'
    note: ''
    link: 'https://www.instagram.com/p/COPwmtxlizO/'
---