---
items:
  -
    note: Step 1
    title: The buyer <br> enters the store
    image: section-steps-step-1.png
  -
    note: Step 2
    title: Scans <br> barcodes
    image: section-steps-step-2.png
  -
    note: Step 3
    title: Pays for <br> goods
    image: section-steps-step-3.png
---