---
title: Business Dashboard
button: Request a demo
link: '#section-feedback'
---

<p class="content has-content-margin is-light has-text-secondary">
  Manage all micromarkets through Briskly Business account. 
  <br><br>
  It contains all analytics about sales, storage and goods.
  You will find out the amount of checks, popular goods, best promotions and other info, that will help you to increase your sales.
  <br><br>
  Add new products and manage promotions
  in a few clicks. Create promocodes for new customers and send push notifications to users.
  <br><br>
  Use promo-materials from the "Goods" section to launch a local campaign and attract the first customers.
</p>