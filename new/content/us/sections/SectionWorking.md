---
title: How does it work?
items:
  -
    title: '1. Choose the format'
    content: 'What kind of store will it be? I can be one micromarket or a coffee point with freezers and a smart coffee machine.'
    image: 'section-working-step1.png'
  -
    title: '2. Choose goods'
    content: You don't have to be a food producer. You can choose a third-party supplier who will take over the production. We will help you with the choice.
    image: 'section-working-step2.png'
  -
    title: '3. Choose a location'
    content: 'Micromarket works better where potential customers are — for example, in business centers.'
    image: 'section-working-step3.png'
  -
    title: '4. Decide how to get payments'
    content: 'Our customers buy products using the B-Pay app, but you can install a POS-terminal on the door.'
    image: 'section-working-step4.png'
  -
    title: '5. Sign the contract with Briskly'
    content: 'We will fill in the data about the legal entity to enable acquiring, determine the location and timing of installation, accept payment on the invoice and ship the micromarket.'
    image: 'section-working-step5.png'
  -
    title: '6. Launch it!'
    content: 'Micromarkets will be delivered to the specified address, and you will load the goods into it. We recommend you to promote the micromarket actively during the first days in order to attract future regular customers.'
    image: 'section-working-step6.png'
---