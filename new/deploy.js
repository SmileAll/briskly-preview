/* eslint-disable no-path-concat */
/* eslint-disable no-console */
const dotenv = require('dotenv')
const FtpDeploy = require('ftp-deploy')
const ftpDeploy = new FtpDeploy()

dotenv.config()

const config = {
  user: process.env.FTP_USER,
  password: process.env.FTP_PASSWORD,
  host: process.env.FTP_HOST,
  port: process.env.FTP_PORT,
  localRoot: __dirname + '/dist',
  remoteRoot: '/',
  include: ['*', '**/*'],
  deleteRemote: true,
  sftp: false
}

console.log(`ℹ Run deploy to ${process.env.FTP_HOST}`)

ftpDeploy
  .deploy(config)
  .then(res => console.log('✔ Finished:', res))
  .catch(err => console.log('Error:', err))
