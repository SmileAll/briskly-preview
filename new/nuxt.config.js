export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    htmlAttrs: {
      lang: 'ru'
    },
    title: 'Бизнес на микромаркетах Briskly | Умные автономные холодильники',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Начните продавать свои товары или готовые блюда от поставщиков в умных микромаркетах, которые работают без персонала. Старт — от 99 000 рублей.' },
      { hid: 'keywords', name: 'keywords', content: 'брискли, briskly, сканируй и покупай, касса в смартфоне, микромаркет, scan&go, приложение b-pay, брискли онлайн, магазин без кассира, микромаркеты briskly, умный холодильник, магазины без кассира, briskly, киоск самообслуживания, микромаркет, вендинг автоматы, вендинговые автоматы, вендинговые аппараты, торговый автомат, автомат в торговом центре, торговый аппарат, вендинговая продажа, новые бизнес идеи, открыть новый бизнес, куда можно вложить деньги, франшиза под ключ, бизнес с быстрой окупаемостью, лучшие бизнес идеи, купить вендинговый, b pay, b-pay, брискли, еда в микромакете' },
      { name: 'apple-mobile-web-app-title' },
      { name: 'application-name' },
      { name: 'msapplication-TileColor' },
      { name: 'theme-color', content: '#333333' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', sizes: '32x32', href: 'favicon-32x32.png' },
      { rel: 'icon', type: 'image/x-icon', sizes: '16x16', href: 'favicon-16x16.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: 'apple-touch-icon-new.png' },
      { rel: 'manifest', href: 'site.webmanifest' },
      { rel: 'mask-icon', href: 'safari-pinned-tab.svg', color: '#5fda3e' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/main.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    { src: '~/plugins/scroll.client.js', mode: 'client' },
    { src: '~/plugins/splide.client.js', mode: 'client' },
    { src: '~/plugins/loopSlider.client.js', mode: 'client' },
    { src: '~/plugins/vue-lazyload.js', mode: 'client' }
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxt/content',
    '@nuxtjs/i18n'
  ],
  hooks: {
    'content:file:beforeParse': (file) => {
      const lang = file.path.match(/content\/([^/]+)\//)[1] // Get lang from path file
      const locale = lang === 'ru' ? '' : `${lang}/`

      file.data = file.data.replace(/{lang}\//g, `${locale}`)
    }
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    vendor: ['Splide']
  },
  router: {
    base: '/'
  },
  content: {
    liveEdit: false
  },
  i18n: {
    defaultLocale: 'ru',
    locales: [
      {
        code: 'us',
        name: 'US',
        iso: 'en-US',
        file: 'en-US.json'
      },
      {
        code: 'eu',
        name: 'EU',
        iso: 'en-EU',
        file: 'en-EU.json'
      },
      {
        code: 'ru',
        name: 'RU',
        iso: 'ru-RU',
        file: 'ru-RU.json'
      },
      {
        code: 'kz',
        name: 'KZ',
        iso: 'ru-KZ',
        file: 'ru-KZ.json'
      },
      {
        code: 'ua',
        name: 'UA',
        iso: 'ru-UA',
        file: 'ru-UA.json'
      },
      {
        code: 'by',
        name: 'BY',
        iso: 'ru-BY',
        file: 'ru-BY.json'
      }
    ],
    langDir: 'locales/',
    vueI18n: {
      fallbackLocale: 'ru'
    },
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      alwaysRedirect: true,
      redirectOn: 'root'
    }
  }
}
