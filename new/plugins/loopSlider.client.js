export default (_context, inject) => {
  const loopSlider = (slider, direction, time = 20, key = 'loopSlider') => {
    const widthSlider = `${slider.getBoundingClientRect().width}px`
    const speedSlider = (slider.getBoundingClientRect().width / 10) / time
    slider.style.animation = `${speedSlider}s loop-${key} linear infinite`
    const sliderDublicate = slider.parentNode.appendChild(slider.cloneNode(true))

    if (direction === 'right') {
      slider.style.right = 0
      sliderDublicate.style.right = `-${widthSlider}`
    } else {
      slider.style.left = 0
      sliderDublicate.style.left = `-${widthSlider}`
    }

    let dynamicStyles = null

    const addAnimation = (body) => {
      if (!dynamicStyles) {
        dynamicStyles = document.createElement('style')
        document.head.appendChild(dynamicStyles)
      }
      dynamicStyles.sheet.insertRule(body, dynamicStyles.length)
    }

    addAnimation(`
      @keyframes loop-${key} {
        0% {
          transform: translate(0, 0);
        }
        100% {
          transform: translate(${direction === 'right' ? `-${widthSlider}` : widthSlider}, 0);
        }
      }
    `)
  }
  inject('loopSlider', loopSlider)
}
