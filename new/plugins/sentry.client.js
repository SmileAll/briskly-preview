import Vue from 'vue'
import * as Sentry from '@sentry/vue'
import { Integrations } from '@sentry/tracing'

Sentry.init({
  Vue,
  dsn: 'https://366e9fd882cb46239a0e68a2dce6b753@o1096157.ingest.sentry.io/6116406',
  integrations: [
    new Integrations.BrowserTracing({
      tracingOrigins: [/^\//]
    })
  ],
  tracesSampleRate: 1.0
})
