import Splide from '@splidejs/splide'
import '@splidejs/splide/dist/css/themes/splide-default.min.css'

export default (_context, inject) => {
  inject('Splide', Splide)
}
