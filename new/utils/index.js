export function sectionsMap (sections) {
  const output = {}

  sections.forEach((element) => {
    output[element.slug] = element
  })

  return output
}
