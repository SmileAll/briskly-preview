<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
// $version = '300820-1';
$title = 'Briskly - тарифы';
?>
<?php require_once $path . '/views/common/head.tpl'; ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-tariffs-page">
        <div class="wrapper-block">
            <span class="s18 semibold">Тарифы, действующие с 30.05.2020:</span>
            <h3>«Комиссия с транзакции до 8%»</h3>
            <p class="s14 bold">Что включено?</p>
            <p>Касса самообслуживания в смартфоне покупателя.</p>
            <p>Отправка чеков в ОФД.</p>
            <p>Стандартная интеграция ассортимента товаров.</p>
            <p>Техническая поддержка покупателей.</p>
            <p>Добавление акций и скидок.</p>
            <p>Личный кабинет в Briskly Business Back Office (BBO)</p>
        </div>
    </div>
    <div class="full-width footer-height">
        <img class="bend bend-bottom" src="/img/bend-images/map-bend-bottom.svg" alt="">
        <?php require_once $path . '/views/common/footer.tpl'; ?>
    </div>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
