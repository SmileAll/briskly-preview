<?php
$path = \dirname(__FILE__, 2);
$version = '300820-1';
$title = 'Briskly - тарифы';
?>
<?php require_once $path . '/views/common/head.tpl'; ?>

<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-tariffs-page">
        <div class="wrapper-block">
            <h3>Сколько это стоит?</h3>
            <p>Покупать сразу на&nbsp;год или&nbsp;два выгоднее: вы&nbsp;получаете скидку до&nbsp;40%</p>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="wrapper-block tariffs-wrapper">
            <div class="table-wrapper-1">
                <div class="table-wrapper">
                    <img class="two-fingers" src="/img/two-fingers-icon.svg" />
                    <div class="tr-block title-tr">
                        <div class="td-block first-td-block td-gray">
                            <div class="prices-menu">
                                <ul class="prices-menu-list">
                                    <li data-prices="0,1 990,5 990,11 990" class="js-prices-menu"><span>Оплата за 1 мес.</span></li>
                                    <li data-prices="0,10 800,31 800,64 200" class="js-prices-menu selected"><span>Оплата за 6 мес.</span></li>
                                    <li data-prices="0,15 600,49 200,99 600" class="js-prices-menu"><span>Оплата за год</span></li>
                                    <li data-prices="0,23 760,84 000,170 400" class="js-prices-menu"><span>Оплата за два года</span></li>
                                </ul>
                            </div>
                            <span class="only-mobile">Тарифы</span>
                        </div>
                        <div class="td-block td-green"><span>Бесплатный</span></div>
                        <div class="td-block td-light-blue"><span>Старт</span></div>
                        <div class="td-block td-blue"><span>Бизнес</span></div>
                        <div class="td-block td-red"><span>Корпорация</span></div>
                    </div>
                    <div class="tr-block special-colors td-border-bottom">
                        <div class="td-block first-td-block td-gray">
                            <span class="semibold">Количество</span>
                        </div>
                        <div class="td-block td-green"><span>до 3 точек</span></div>
                        <div class="td-block td-light-blue"><span>до 5 точек</span></div>
                        <div class="td-block td-blue"><span>до 20 точек</span></div>
                        <div class="td-block td-red"><span style="font-size: 40px; font-family: Arial;">&infin;</span></div>
                    </div>
                    <div class="tr-block special-colors">
                        <div class="td-block first-td-block td-gray"><span class="semibold">Сервисный сбор <span class="nowrap">B-pay</span></span></div>
                        <div class="td-block td-green"><span>5%</span></div>
                        <div class="td-block td-light-blue"><span>5%</span></div>
                        <div class="td-block td-blue"><span>5%</span></div>
                        <div class="td-block td-red"><span>5%</span></div>
                    </div>
                    <div class="tr-block standart-view border-top-none prices-line">
                        <div class="td-block first-td-block"><span class="semibold">Стоимость</span></div>
                        <div class="td-block"><span class="price js-price" data-price="0">0 ₽</span></div>
                        <div class="td-block"><span class="price js-price" data-price="1">10 800 ₽</span></div>
                        <div class="td-block"><span class="price js-price" data-price="2">31 800 ₽</span></div>
                        <div class="td-block"><span class="price js-price" data-price="3">64 200 ₽</span></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="semibold have-icon">Приложение с&nbsp;функцией Scan&Go</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Для любой торговой точки: магазина, микромаркета, кафе, АЗС и&nbsp;даже онлайн-магазина.</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Техподдержка покупателей</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Мы&nbsp;отвечаем покупателям в&nbsp;чате B-Pay и&nbsp;помогаем решать проблемы</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Кассовый чек (автоматом передается в ОФД), поддержка ФЗ-54</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Вы&nbsp;можете подключить свою кассу начиная с&nbsp;тарифа &laquo;Старт&raquo;</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Эквайринг</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Система лояльности и&nbsp;накопления баллов</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">В&nbsp;бесплатном тарифе&nbsp;&mdash; только наша собственная система. Свою можно внедрить на&nbsp;тарифе &laquo;Корпорация&raquo;</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Витрина товаров в&nbsp;приложении с&nbsp;возможностью самовывоза</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Подключить доставку можно с&nbsp;тарифа &laquo;Старт&raquo;</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Приложение <span class="nowrap">ЧЕК-ЕР</span> для контроля покупателей и&nbsp;сбора заказов на&nbsp;самовывоз</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block">
                            <span>Инструкция по&nbsp;запуску проекта в&nbsp;магазине</span>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Макеты промо-материалов для&nbsp;запуска</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Инструкции, флайеры, ролл-апы, презентации, оклейка для&nbsp;микромаркетов</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span class="semibold">Бизнес-кабинет для&nbsp;владельца</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Полный каталог товаров</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Сводный отчёт по&nbsp;продажам</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Отчётность в&nbsp;личном кабинете или&nbsp;на&nbsp;e-mail</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Интеграция с&nbsp;товароучетной системой</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">1C, iiko, СуперМаг, МТС-касса или в&nbsp;форме excel-таблиц. Если у&nbsp;вас своя система, её&nbsp;мы&nbsp;интегрируем с&nbsp;тарифа &laquo;Старт&raquo;</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Интеграция с&nbsp;1С</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>ФЗ-152 (о&nbsp;перс. данных)</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Разграничение прав доступа ваших сотрудников</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>История действий всех сотрудников на&nbsp;аккаунте</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Полностью функциональное REST API</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Возможность подключить свою кассу</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Портрет клиента</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Клиентская база</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Отдельная вкладка в&nbsp;кабинете со&nbsp;списком клиентов и&nbsp;информацией об&nbsp;их&nbsp;покупках в&nbsp;сети.</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Система скидок</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Можно собирать готовые скидочные программы по&nbsp;шаблону. Например, &laquo;Скидка после 20:00&nbsp;&mdash; 30%&raquo;. Индивидуальные скидки доступны на&nbsp;тарифе &laquo;Корпорация&raquo;.</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Промокоды</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Стимулирующие акции 1+1</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Если в&nbsp;вашем магазине работает scan&amp;go, то&nbsp;в&nbsp;чате сканирования мы&nbsp;рассказываем об&nbsp;акциях.</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Сбор отзывов от&nbsp;покупателей</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">В&nbsp;чате поддержки спросим покупателя, что ему понравилось или проведем голосование или зададим любой другой вопрос.</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="semibold have-icon semibold">Доставка</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">С&nbsp;вариантами выбора стоимости доставки. Насчитывается автоматически. Возможна установка фиксированной цены.</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Приложение <span class="nowrap">ЧЕК-ЕР</span> для курьера микромаркета и&nbsp;для сбора заказа на&nbsp;доставку</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Чат с&nbsp;клиентом для вопросов по&nbsp;сбору заказа</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">Возможность выбора облачной кассы</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">АТОЛ, Orange DATA и&nbsp;другие</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Интеграция вашей системы лояльности </span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Интеграция с&nbsp;товароучетной системой, которая не&nbsp;входит в&nbsp;стандартный список</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Возможность выбора облачной кассы или&nbsp;интеграция своего решения</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Возможность создавать отделы и&nbsp;филиалы в&nbsp;бизнес-кабинете</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span class="semibold">Маркетинг:</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">&mdash; Push-уведомления</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Своим клиентам. Есть ограничения по&nbsp;количеству.</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>&mdash;&nbsp;E-mail и&nbsp;sms-рассылки</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">&mdash; Возврат клиентов </span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Покажем, какие клиенты давно не&nbsp;покупали в&nbsp;вашем магазине и&nbsp;подскажем, как их&nbsp;вернуть</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">&mdash;&nbsp;Анонсы магазине в&nbsp;Stories приложения и&nbsp;в&nbsp;SMM-каналах нашей компании</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Вы&nbsp;сможете добавлять свои сториз в&nbsp;приложение, которые показываются в&nbsp;зависимости от&nbsp;геолокации ваших точек. Наши соц. сети&nbsp;&mdash; бонусом :)</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block js-mobile-click">
                            <div class="have-explanatory-note-block">
                                <span class="have-icon">&mdash; Геймификация</span>
                                <div class="explanatory-note-block">
                                    <div class="explanatory-note-block-text">Например, &laquo;Купи 3&nbsp;чашки кофе три дня подряд и&nbsp;получи 4-ю в&nbsp;подарок&raquo;</div>
                                </div>
                            </div>
                        </div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Складской учет (Документы)</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Учёт остатков</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>SDK для создания своего приложения на&nbsp;базе нашего. Для&nbsp;ios&nbsp;/ android</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block standart-view">
                        <div class="td-block first-td-block"><span>Документация для&nbsp;разработчиков</span></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/absence-marker.svg" alt="" /></i></div>
                        <div class="td-block"><i><img src="/img/tariffs-page/check-mark.svg" alt="" /></i></div>
                    </div>
                    <div class="tr-block btns-fixed-position">
                        <div class="td-block first-td-block"></div>
                        <div class="td-block"><a href="https://backoffice.briskly.online/company-registration/" target="_blank" class="btn btn-big btn-spec-green">Подключить</a></div>
                        <div class="td-block"><a href="https://backoffice.briskly.online/company-registration/" target="_blank" class="btn btn-big btn-blue">Подключить</a></div>
                        <div class="td-block"><a href="https://backoffice.briskly.online/company-registration/" target="_blank" class="btn btn-big btn-light-blue">Подключить</a></div>
                        <div class="td-block"><a href="https://backoffice.briskly.online/company-registration/" target="_blank" class="btn btn-big btn-red">Подключить</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width footer-height">
        <img class="bend bend-bottom" src="/img/bend-images/map-bend-bottom.svg" alt="">
        <?php require_once $path . '/views/common/footer2.tpl'; ?>
    </div>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>