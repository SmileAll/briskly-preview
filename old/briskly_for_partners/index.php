<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Стать партнёром Briskly | Дистрибьюторам и дилерам';
$description = 'Станьте представителем Briskly в своем регионе: зарабатывайте на продаже микромаркетов и подключайте магазины и кафе к системе Scan&Pay.';
$keywords = 'брискли, briskly, сканируй и покупай, касса в смартфоне, микромаркет, scan&go, приложение b-pay, брискли онлайн, магазин без кассира, микромаркеты briskly, умный холодильник, магазины без кассира, briskly, киоск самообслуживания, микромаркет, вендинг автоматы, вендинговые автоматы, вендинговые аппараты, торговый автомат, автомат в торговом центре, торговый аппарат, вендинговая продажа, новые бизнес идеи, открыть новый бизнес, куда можно вложить деньги, франшиза под ключ, бизнес с быстрой окупаемостью, лучшие бизнес идеи, купить вендинговый, b pay, b-pay, брискли, еда в микромакете';
require_once $path . '/views/common/head.tpl';
?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-for-partners">
        <div class="w660 absolute-position">
            <div class="logo-top">
                <a href="/"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала" /></a>
            </div>
            <div class="text-block">
                <h1>Партнёрская программа</h1>
                <h4>Постройте новый бизнес с&nbsp;Briskly. Подключайте магазины и&nbsp;рестораны и&nbsp;получайте регулярный&nbsp;доход</h4>
                <ul class="standart marker-abroad">
                    <li>С&nbsp;Briskly магазины и&nbsp;рестораны увеличивают чек на&nbsp;15%&nbsp;и&nbsp;обслуживают в&nbsp;два раза больше клиентов</li>
                    <li>Покупатели оплачивают товары и&nbsp;заказывают блюда в&nbsp;приложении. Без очереди, без персонала, 24/7</li>
                </ul>
                <div class="btns-block">
                    <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Стать партнером</a>
                    <a href="tel:+78006005096" class="tel btn btn-big btn-green-border full-green btn300">8-800-600-50-96</a>
                </div>
                <a href="#feedback" class="see-more to-feedback">Подробности ниже</a>
            </div>
        </div>
        <div class="img-block-responsive">
            <div class="image-wrapper">
                <img class="main-image" src="/img/affiliate-program/content-images/leaf-photo.png" alt="Партнёрская программа Briskly, франшиза B-Pay." />
                <div class="phone-image-block">
                    <img src="/img/phone-screens/screen5.png" alt="" />
                    <img class="phone-frame" src="/img/phone-frame-dark.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dots-bottom-under-div">
        <div class="top-form-wrapper">
            <div class="wrapper-block">
                <div class="flex-wrapper center-items">
                    <div class="flex-block left-position" id="feedback">
                        <h3 class="color-white">Отправим презентацию на почту или перезвоним</h3>
                        <p class="s18 color-white">Получите всё сейчас, решите потом</p>
                        <div class="form-block-2">
                            <?php require_once $path . '/views/common/forms/21.tpl'; ?>
                        </div>
                    </div>
                    <div class="flex-block">
                        <div class="image-wrapper">
                            <div class="phone-image-block have-icon">
                                <img src="/img/phone-screens/screen6.png" alt="" />
                                <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                <img class="b-pay-icon" src="/img/icon-bpay-new.svg" alt="">
                            </div>
                            <span class="s24 color-white">Помогайте ресторанам и&nbsp;магазинам обслуживать в&nbsp;2&nbsp;раза больше клиентов благодаря приложению B-Pay</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dots"></div>
    </div>
    <div class="full-width light-gray-bg first-bend-block on-affiliate-program">
        <div class="wrapper-block">
            <div class="flex-wrapper">
                <div class="flex-block vertical-flex left-position">
                    <h2>Как это работает?</h2>
                    <p>Briskly&nbsp;&mdash; платформа самообслуживания магазинов и&nbsp;ресторанов, которая увеличивает количество заказов на&nbsp;30%, дает рост среднего чека на&nbsp;15% и&nbsp;избавляет покупателей от&nbsp;ожидания в&nbsp;очередях.</p>
                    <p>Это возможно с&nbsp;помощью приложения B-Pay, в&nbsp;котором торговая точка организует оплату товаров и&nbsp;заказов со&nbsp;смартфона. Покупатель сканирует товары или добавляет их&nbsp;из&nbsp;меню и&nbsp;оплачивает через приложение.</p>
                </div>
                <div class="flex-block">
                    <div class="work-steps-wrapper vertical-view">
                        <div class="work-step-block">
                            <div class="icon-block">
                                <img class="green-shadow" src="/img/icons/restaurant-icon-new.svg" alt="" />
                            </div>
                            <div class="text-block">
                                <span class="s16 bold">Покупатель выбирает ресторан или магазин в&nbsp;приложении</span>
                            </div>
                        </div>
                        <div class="work-step-block">
                            <div class="icon-block">
                                <img class="green-shadow" src="/img/icons/barcode-icon-new.svg" alt="" />
                            </div>
                            <div class="text-block">
                                <span class="s16 bold">Сканирует товары или выбирает блюда в&nbsp;меню</span>
                            </div>
                        </div>
                        <div class="work-step-block">
                            <div class="icon-block">
                                <img class="green-shadow" src="/img/icons/payment-icon-new.svg" alt="" />
                            </div>
                            <div class="text-block">
                                <span class="s16 bold">Добавляет в&nbsp;корзину и&nbsp;оплачивает заказ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="">
    </div>
    <div class="full-width revenue-growth-block-2-wrapper">
        <div class="wrapper-block">
            <div class="revenue-growth-block-2">
                <h3 class="color-green align-center">Рост среднего чека на&nbsp;15%, <br />в&nbsp;два раза больше покупателей</h3>
                <div class="flex-wrapper">
                    <div class="flex-block">
                        <div class="text-block white-bg br-10 box-sh-gray">
                            <span class="s18 semibold">Выгода торговой точки</span>
                            <ul class="standart">
                                <li>Возможность делать предзаказ увеличивает количество обслуживаемых клиентов на&nbsp;30-50%</li>
                                <li>Электронная очередь позволяет принимать в&nbsp;2&nbsp;раза больше заказов и&nbsp;работать в&nbsp;два раза быстрее</li>
                                <li>Клиенты не&nbsp;уходят из-за долгого ожидания</li>
                                <li>Рост среднего чека на&nbsp;15% за&nbsp;счет акций и&nbsp;скидок в&nbsp;приложении</li>
                                <li>Разгрузка кассира и&nbsp;официантов в&nbsp;часы пик</li>
                                <li>Трафик растёт за&nbsp;счёт информирования аудитории приложения о&nbsp;новом заведении</li>
                            </ul>
                        </div>
                    </div>
                    <div class="flex-block">
                        <div class="text-block white-bg br-10 box-sh-gray">
                            <span class="s18 semibold">Выгода клиента</span>
                            <ul class="standart">
                                <li>Больше не&nbsp;нужно ждать официанта и&nbsp;стоять в&nbsp;очереди на&nbsp;кассе</li>
                                <li>Всегда работает оплата картой или Apple Pay за&nbsp;счет эквайринга, встроенного в&nbsp;приложение</li>
                                <li>Сумму заказа сразу видно на&nbsp;экране с&nbsp;учетом скидок и&nbsp;акций, что позволяет потратить весь заплланированный бюджет</li>
                                <li>Информация о&nbsp;готовности заказа появляется в&nbsp;виде push-уведомления</li>
                                <li>Всегда позитивный клиентский опыт без человеческого фактора</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block placements-wrapper swiper-container js-placements-block">
            <div class="placements-block have-page-dots bottom-dots swiper-wrapper">
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i1.jpg" alt="" />
                        <div class="text-block">
                            <h5>Фудкорты на фестивалях</h5>
                            <p>Помогите фестивалям, рейвам, open air и&nbsp;концертам избавиться от&nbsp;очередей.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i2.jpg" alt="" />
                        <div class="text-block">
                            <h5>Кафе, бары и&nbsp;рестораны</h5>
                            <p>Во&nbsp;время бизнес-ланча, в&nbsp;загруженный день, в&nbsp;час пик&nbsp;&mdash; Briskly спасёт от&nbsp;долгого ожидания.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i3.jpg" alt="" />
                        <div class="text-block">
                            <h5>Магазины у дома</h5>
                            <p>Избавьте магазин, супермаркет или любую торговую точку от&nbsp;очередей.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i4.jpg" alt="" />
                        <div class="text-block">
                            <h5>Стадионы <br />и&nbsp;event-площадки</h5>
                            <p>Помогите организовать обслуживание без&nbsp;официантов и&nbsp;очередей.</p>
                        </div>
                    </div>
                </div>
            </div>
            <h3 class="color-green align-center">Без&nbsp;ожидания. Без&nbsp;очередей. Без&nbsp;ошибок</h3>
            <a href="#map" class="btn btn-green-border full-green btn-big center-position">Смотреть карту магазинов Briskly</a>
        </div>
    </div>
    <!--<div class="full-width connection-cost-wrapper">
        <img class="bend" src="/img/bend-images/bend1_1.svg" alt="">
        <div class="wrapper-block">
            <h3 class="align-center">Стоимость подключения к&nbsp;Briskly для торговой точки</h3>
            <div class="flex-wrapper js-connection-cost have-page-dots bottom-dots">
                <div class="carousel-cell flex-block white-bg br-10 box-sh-gray-mini">
                    <span class="cost color-green bold">25 000 рублей</span>
                    <p>Основатель Briskly Глеб Харитонов рассказал Rusbase о&nbsp;создании магазинов на&nbsp;базе технологии Scan&amp;Go, где нет касс и&nbsp;очередей, а&nbsp;безопасность обеспечивает нейросеть. Инвесторы оценивают технологию в&nbsp;$10&nbsp;млн</p>
                </div>
                <div class="carousel-cell flex-block white-bg br-10 box-sh-gray-mini">
                    <span class="cost color-green bold">8% с транзакций</span>
                    <p>Основатель Briskly Глеб Харитонов рассказал Rusbase о&nbsp;создании магазинов на&nbsp;базе технологии Scan&amp;Go, где нет касс и&nbsp;очередей, а&nbsp;безопасность обеспечивает нейросеть. Инвесторы оценивают технологию в&nbsp;$10&nbsp;млн</p>
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend7.svg" alt="">
    </div>
    <div class="full-width dark-bg about-benefit-wrapper">
        <div class="wrapper-block">
            <div class="about-benefit-top">
                <h3 class="color-white align-center">Выгода для партнёра</h3>
                <span class="h3 color-green align-center bold align-center">Подключение торговых точек и&nbsp;доход с&nbsp;%&nbsp;с&nbsp;транзакций</span>
            </div>
            <h4 class="color-white bold">Ваш доход</h4>
            <div class="js-benefits benefits-block swiper-container">
                <div class="swiper-wrapper">
                    <div class="flex-wrapper swiper-slide">
                        <div class="flex-block narrow-block br-10">
                            <img src="/img/affiliate-program/content-images/benefit-1.jpg" alt="" />
                            <div class="text-block green-bg">
                                <h4 class="bold">С транзакций</h4>
                                <p class="s18">Гости платят через приложение, а&nbsp;вы&nbsp;получаете фиксированный транзакционный доход</p>
                                <span class="fs50 bold">%</span>
                            </div>
                        </div>
                        <div class="flex-block white-bg br-10">
                            <div class="text-block">
                                <h4 class="bold">Паушальный взнос партнёра</h4>
                                <p class="s18">Единоразовый взнос для партнёров программы в&nbsp;момент заключения договора. Стоимость платежа рассчитывается из&nbsp;численности населения вашего города.</p>
                                <a href="#feedback" class="btn btn-green-border full-green btn-medium to-feedback">Запросить расчет</a>
                            </div>
                        </div>
                    </div>
                    <div class="flex-wrapper swiper-slide">
                        <div class="flex-block narrow-block br-10">
                            <img src="/img/affiliate-program/content-images/benefit-2.jpg" alt="" />
                            <div class="text-block green-bg">
                                <h4 class="bold">За интеграцию мерчанта</h4>
                                <p class="s18">окупка лицензии на&nbsp;платформу Briskly обойдется заведению в&nbsp;25&nbsp;000&nbsp;₽, которые мы&nbsp;вам перечисляем.</p>
                                <span class="h3 bold">до 25 000 ₽</span>
                            </div>
                        </div>
                        <div class="flex-block white-bg br-10">
                            <div class="text-block">
                                <h4 class="bold">Окупаемость</h4>
                                <p class="s18">При подключении торговой точки вам перечисляется лицензионный взнос в&nbsp;размере 25&nbsp;000&nbsp;₽. Вы&nbsp;можете самостоятельно регулировать сумму взноса в&nbsp;меньшую сторону.</p>
                                <span class="h3 bold">2-3 месяца</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-green btn-big center-position btn300">Стать партнером</a>
        </div>
    </div>-->
    <div class="full-width light-gray-bg about-commission">
        <img class="bend bend-top" src="/img/bend-images/bend10-bottom-2.svg" alt="">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h3>В&nbsp;стоимость&nbsp;транзакционной комиссии&nbsp;для клиентов Briskly входит:</h3>
                    <ul class="standart">
                        <li>Эквайринг</li>
                        <li>Онлайн-касса с&nbsp;отправкой чеков в&nbsp;ОФД и&nbsp;клиенту</li>
                        <li>Маркетинг</li>
                        <li>Техническая поддержка</li>
                        <li>E-mail, Push уведомления пользователям</li>
                    </ul>
                    <!--<a href="#" class="standart-gray">Изучить лендинг по&nbsp;подключению к&nbsp;платформе</a>-->
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper">
                        <img src="/img/affiliate-program/content-images/image1.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend8-bottom.svg" alt="">
    </div>
    <div class="full-width white-bg connection-steps-wrapper">
        <div class="wrapper-block">
            <h3 class="align-center">Три шага для подключения торговой точки:</h3>
            <div class="connection-steps">
                <div class="step-item">
                    <img class="green-shadow" src="/img/affiliate-program/steps-icons/step1-icon.svg" alt="" />
                    <h4 class="bold">Регистрация заведения в&nbsp;партнёрском кабинете</h4>
                    <p>Зайдите в&nbsp;кабинет на&nbsp;start.briskly.online и&nbsp;заведите ваш магазин, ресторан, кафе, фудкорт в&nbsp;качестве нового партнёра.</p>
                </div>
                <div class="step-item">
                    <img class="blue-shadow" src="/img/affiliate-program/steps-icons/step2-icon.svg" alt="" />
                    <h4 class="bold">Заполнение анкету заведения в&nbsp;кабинете</h4>
                    <p>Заполните данные партнёра&nbsp;&mdash; реквизиты, тип торговой точки, категории, товары.</p>
                </div>
                <div class="step-item">
                    <img class="purple-shadow" src="/img/affiliate-program/steps-icons/step3-icon.svg" alt="" />
                    <h4 class="bold">Запуск заведения в&nbsp;приложении B-Pay</h4>
                    <p>Внесите информацию о&nbsp;меню, добавьте информационные буклеты на&nbsp;столики, проинструктируйте персонал о&nbsp;том, как&nbsp;работает B-Pay</p>
                </div>
            </div>
            <a href="#my-form" class="btn btn-green btn-big center-position btn300">Стать партнером</a>
        </div>
    </div>
    <div class="full-width light-gray-bg for-partners">
        <img class="bend bend-top" src="/img/bend-images/bend9.svg" alt="">
        <div class="wrapper-block">
            <h3 class="align-center">Мы предоставляем партнерам</h3>
            <ul class="standart two-columns-view">
                <li>Обучение по&nbsp;условиям продукта</li>
                <li>Кабинет на&nbsp;платформе Briskly для подключения торговых точек и&nbsp;отслеживания статистики</li>
                <li>Промо-материалы для эффективных продаж</li>
                <li>Вы&nbsp;регулируете стоимость взноса за&nbsp;подключение, чтобы вы&nbsp;могли делать скидки</li>
            </ul>
        </div>
    </div>


    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top-gray.svg" alt="" />
        <?php require_once $path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom-white.svg" alt="" />
    </div>


    <div class="full-width str-after-wrapper">
        <div class="str-after">
            <div>
                <span class="s24 middle align-center">С Briskly будущее становится ближе и перемещается в смартфон!</span>
                <img src="/img/str-green.svg" alt="" />
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $path . '/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $path . '/views/elems/work-steps-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/views/elems/map2.tpl'; ?>
    <?php require_once $path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
