<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Микромаркеты Briskly | Умные автономные холодильники';
$description = 'Умный холодильник от Briskly, где можно продавать продукты и готовую еду. Открывается через приложение B-Pay. Установи микромаркет в своем городе и начни зарабатывать! От 99000₽.';
$keywords = 'Брискли, сканируй и покупай, микромаркет, умный холодильник, вендинг, микромаркет азбука вкуса, микромаркет greenbox, scan&go, магазин без кассира, магазин без персонала, микромаркет в подъезде';
require_once $path . '/views/common/head.tpl';
?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-micromarket-page">
        <div class="top-block-left-spec-bg">
            <div class="w660 absolute-position">
                <div class="logo-top">
                    <a href="/"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала" /></a>
                </div>
                <div class="text-block">
                    <h1>Микромаркет Briskly <br />с вашими товарами <br />под ключ за <span class="nowrap">99 000 руб.</span></h1>
                    <ul class="standart marker-abroad">
                        <li>Готовая точка сбыта вашей продукции</li>
                        <li>Оплата картой или через приложение</li>
                    </ul>
                    <div class="btns-block">
                        <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Подать заявку</a>
                    </div>
                    <a href="#feedback" class="see-more to-feedback">Подробности ниже</a>
                </div>
            </div>
            <div class="img-block-responsive">
                <div class="image-wrapper">
                    <img class="main-image max-height" src="/img/М4_1.png" alt="Умный холодильник от Briskly открывается через приложение B-Pay, магазин без кассира" />
                </div>
            </div>
        </div>
    </div>
    <a name="form" id="form"></a>
    <div class="form-block-wrapper">
        <div class="form-block-wrapper-special-bg">
            <div class="wrapper-block" id="feedback">
                <h2 class="color-white">Отправим презентацию на почту <br />или&nbsp;перезвоним</h2>
                <div class="flex-wrapper color-white">
                    <div class="flex-block left-position">
                        <h4 class="middle">Получите всё сейчас, решите потом</h4>
                        <p>Оставьте телефон, если хотите получить фин. модель бизнеса микромаркетов</p>
                        <div class="form-block-1">
                            <?php require_once $path . '/by/views/common/forms/47.tpl'; ?>
                        </div>
                    </div>
                    <div class="flex-block">
                        <div class="video-block">
                            <a href="https://youtu.be/wHyvyqfE0vo" class="fancybox" data-fancybox>
                                <img src="/img/micro-market/content-images/video-preview1.jpg" alt="" />
                                <img class="video-icon" src="/img/play-icon.svg" alt="" />
                            </a>
                        </div>
                    </div>
                    <div class="flex-block full-width">
                        <p class="s18">Умные холодильники&nbsp;&mdash; новый способ поставки свежей готовой еды в&nbsp;офис, бизнес-центр или любое другое место.</p>
                        <p class="s18">За&nbsp;99&nbsp;000&nbsp;руб.&nbsp;вы&nbsp;получите готовый холодильный киоск, оплата в&nbsp;котором работает через приложение.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="dots"></div>
    </div>
    <div class="gallery-wrapper">
        <div class="wrapper-block">
            <h3>#briskly <span class="color-light-gray">#нуженбрискли #brisklyэтобудущее</span></h3>
        </div>
        <div class="swiper-container js-micromarket-gallery">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img1_small.jpg">
                        <img src="/img/micro-market/gallery-images/img1.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img2_small.jpg">
                        <img src="/img/micro-market/gallery-images/img2.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img3_small.jpg">
                        <img src="/img/micro-market/gallery-images/img3.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img4_small.jpg">
                        <img src="/img/micro-market/gallery-images/img4.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img5_small.jpg">
                        <img src="/img/micro-market/gallery-images/img5.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img6_small.jpg">
                        <img src="/img/micro-market/gallery-images/img6.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img7_small.jpg">
                        <img src="/img/micro-market/gallery-images/img7.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img8_small.jpg">
                        <img src="/img/micro-market/gallery-images/img8.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img9_small.jpg">
                        <img src="/img/micro-market/gallery-images/img9.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img12_small.jpg">
                        <img src="/img/micro-market/gallery-images/img12.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img13_small.jpg">
                        <img src="/img/micro-market/gallery-images/img13.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img14_small.jpg">
                        <img src="/img/micro-market/gallery-images/img14.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img15_small.jpg">
                        <img src="/img/micro-market/gallery-images/img15.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img16_small.jpg">
                        <img src="/img/micro-market/gallery-images/img16.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img17_small.jpg">
                        <img src="/img/micro-market/gallery-images/img17.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img18_small.jpg">
                        <img src="/img/micro-market/gallery-images/img18.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img19_small.jpg">
                        <img src="/img/micro-market/gallery-images/img19.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img20_small.jpg">
                        <img src="/img/micro-market/gallery-images/img20.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img21_small.jpg">
                        <img src="/img/micro-market/gallery-images/img21.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img22_small.jpg">
                        <img src="/img/micro-market/gallery-images/img22.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img23_small.jpg">
                        <img src="/img/micro-market/gallery-images/img23.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img24_small.jpg">
                        <img src="/img/micro-market/gallery-images/img24.jpg" alt="" />
                    </picture>
                </div>
            </div>
        </div>
    </div>
    <div class="for-whom-wrapper">
        <div class="wrapper-block">
            <a name="forwho" id="forwho"></a>
            <h2 class="align-center">Для кого бизнес микромаркетов?</h2>
            <div class="flex-wrapper mobile-reverse for-whom">
                <div class="flex-block for-image left-position">
                    <div class="dots green-dots"></div>
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 767px)" srcset="/img/micro-market/content-images/wide-icebox-new.jpg">
                        <img src="/img/micro-market/content-images/wide-icebox-new.png" alt="" />
                    </picture>
                </div>
                <div class="flex-block">
                    <div class="for-whom-block">
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/chef-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Фабрики кухни и&nbsp;производители <br />готовой еды</h4>
                                <p>Развивайте свой бизнес, подключайте новые каналы сбыта через микромаркеты.</p>
                            </div>
                        </div>
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/restaurant-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Рестораны и кафе</h4>
                                <p>Развивайте вашу сеть с&nbsp;помощью нового формата. Кормите свежими обедами офисы и&nbsp;общественные учреждения.</p>
                            </div>
                        </div>
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/invest-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Владельцы вендингов</h4>
                                <p>Вкладывайте средства в&nbsp;понятную и&nbsp;прозрачную нишу с&nbsp;высокой доходностью. Минимум рисков благодаря отсутствию персонала.</p>
                            </div>
                        </div>
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/calculator-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Предприниматели и&nbsp;инвесторы</h4>
                                <p>Попробуйте себя в&nbsp;новой сфере&nbsp;&mdash; мы&nbsp;дадим все инструменты. Низкий порог входа&nbsp;&mdash; от&nbsp;99&nbsp;000&nbsp;руб.</p>
                            </div>
                        </div>
                        <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" class="standart-dark" target="_blank">Показать примеры</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="installation-options-wrapper">
        <div class="wrapper-block">
            <h2 class="align-center">Варианты установки</h2>
            <div class="installation-options-carousel swiper-container js-installation-options-block">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/box-new.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Установка системы в ваш холодильник</h4>
                                    <p>Установка эл. замка</p>
                                    <p>Подключение к приложению B-Pay</p>
                                    <p>Интеграция ассортимента</p>
                                    <p>Датчики температуры</p>
                                    <p>Камера наблюдения</p>
                                    <p>Обслуживание приложения B-Pay - 5000 р/мес.</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">63 000 ₽</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Подать заявку</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper spec-type">
                            <span class="popular">Популярный формат</span>
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micromarketnews2.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Умный холодильник «под ключ»</h4>
                                    <p>Микромаркет Briskly M4</p>
                                    <p>ДxШxВ: 595x600x2064 мм</p>
                                    <p>Электронный замок</p>
                                    <p>Подключение к приложению B-Pay</p>
                                    <p>Интеграция ассортимента</p>
                                    <p>Датчики температуры</p>
                                    <p>Камера наблюдения</p>
                                    <p>Обслуживание приложения B-Pay - 5000 р/мес.</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">99 000 ₽</span>
                                    <a href="#feedback" class="btn btn-green btn-big to-feedback">Подать заявку</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/mm_slide.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Большой киоск «под ключ»</h4>
                                    <p>Микромаркет Briskly M11</p>
                                    <p>ДxШxВ: 737x1195x2055 мм</p>
                                    <p>Электронный замок</p>
                                    <p>Подключение к приложению B-Pay</p>
                                    <p>Интеграция ассортиента</p>
                                    <p>Датчики температуры</p>
                                    <p>Камера наблюдения</p>
                                    <p>Обслуживание приложения B-Pay - 5000 р/мес.</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">132 000 ₽</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Подать заявку</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="align-center">Дополнительные модули:</h2>
            <div class="installation-options-carousel swiper-container js-installation-options-block-2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/coffee-machine.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Кофемашина</h4>
                                    <p>Заказ напитков через мобильное приложение B-Pay</p>
                                    <p>Настройка вашего ассортимента</p>
                                    <p>Встроенный эквайринг</p>
                                    <p>Техническая поддержка покупателей</p>
                                    <p>Онлайн-платформа для управления торговой точкой</p>
                                    <p>Обслуживание мобильного сервиса</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">169 000 ₽</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Подать заявку</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/freezer-new.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Морозильный шкаф</h4>
                                    <p>Темп. режим: от -18 до -24</p>
                                    <p>Интеграция с системой учёта</p>
                                    <p>Электронный замок</p>
                                    <p>Камеры наблюдения</p>
                                    <p>Мобильное приложение B-Pay</p>
                                    <p>Эквайринг</p>
                                    <p>Техническая поддержка</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">160 000 ₽</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Подать заявку</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/terminal-new2.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Терминал 2.0 для оплаты картой</h4>
                                    <p>Дополнительный терминал для оплаты без приложения</p>
                                    <p><strong>Включает:</strong></p>
                                    <p>Сканер штрихкодов товаров</p>
                                    <p>Приём банковских карт</p>
                                    <p>Эквайринг</p>
                                    <p>Выдача электронных чеков</p>
                                    <p>Техническая поддержка</p>
                                </div>
                                <div class="price">
                                    <!-- <span class="fs40 fatter service-cost">59 000 ₽</span> -->
                                    <p><strong>Только для РФ</strong></p>
                                    <!-- <a href="#feedback" class="btn btn-white btn-big to-feedback">Подать заявку</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a name="where" id="where"></a>
    <div class="places-of-use" style="padding-top: 0;">
        <div class="wrapper-block">
            <h2>Куда поставить микромаркет?</h2>
        </div>
        <div class="places-carousel swiper-container js-places-block">
            <div class="swiper-wrapper">
                <div class="swiper-slide first-elem-1"></div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img1_small.jpg">
                            <img src="/img/micro-market/places-images/img1.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>В офисы</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img2_small.jpg">
                            <img src="/img/micro-market/places-images/img2.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>В бизнес-центры</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img3_small.jpg">
                            <img src="/img/micro-market/places-images/img3.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>В торговые центры</span>
                            <div class="food-icons-active">
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                                <div class="food-icon non-food"></div>
                                <div class="food-icon freezing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img4_small.jpg">
                            <img src="/img/micro-market/places-images/img4.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>В производственные предприятия</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img5_small.jpg">
                            <img src="/img/micro-market/places-images/img5.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>В учебные заведения</span>
                            <div class="food-icons-active">
                                <div class="food-icon non-food"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img6_small.jpg">
                            <img src="/img/micro-market/places-images/img6.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>В спортивные клубы</span>
                            <div class="food-icons-active">
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img7_small.jpg">
                            <img src="/img/micro-market/places-images/img7.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>В административные учреждения</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snaks"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block">
            <ul class="food-types">
                <li>
                    <img src="/img/micro-market/food-icons/salad.svg" alt="" />
                    <span>Свежие блюда</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/sandwich.svg" alt="" />
                    <span>Обеды</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/freezing.svg" alt="" />
                    <span>Заморозка</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/coffee.svg" alt="" />
                    <span>Напитки</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/snacks.svg" alt="" />
                    <span>Снеки</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/non-food.svg" alt="" />
                    <span>Non-food</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="full-width">
        <img class="bend" src="/img/bend-images/bend10-top.svg" alt="">
        <a name="how" id="how"></a>
        <div class="white-bg">
            <div class="wrapper-block icebox-work-steps-wrapper">
                <h2 class="align-center">Как работает умный холодильник?</h2>
                <div class="icebox-work-steps-block">
                    <div class="work-step-block">
                        <img src="/img/micro-market/drawing-icons/icebox.svg" alt="" />
                        <h4 class="bold">Установка</h4>
                        <p class="s18">Ставим микромаркет в офисе, спортзале или любом другом месте. Нужен 1 кв. метр и розетка.</p>
                    </div>
                    <div class="work-step-block">
                        <img src="/img/micro-market/drawing-icons/girl.svg" alt="" />
                        <h4 class="bold">Загружаем товары</h4>
                        <p class="s18">Курьер ежедневно пополняет холодильник свежими товарами и забирает остатки. У него есть своё приложение с доступом.</p>
                    </div>
                    <div class="work-step-block">
                        <img src="/img/micro-market/drawing-icons/men.svg" alt="" />
                        <h4 class="bold">Перечисляем вам прибыль</h4>
                        <p class="s18">Деньги за проданные товары перечисляются на счёт вашей компании раз в сутки. Эквайринг включён в комиссию Briskly.</p>
                    </div>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend10-bottom.svg" alt="">
    </div>

    <a name="what" id="what"></a>
    <div class="offers-wrapper">
        <div class="wrapper-block">
            <h2 class="align-center">Что мы предлагаем</h2>
            <div class="offer-blocks-wrapper">
                <div class="offer-block">
                    <h4 class="bold">Приложение B-Pay</h4>
                    <ul class="standart">
                        <li>Показывает ближайший киоск</li>
                        <li>Открывает замок холодильника</li>
                        <li>Сканирует товары</li>
                        <li>Оплата картой или Apple Pay</li>
                        <li>Чек ОФД доступен в почте или приложении</li>
                    </ul>
                </div>
                <div class="offer-block">
                    <h4 class="bold">Холодильник под ключ</h4>
                    <ul class="standart">
                        <li>Блок управления замком</li>
                        <li>Модуль подключения к приложению и платформе</li>
                        <li>Холодильник МХМ, Linnafrost или другого производителя</li>
                        <li>Датчики температуры и влажности</li>
                        <li>Брендинг киоска</li>
                    </ul>
                </div>
                <div class="offer-block">
                    <h4 class="bold">Безопасные продажи</h4>
                    <ul class="standart">
                        <li>Камера наблюдения записывает все операции с холодильником</li>
                        <li>Сомнительные операции отображаются в специальном разделе</li>
                        <li>Пользователи идентифицируются по телефону и карте, дополнительно — по паспорту</li>
                        <li>В случае мошенничества деньги списываются с привязанной карты</li>
                    </ul>
                </div>
                <div class="offer-block">
                    <h4 class="bold">CRM, аналитика и маркетинг</h4>
                    <ul class="standart">
                        <li>Интеграция товарной матрицы с 1С, iiko или вручную (.xls, .csv)</li>
                        <li>Программа лояльности: скидки, акции, привязка карты</li>
                        <li>Отчёты о продажах и логирование событий</li>
                        <li>Расчёт рентабельности, популярности товаров</li>
                        <li>История подозрительных операций</li>
                        <li>Техническая поддержка пользователей</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="markets-in-num-wrapper">
        <div class="wrapper-block">
            <h2 class="align-center color-white">Микромаркет Briskly в цифрах</h2>
            <div class="flex-wrapper about-nums">
                <div class="flex-block">
                    <div class="about-nums-block">
                        <h4 class="bold">Стартовые инвестиции</h4>
                        <p><strong>Комплект под ключ</strong></p>
                        <p>Холодильник, блок управления замком, приложение B-Pay, камера наблюдения, датчики температуры и влажности, брендинг киоска</p>
                        <p>Поставщика еды мы предлагаем выбрать из наших партнёров-фабрик кухни. </p>
                        <span class="fs40 bold color-green">99 000 ₽</span>
                    </div>
                    <div class="about-nums-block">
                        <h4 class="bold">Вознаграждение Briskly</h4>
                        <p>с каждой транзакции</p>
                        <p><strong>Включает в себя</strong></p>
                        <p>Эквайринг, онлайн кассу, поддержку пользователей.</p>
                        <span class="fs40 bold color-green">5000 ₽/мес.</span>
                    </div>
                </div>
                <div class="flex-block">
                    <div class="about-nums-block">
                        <h4 class="bold">Оборот за месяц</h4>
                        <p>При размещении киоска в офисе компании численностью от 130 человек</p>
                        <span class="fs40 bold color-green">140 000 ₽</span>
                    </div>
                    <div class="about-nums-block">
                        <h4 class="bold">Срок окупаемости</h4>
                        <p>Без учёта затрат на логистику и товарное наполнение</p>
                        <span class="fs40 bold color-green">3 месяца</span>
                    </div>
                    <div class="about-nums-block">
                        <h4 class="bold">Чистая прибыль</h4>
                        <p><strong></strong></p>
                        <p>Среднее значение для одного киоска в месяц </p>
                        <span class="fs40 bold color-green">от 35 000 ₽</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block green-dots-decoration align-center color-white have-questions">
            <h2>Появились вопросы?</h2>
            <span class="s24 middle">Позвоните нашему менеджеру или получите презентацию на почту</span>
            <div class="btns-block">
                <a href="tel:+78006005096" class="btn btn-green btn-big btn300 tel">8-800-600-50-96</a>
                <a href="#feedback" class="btn btn-green-border btn-big btn300 to-feedback">Презентацию на почту?</a>
            </div>
        </div>
    </div>
    <div class="food-suppliers-wrapper">
        <div class="wrapper-block h-block">
            <h2>Нет своего производства <br />готовой еды?</h2>
            <p class="s24 middle">Выберите проверенного поставщика из числа наших партнёров</p>
        </div>
        <div class="full-width-block">
            <div class="dots"></div>
            <img class="absolute-position" src="/img/micro-market/content-images/bag.png" alt="" />
            <div class="swiper-container food-suppliers-carousel js-food-suppliers">
                <div class="swiper-wrapper">
                    <div class="swiper-slide first-elem-2"></div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://kylinarium.ru/" class="logo-block"><img src="/files/suppliers-logos/kulinarium.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Кулинариум</h4>
                                <p class="color-gray middle">Москва</p>
                            </div>
                            <p>Готовое производство еды в Москве</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="http://st-pk.ru/" class="logo-block"><img src="/files/suppliers-logos/slav-trapeza-new.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Славянская трапеза</h4>
                                <p class="color-gray middle">Москва</p>
                            </div>
                            <p>Готовое производство еды в Москве</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://www.justfood.pro/" class="logo-block"><img src="/files/suppliers-logos/just-food.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Just Food Pro</h4>
                                <p class="color-gray middle">Москва</p>
                            </div>
                            <p>Готовое производство еды в Москве</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://www.gyrmanov.com/" class="logo-block"><img src="/files/suppliers-logos/gurmanov.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Гурманов</h4>
                                <p class="color-gray middle">Москва</p>
                            </div>
                            <p>Готовое производство еды в Москве</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <div class="logo-block"><img src="/files/suppliers-logos/my-fruit.jpg" alt="" /></div>
                            <div class="supplier-name">
                                <h4 class="bold">MyFruit</h4>
                                <p class="color-gray middle">Москва и Санкт-Петербург</p>
                            </div>
                            <p>Производство свежих фруктовых десертов</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://cityfood.pro/" class="logo-block"><img src="/files/suppliers-logos/city-food.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">City food</h4>
                                <p class="color-gray middle">Москва</p>
                            </div>
                            <p>Производств сендвичей, салатов, сладкого</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://uppetit.ru/" class="logo-block"><img src="/files/suppliers-logos/uppetit.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Uppetit</h4>
                                <p class="color-gray middle">Санкт-Петербург</p>
                            </div>
                            <p>Готовое производство еды в Санкт-Петербурге</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://olimpfood.com/" class="logo-block"><img src="/files/suppliers-logos/olimpfood.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">olimpfood</h4>
                                <p class="color-gray middle">Нижний Новгород и Красноярск</p>
                            </div>
                            <p>Производство готовой еды в Нижнем Новгороде и Красноярске</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://naturovo.ru/" class="logo-block"><img src="/files/suppliers-logos/naturovo.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Натурово</h4>
                                <p class="color-gray middle">Калининград</p>
                            </div>
                            <p>Производство готовой еды в Калининграде</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="http://gastronomist.pro/" class="logo-block"><img src="/files/suppliers-logos/gastronomist.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Gastronomist</h4>
                                <p class="color-gray middle">Санкт-Петербург</p>
                            </div>
                            <p>Готовое производство еды в Санкт-Петербурге</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://myfiteat.ru/" class="logo-block"><img src="/files/suppliers-logos/myfiteat.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">My Fit Eat</h4>
                                <p class="color-gray middle">Санкт-Петербург</p>
                            </div>
                            <p>Производство готовой еды в Санкт-Петербурге</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <div class="logo-block"><img src="/files/suppliers-logos/supply.png" alt="" /></div>
                            <div class="supplier-name">
                                <h4 class="bold">Super Food</h4>
                                <p class="color-gray middle">Краснодар</p>
                            </div>
                            <p>Производство готовой еды в Краснодаре</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <div class="logo-block"><img src="/files/suppliers-logos/supply.png" alt="" /></div>
                            <div class="supplier-name">
                                <h4 class="bold">Mr.Food</h4>
                                <p class="color-gray middle">Москва</p>
                            </div>
                            <p>Производство готовой еды в Москве</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="http://obedov66.ru" class="logo-block"><img src="/files/suppliers-logos/obedov.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Обедов</h4>
                                <p class="color-gray middle">Екатеринбург</p>
                            </div>
                            <p>Производство готовой еды в Екатеринбурге</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom-white.svg" alt="" />
    </div>
    <div class="full-width str-after-wrapper">
        <div class="str-after">
            <div>
                <span class="s24 middle align-center">С Briskly будущее становится ближе и перемещается в смартфон!</span>
                <img src="/img/str-green.svg" alt="" />
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $path . '/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $path . '/views/elems/work-steps-b-pay.tpl'; ?>
        <?php require_once $path . '/views/elems/about-terminal.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/views/elems/map2.tpl'; ?>
    <?php require_once $path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
