<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Подключи доставку для своего магазина или кафе';
$description = 'Подключи заказ еды для ресторана и доставку продуктов для магазина через приложение B-Pay за 15-40 минут.';
$keywords = 'Брискли, сканируй и покупай, подключить доставку, приложение для магазина, приложение для ресторана, лояльность для ресторана, микромаркет, магазин без кассира, магазин без персонала, микромаркет в подъезде';
require_once $path . '/views/common/head.tpl';
?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-delivery-page">
        <div class="wrapper-block flex-wrapper">
            <div class="flex-block w50">
                <div class="logo-top">
                    <a href="/"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала" /></a>
                </div>
                <div class="text-block">
                    <h3>Подключим срочную доставку для&nbsp;вашего магазина или кафе за&nbsp;2&nbsp;дня</h3>
                    <ul class="standart marker-abroad">
                        <li>Онлайн-витрина ваших товаров по&nbsp;ссылке <br />и&nbsp;в&nbsp;приложении <span class="nowrap">B-Pay</span></li>
                        <li>Доставка курьером или на&nbsp;такси</li>
                    </ul>
                    <div class="btns-block">
                        <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Подключить</a>
                    </div>
                </div>
            </div>
            <div class="flex-block auto-width center-position">
                <img class="main-image" src="/img/preorder-delivery/content-images/car-phone-bag.png" alt="" />
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend1.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg first-bend-block on-preorder-and-delivery-page">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items tablet-wrap">
                <div class="flex-block" id="feedback">
                    <h3>Ваша собственная доставка <br />за&nbsp;два дня</h3>
                    <h4>Подключим к&nbsp;приложению для заказа товаров с&nbsp;аудиторией 50&nbsp;000&nbsp;покупателей. Предоставим ссылку на&nbsp;ваш собственный интернет-магазин. Крупнейшие партнёры по&nbsp;доставке уже встроены!</h4>
                    <div class="form-block white-bg br-20">
                        <p class="s18 semibold align-center">Сколько это стоит? Получите презентацию сейчас</p>
                        <div class="form-wrapper">
                            <?php require_once $path . '/views/common/forms/25.tpl'; ?>
                        </div>
                    </div>
                </div>
                <div class="flex-block right-position">
                    <ul class="standart marker-abroad">
                        <li>Добавим ваши товары в&nbsp;приложение <storng>B-Pay</storng></li>
                        <li>Предоставим аудиторию в&nbsp;<strong>50&nbsp;000</strong> человек</li>
                        <li>Примем заказы и&nbsp;дадим ссылку на&nbsp;онлайн-витрину с&nbsp;возможностью оплаты</li>
                        <li>Доставим товары за&nbsp;<strong>15-40</strong>&nbsp;минут с&nbsp;помощью надёжных партнёров</li>
                    </ul>
                    <h4><stroong>B-Pay</stroong>&nbsp;&mdash; ваш собственный <span class="nowrap">интернет-магазин</span> в&nbsp;приложении.</h4>
                    <h4>Подключим бесплатно. Оплата&nbsp;&mdash;&nbsp;%&nbsp;с&nbsp;проведенных транзакций.</h4>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="" />
    </div>
    <div class="full-width">
        <div class="wrapper-block preorder-process">
            <h3 class="align-center">Как выглядит процесс для&nbsp;покупателя?</h3>
            <div class="flex-wrapper center-items">
                <div class="flex-block">
                    <ol class="special-decor">
                        <li><span class="big-text">Клиент выбирает магазин или&nbsp;заведение</span></li>
                        <li><span class="big-text">Добавляет товары в&nbsp;корзину</span></li>
                        <li><span class="big-text">Оплачивает картой или&nbsp;Apple&nbsp;Pay</span></li>
                        <li><span class="big-text">Торговая точка собирает доставку</span></li>
                        <li><span class="big-text">Через 20&nbsp;минут приходит курьер, забирает заказ и&nbsp;отвозит покупателю</span></li>
                    </ol>
                </div>
                <div class="flex-block image-wrapper">
                    <div class="image-wrapper-left">
                        <img src="/img/preorder-delivery/content-images/phone.jpg" alt="" />
                        <a class="standart-green" href="http://brskl.io/av37" target="_blank">Смотреть пример магазина онлайн</a>
                    </div>
                    <div class="image-wrapper-right">
                        <a class="b-pay" href="https://b-pay.online/" target="_blank"><img src="/img/bpay-logo.svg" alt="Приложение B-Pay" /></a>
                        <p>Приложение <br />для быстрых покупок</p>
                        <div class="application-stores">
                            <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white-dark-border.svg" /></a>
                            <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white-dark-border.svg" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block delivery-organization-wrapper">
            <h3 class="align-center">Организуйте свою доставку <br />под ключ</h3>
            <ul class="delivery-organization">
                <li>
                    <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow">
                    <h4>Запустите свою доставку всего за&nbsp;2&nbsp;дня</h4>
                </li>
                <li>
                    <img src="/img/icons/setting-icon.svg" alt="" class="blue-shadow">
                    <h4>Бесплатная установка, интеграция и&nbsp;настройка</h4>
                </li>
                <li>
                    <img src="/img/icons/statistics-icon.svg" alt="" class="blue-shadow">
                    <h4>Увеличивайте оборот бизнеса за&nbsp;счет дополнительного канала продаж</h4>
                </li>
            </ul>
        </div>
    </div>
    <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block about-delivery">
            <div class="flex-wrapper about-delivery-top-block tablet-wrap center-items">
                <div class="flex-block">
                    <h3>Как это работает для <br />магазина или кафе?</h3>
                    <h4 class="semibold">Доставка ваших товаров за&nbsp;15-40 минут на&nbsp;базе платформы Briskly.</h4>
                    <h4>Круглосуточная доставка предоплаченных покупок. Возможна оплата на&nbsp;счет Briskly один раз в&nbsp;конце месяца</h4>
                </div>
                <div class="flex-block">
                    <div class="image-wrapper">
                        <img class="ml40" src="/img/preorder-delivery/content-images/image2.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="flex-wrapper about-delivery-block">
                <div class="flex-block left-position">
                    <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow" />
                    <span class="s18 fatter">Пешие курьеры</span>
                    <ul class="standart marker-abroad">
                        <li>Только в Москве в пределах ТТК</li>
                        <li>Фиксированная стоимость доставки</li>
                        <li>Заказы до 5 кг и до 80 см</li>
                    </ul>
                    <p class="color-light-gray">Пеший курьер в&nbsp;среднем прибывает за&nbsp;15-20&nbsp;минут, водитель&nbsp;&mdash; за&nbsp;7-10&nbsp;минут. Курьер сразу отправляется к&nbsp;покупателю.</p>
                </div>
                <div class="flex-block right-position">
                    <img src="/img/icons/car-icon.svg" alt="" class="purple-shadow" />
                    <span class="s18 fatter">Водители</span>
                    <ul class="standart marker-abroad">
                        <li>В более 700 городах, где есть Яндекс.Такси</li>
                        <li>В Москве — до 30 км от МКАД</li>
                        <li>Стоимость доставки рассчитывается автоматически</li>
                        <li>Заказы до 20 кг и до 180 см</li>
                    </ul>
                    <p class="color-light-gray">Вы&nbsp;можете включить оплату доставки целиком за&nbsp;счёт клиента, обозначить фиксированную стоимость или самостоятельно платить за&nbsp;доставку.</p>
                </div>
            </div>
            <div class="our-delivery-service">
                <ul>
                    <li>
                        <p style="color: #6d818c;">Интеграции:</p>
                        <img class="mts-logo" src="/img/mts-logo.svg" alt="" />
                        <p>а так же с 1с, iiko, r-keeper</p>
                    </li>
                    <li>
                        <p style="color: #6d818c;">Доставку осуществляет:</p>
                        <img src="/img/delivery-logos/yandex-taxi-logo.svg" alt="" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="wrapper-block delivery-options-wrapper">
            <h3 class="align-center">Три варианта работы с&nbsp;доставкой Briskly</h3>
            <div class="delivery-options-block">
                <div class="option-item">
                    <img src="/img/icons/delivery-option-1.svg" alt="" />
                    <span>Клиент оплачивает 100% доставки сам </span>
                </div>
                <div class="option-item modile-row-reverse">
                    <span>Клиент оплачивает доставку по&nbsp;фиксированной стоимости, остальное доплачивает компания</span>
                    <img src="/img/icons/delivery-option-2.svg" alt="" />
                </div>
                <div class="option-item">
                    <img src="/img/icons/delivery-option-3.svg" alt="" />
                    <span>Компания оплачивает доставку за&nbsp;клиента на&nbsp;100%</span>
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend3-bottom.svg" />
    </div>
    <div class="full-width checker-screens-wrapper">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h3>Для сборки заказа у&nbsp;вас будет приложение <span class="nowrap">&laquo;ЧЕК-ЕР&raquo;</span>.</h3>
                    <ul class="standart marker-abroad">
                        <li>Ваш сотрудник собирает заказ и&nbsp;отмечает это в&nbsp;приложении</li>
                        <li>Когда заказ собран, курьер получает уведомление и&nbsp;приезжает забирать заказ</li>
                        <li>Сотрудник отдаёт заказ курьеру</li>
                        <li>Курьер отвозит заказ клиенту</li>
                    </ul>
                </div>
                <div class="flex-block w40">
                    <div class="swiper-container js-phone-screens">
                        <div class="phone-screens-wrapper swiper-wrapper">
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen8.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen9.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen10.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"><img src="/img/control.svg" /></div>
                        <div class="swiper-button-next"><img src="/img/control.svg" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block preorder-block">
            <div class="flex-wrapper">
                <div class="flex-block w60">
                    <h3>Предзаказ</h3>
                    <h4>Клиенты делают предзаказ в&nbsp;телефоне и&nbsp;забирают блюдо в&nbsp;зоне выдачи</h4>
                    <ul class="standart marker-abroad">
                        <li>Ресторан подключается за&nbsp;1&nbsp;день</li>
                        <li>Не&nbsp;требуется дополнительное оборудование для старта</li>
                        <li>Экономит время покупателя</li>
                    </ul>
                    <h4 class="bottom-position">Покупатель может легко заказать еду в&nbsp;приложении&nbsp;<span class="nowrap">B-Pay</span></h4>
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper">
                        <img src="/img/preorder-delivery/content-images/image3.png" alt="Доставка еды из магазинов и кафе через приложение B-Pay от Брискли" />
                    </div>
                </div>
            </div>
            <div class="horizontal-special-decor-list">
                <ol class="special-decor gray-counter">
                    <li>
                        <h4 class="bold">Выбор заведения</h4>
                        <div>
                            <p>Выберите ресторан в&nbsp;списке, на&nbsp;карте или отсканируйте <span class="nowrap">QR-код</span></p>
                            <img src="/img/preorder-delivery/content-images/phone1-new.jpg" alt="" />
                        </div>
                    </li>
                    <li>
                        <h4 class="bold">Выбор товаров</h4>
                        <div>
                            <p>Добавьте блюда, которые вам выдадут на&nbsp;стойке или принесут</p>
                            <img src="/img/preorder-delivery/content-images/phone2-new.jpg" alt=""" />
                        </div>
                    </li>
                    <li>
                        <h4 class="bold">Оплата</h4>
                        <div>
                            <p>Оплатите картой или Apple&nbsp;Pay. Чек уходит на&nbsp;почту и&nbsp;в&nbsp;ОФД</p>
                            <img src="/img/preorder-delivery/content-images/phone3-new.jpg" alt=""" />
                        </div>
                    </li>
                </ol>
            </div>
            <div class="str-after on-scan-go">
                <div>
                    <span class="s24 semibold align-center">Быстро покупать, легко оплачивать</span>
                    <img src="/img/str-green.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top-gray.svg" alt="" />
        <?php require_once $path . '/views/elems/main-about-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom-gray.svg" alt="" />
    </div>
    <div class="full-width p85">
        <div class="wrapper-block">
            <h3 class="align-center">Частые вопросы</h3>
            <div class="questions-wrapper">
                <div class="question-block js-opened-text">
                    <span>Где именно сотрудник формирует заказ?</span>
                    <div class="text">
                        <p>Заказ формируется в&nbsp;приложении ЧЕК-ЕР, сотрудник может скачать его на&nbsp;собственный или&nbsp;рабочий телефон.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Что происходит, если в&nbsp;составе заказа один товар закончился? Как управлять изменением состава заказа?</span>
                    <div class="text">
                        <p>Вы&nbsp;можете заменить товар при сборке заказа и&nbsp;согласовать замену с&nbsp;клиентом до&nbsp;окончательной оплаты заказа.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Как сотрудник получает оповещение о&nbsp;заказе?</span>
                    <div class="text">
                        <p>Оповещение о&nbsp;новом заказе приходит в&nbsp;push-уведомлении приложения ЧЕК-ЕР. Или сотрудник может следить за&nbsp;появлением нового заказа в&nbsp;самом приложении.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Как возвращать оплату клиенту?</span>
                    <div class="text">
                        <p>Если вы&nbsp;уже подтвердили заказ и&nbsp;с&nbsp;клиента списались деньги, то&nbsp;покупатель может написать в&nbsp;техническую поддержку приложения. Специалисты службы поддержки согласуют решение с&nbsp;вами и&nbsp;вернут деньги клиенту за&nbsp;заказ.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>В&nbsp;какой момент с&nbsp;клиента списываются деньги?</span>
                    <div class="text">
                        <p>Деньги списываются после окончательной сборки заказа и&nbsp;согласования всех возможных изменений в&nbsp;составе с&nbsp;клиентом.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Как мы&nbsp;поймем, что деньги списались с&nbsp;клиента?</span>
                    <div class="text">
                        <p>В&nbsp;приложении ЧЕР-ЕР сотрудник получает уведомление о&nbsp;том, что деньги списались у&nbsp;клиента. Если уведомление не&nbsp;пришло, то&nbsp;сотрудник может связаться с&nbsp;клиентом и&nbsp;попросить привязать новую карту или пополнить счёт.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/views/elems/map2.tpl'; ?>
    <?php require_once $path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
