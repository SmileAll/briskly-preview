<div class="wrapper-block">
    <div class="spec-text">
        <h2 class="leaf-after">Встроенный маркетинг</h2>
        <p>На&nbsp;базе маркетинговой платформы мы&nbsp;накапливаем данные и&nbsp;анализируем посетителей с&nbsp;точки зрения покупательской активности и&nbsp;паттернов поведения. Данные клиентов собираются в&nbsp;профили, маркетинг становится системным.</p>
    </div>
</div>
<div class="screen-block wrapper-block">
    <img src="/img/main-page-images/bbo-screen.jpg" alt="" />
</div>