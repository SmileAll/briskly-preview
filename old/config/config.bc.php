<?php

//require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/config/i18n.php';

//if (isset($_SERVER['SERVER_SITE']) && $_SERVER['SERVER_SITE'] === 'briskly-online.loc') {
//    
//} else {
//    if (!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on") {
//        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], true, 301);
//        exit();
//    }
//}

if (!isset($_SERVER['HTTPS']) || !$_SERVER['HTTPS'] || $_SERVER['HTTPS'] === "off") {
    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $location);
    exit();
}

$version = '2.1.13';
$path = $_SERVER['DOCUMENT_ROOT'];
$url = parse_url(str_replace('/en/', '/', $_SERVER["REQUEST_URI"]), PHP_URL_PATH);

if ($active_code === 'RU') {
    $lang_path = $path;
} else {
    $lang_path = $path . '/' . strtolower($active_code);
}

