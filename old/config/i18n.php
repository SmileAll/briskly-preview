<?php

//use GeoIp2\Database\Reader;

$langs = [
    'RU' => '',
    'EU' => 'en',
    'US' => 'us',
    'UA' => 'ua',
    'KZ' => 'kz',
    'BY' => 'by',
];

$phones = [
    'RU' => '+7-800-600-50-96',
    'EU' => '+442045771195',
    'US' => '+13235405719',
    'UA' => '+7-800-600-50-96',
    'KZ' => '+7771-755-55-74',
    'BY' => '+7-800-600-50-96',
];

$langs_list = [
    'RU' => [
        'icon' => 'ru',
        'code' => '',
        'name' => 'RU',
        'active' => false,
        'phone' => '8-800-600-50-96',
        'phone_href' => '+78006005096',
    ],
    'EU' => [
        'icon' => 'eu',
        'code' => 'en',
        'name' => 'EU',
        'active' => false,
        'phone' => '+442045771195',
        'phone_href' => '+442045771195',
    ],
    'US' => [
        'icon' => 'us',
        'code' => 'us',
        'name' => 'US',
        'active' => false,
        'phone' => '+13235405719',
        'phone_href' => '+13235405719',
    ],
    'UA' => [
        'icon' => 'ua',
        'code' => 'ua',
        'name' => 'UA',
        'active' => false,
        'phone' => '8-800-600-50-96',
        'phone_href' => '+78006005096',
    ],
    'KZ' => [
        'icon' => 'kz',
        'code' => 'kz',
        'name' => 'KZ',
        'active' => false,
        'phone' => '+7771-755-55-74',
        'phone_href' => '+77717555574',
    ],
    'BY' => [
        'icon' => 'by',
        'code' => 'by',
        'name' => 'BY',
        'active' => false,
        'phone' => '8-800-600-50-96',
        'phone_href' => '+78006005096',
    ],
];

if (isset($_GET['location_code']) && isset($langs_list[$_GET['location_code']])) {
    setcookie("location_code", $_GET['location_code'], time() + 3600 * 24 * 365, "/", $_SERVER['HTTP_HOST'], true);
}

$active_code = isset($_GET['location_code']) && isset($langs_list[$_GET['location_code']]) ? $_GET['location_code'] : null;
if ($active_code === null) {
    if (isset($_COOKIE['location_code']) && isset($langs_list[$_COOKIE['location_code']])) {
        $active_code = $_COOKIE['location_code'];
        if ($active_code !== 'RU' && strpos($_SERVER['REQUEST_URI'], '/en') === FALSE && strpos($_SERVER['REQUEST_URI'], '/ua') === FALSE && strpos($_SERVER['REQUEST_URI'], '/kz') === FALSE && strpos($_SERVER['REQUEST_URI'], '/by') === FALSE && strpos($_SERVER['REQUEST_URI'], '/us') === FALSE) {
            $keys = array_keys($langs_list);
            foreach ($keys as $k) {
                $request_uri = str_replace('/' . $k['code'] . '/', '/', $_SERVER['REQUEST_URI']);
            }
            $location = 'https://' . $_SERVER['HTTP_HOST'] . '/' . $langs_list[$_COOKIE['location_code']]['code'] . $request_uri;
            header('Location: ' . $location);
            exit();
        }
    } else {

        if (strpos($_SERVER['REQUEST_URI'], '/en') === 0) {
            $active_code = 'EU';
        } elseif (strpos($_SERVER['REQUEST_URI'], '/ua') === 0) {
            $active_code = 'UA';
        } elseif (strpos($_SERVER['REQUEST_URI'], '/kz') === 0) {
            $active_code = 'KZ';
        } elseif (strpos($_SERVER['REQUEST_URI'], '/by') === 0) {
            $active_code = 'BY';
        } elseif (strpos($_SERVER['REQUEST_URI'], '/us') === 0) {
            $active_code = 'US';
        } else {
            $active_code = 'RU';
        }
    }
}
//if ($active_code === null) {
//    $reader = new Reader('/usr/local/share/GeoIP/GeoLite2-Country.mmdb');
//    $record = $reader->country($_SERVER['REMOTE_ADDR']);
//    if ($record->country->isoCode === 'RU' || $record->country->isoCode === 'KZ' || $record->country->isoCode === 'UA' || $record->country->isoCode === 'BY') {
//        $active_code = 'RU';
//    } elseif ($record->country->isoCode === 'US') {
//        $active_code = 'US';
//    } else {
//        $active_code = 'EU';
//    }
//}

$langs_list[$active_code]['active'] = true;

$new_lang_list = [];
$tmp_list = [];
foreach ($langs_list as $k => $v) {
    if ($v['active']) {
        $new_lang_list[$k] = $v;
    } else {
        $tmp_list[$k] = $v;
    }
}

$langs_list = array_merge($new_lang_list, $tmp_list);

$active_language = $active_code;
$active_path_code = strtolower($active_language);
$active_code = $active_code === 'RU' ? 'RU' : 'EN';

$header_phone_url = str_replace('-', '', $phones[$active_language]);
$header_phone = str_replace('+77', '~', $phones[$active_language]);
$header_phone = str_replace('+7', '8', $phones[$active_language]);
$header_phone = str_replace('~', '+77', $phones[$active_language]);

if (isset($_GET['tt']) && $_GET['tt']) {
    //$header_phone = '111';
}