<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Connect B-Pay app from Briskly';
$description = 'Connect your store to the B-Pay app, which scans products and helps pay on your smartphone. No queues.';
$keywords = 'Briskley, scan and buy, express checkout in a smartphone, micromarket, scan & go, delivery, shop without a cashier, shop without staff, retail of the future';
require_once $lang_path . '/views/common/head.tpl';
?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color on-scan-go">
        <?php require_once $lang_path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper scan-go-top-block">
        <div class="wrapper-block flex-wrapper">
            <div class="flex-block w660">
                <div class="logo-top">
                    <a href="/"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала" /></a>
                </div>
                <div class="text-block">
                    <h3>B-Pay app</h3>
                    <h3>Customer self-checkout app. Connect your store for free! </h3>
                    <ul class="standart marker-abroad">
                        <li>Customers scan the barcode and pay via smartphone </li>
                        <li>The store increases sales and reduces queues</li>
                        <li>No cashier. No queues. 24/7</li>
                    </ul>
                    <div class="btns-block">
                        <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Contact us</a>
                    </div>
                </div>
            </div>
            <div class="flex-block auto-width">
                <div class="phone-image-block">
                    <img src="/img/phone-screens/screen7.png" alt="" />
                    <img class="phone-frame" src="/img/phone-frame-gray-light.svg" alt="" />
                    <img class="icon-image" src="/img/icons/restaurant-icon-red.svg" alt="" />
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend1.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg first-bend-block on-scan-go">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items tablet-wrap" id="feedback">
                <div class="flex-block w40">
                    <div class="form-block white-bg br-20 left-alignment" id="my-form-1">
                        <p class="s18 semibold align-center">Get a presentation about our products now, decide later</p>
                        <div class="form-wrapper">
                            <?php require_once $path . '/views/common/forms/39.tpl'; ?>
                        </div>
                    </div>
                </div>
                <div class="flex-block w60">
                    <h3>What is <span class="nowrap">B-Pay App?</span> <br />Express-checkout in customer's smartphone.</h3>
                    <h4>Scan&Pay - is a feature of the B-Pay&nbsp;App, where cutomers  scan barcodes and pay by card or&nbsp;Apple&nbsp;Pay.</h4>
                    <p class="s18">Capabilities of the app:</p>
                    <ul class="standart marker-abroad">
                        <li>Check-ER - special app to control the customer's behavior</li>
                        <li>Checks are available in the app</li>
                        <li>Aquiring is included</li>
                        <li>Technical support</li>
                        <li>Marketing support - promocodes, offers, discounts</li>
                    </ul>
                </div>
            </div>
            <div class="flex-wrapper">
                <div class="flex-block pluses-for-you-wrapper">
                    <span class="s24 middle" style="letter-spacing: -1px;">Your store can sell up to 15% more goods and work 2 times faster. With no staff and no queues.</span>
                    <ul class="pluses-for-you">
                        <li>
                            <img src="/img/icons/chel×2-icon.svg" alt="" class="green-shadow" />
                            <p class="s18">Serve twice as many clients in the same time, without expanding staff and retail space.</p>
                        </li>
                        <li>
                            <img src="/img/icons/growth-icon.svg" alt="" class="blue-shadow" />
                            <p class="s18">Increase revenue by 15% in 3 months, without the cost of cashiers, security, work around the clock.</p>
                        </li>
                        <li>
                            <img src="/img/icons/compass-icon.svg" alt="" class="pink-shadow" />
                            <p class="s18">Get a new sales channel. Know what the customers wants and convince to spend more.</p>
                        </li>
                    </ul>
                </div>
                <div class="flex-block right-position">
                    <div class="image-wrapper">
                        <img src="/img/scan-go/content-images/image1.png" alt="Приложение B-Pay от Briskly помогает сканировать товары и полачивать их через смартфон." />
                    </div>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="" />
        <div class="to-video">
            <a href="#video" class="to-video">
                <span>See, <br/>how it works</span>
                <img src="/img/triangle.svg" alt="" />
            </a>
        </div>
        <img class="bend special" src="/img/bend-images/bend2-special.svg" alt="" />
    </div>
    <div class="full-width scan-go-work" id="video">
        <div class="wrapper-block">
            <h2 class="align-center">How Scan&Pay app works? </h2>
            <div class="video-wrapper">
                <div class="video-block">
                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/nhIEmHAKd4I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <!--<iframe width="100%" height="100%" src="https://www.youtube.com/embed/6YcvNIwSuDw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
                </div>
            </div>
            <div class="btns-block application-stores center-position">
                <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore"><img src="/img/appstore.svg" alt=""></a>
                <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="/img/googleplay.svg" alt=""></a>
            </div>
        </div>
        <div class="wrapper-block">
            <h3 class="align-center">App commission - 5%, including acquiring</h3>
            <h4 class="align-center semibold">We accept:</h3>
                <div class="flex-table-view">
                    <div class="table-tr">
                        <div class="table-cell">
                            <div class="logos-block js-z-index">
                                <img src="/img/pay_systems/sbp.svg" alt="" />
                            </div>
                            <span>СБП</span>
                        </div>
                    </div>
                    <div class="table-tr">
                        <div class="table-cell">
                            <div class="logos-block">
                                <img src="/img/pay_systems/visa2.svg" alt="" />
                                <img src="/img/pay_systems/mir2.svg" alt="" />
                                <img src="/img/pay_systems/mastercard2.svg" alt="" />
                            </div>
                            <span>credit/debit cards </span>
                        </div>
                    </div>
                    <div class="table-tr">
                        <div class="table-cell">
                            <div class="logos-block">
                                <img src="/img/pay_systems/apple-pay.svg" alt="" />
                            </div>
                            <span>Apple Pay</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="wrapper-block like-cashbox">
            <div class="flex-wrapper tablet-wrap">
                <div class="flex-block w60">
                    <h3>B-Pay is an express checkout and online store in the buyer's smartphone</h3>
                    <ul class="standart marker-abroad">
                        <li>Turn-key solution in 1 day.</li>
                        <li>No extra equipment to start.</li>
                        <li>Saves chekout time increases customers flow.</li>
                        <li>Low implementation cost.</li>
                        <li>Your distribution channel in the buyer's smartphone</li>
                    </ul>
                    <div class="flex-wrapper tab-no-wrap">
                        <div class="flex-block">
                            <span class="fs30 bold color-green">+15-19%</span>
                            <p class="s18">to the average purchase amount when paying in&nbsp;the application</p>
                        </div>
                    </div>
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper have-small-icon-blocks">
                        <img class="main-image" src="/img/scan-go/content-images/image3.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block about-delivery">
            <div class="flex-wrapper about-delivery-top-block tablet-wrap center-items">
                <div class="flex-block w60">


                    <div class="text-block">

                        <h3>We automatically integrate an&nbsp;assortment of&nbsp;goods from most common inventory systems into the&nbsp;application.</h3>
                        <!--<p>We automatically integrate an assortment of goods from most common inventory systems into the application.</p>-->
    <!-- <a href="#" class="standart-gray border-decor">Конкуренты</a> --> -->
    <!-- </div>

</div>
<div class="flex-block w40">
    <div class="image-wrapper">
        <img src="/img/scan-go/content-images/image2.png" alt="" />
    </div>
</div>
</div> -->



    <!--<div class="flex-wrapper about-delivery-block">

        <div class="flex-block right-position">
            <img src="/img/icons/car-icon.svg" alt="" class="purple-shadow" />
            <ul class="standart marker-abroad">
                <li>Shipping cost is calculated automatically</li>
                <li>Orders up to 20 kg and up to 180 cm</li>
            </ul>
            <p class="color-light-gray">You can include payment for delivery entirely at the expense of the client, indicate a fixed cost or pay for delivery yourself.</p>
        </div>
    </div>-->

    <!-- <div class="flex-wrapper about-delivery-block">
        <div class="flex-block left-position">
            <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow" />
            <span class="s18 fatter">Pedestrian couriers</span>
            <ul class="standart marker-abroad">
                <li>Fixed shipping cost</li>
                <li>Orders up to 5 kg and up to 80 cm</li>
            </ul>
            <p class="color-light-gray">On average, a pedestrian courier arrives in 15-20 minutes, a driver in 7-10 minutes. The courier is immediately sent to the buyer.</p>
        </div>
        <div class="flex-block right-position">
            <img src="/img/icons/car-icon.svg" alt="" class="purple-shadow" />
            <span class="s18 fatter">Drivers</span>
            <ul class="standart marker-abroad">
                <li>Shipping cost is calculated automatically</li>
                <li>Orders up to 20 kg and up to 180 cm</li>
            </ul>
            <p class="color-light-gray">You can include payment for delivery entirely at the expense of the client, indicate a fixed cost or pay for delivery yourself.</p>
        </div>
    </div>
    <div class="our-delivery-service">
        <ul>
            <li>
                <p style="color: #6d818c;">Integration:</p>
                <img class="mts-logo" src="/img/mts-logo.svg" alt="" />
            </li>
            <li>
                <p style="color: #6d818c;">Delivery is carried out by:</p>
                <img src="/img/delivery-logos/yandex-taxi-logo.svg" alt="" />
            </li>
        </ul>
    </div>
</div>
<img class="bend bend-bottom" src="/img/bend-images/bend3-bottom.svg" />
</div> -->
    <div class="full-width">
        <div class="wrapper-block about-discounts on-scan-go">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h2>Pioneered feature:<br />discounts and&nbsp;promotions <br /> at the time of purchase.</h2>
                </div>
                <div class="flex-block w40 align-center">
                    <img src="/img/main-page-images/discounts-phone.png" alt="Приложение B-Pay от Briskly - касса в смартфоне, сканируё и покупай" />
                </div>
            </div>
        </div>
        <!--<div class="wrapper-block about-discounts on-scan-go">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h3>Впервые на&nbsp;рынке: скидки&nbsp;и&nbsp;акции в&nbsp;момент покупки, в&nbsp;чате приложения.</h3>
                    <h4>В&nbsp;тот момент, когда это действительно нужно&nbsp;покупателю!</h4>
                    <p class="s18">Персональный подход к&nbsp;покупателю&nbsp;&mdash; больше не&nbsp;абстрактные слова.</p>
                    <p class="s18">Вы&nbsp;действительно знаете, что им&nbsp;нужно. Изучайте факты, которые платформа выявляет на&nbsp;основе анализа и&nbsp;сбора покупательских данных.</p>
                    <p class="s18">Предлагайте скидки и&nbsp;акции на&nbsp;базе доказанных поведенческих паттернов, а&nbsp;не&nbsp;догадок.</p>
                </div>
                <div class="flex-block w40 align-right">
                    <div class="phone-image-block">
                        <img src="/img/phone-screens/screen3.png" alt="" />
                        <img class="phone-frame" src="/img/phone-frame-dark.svg" alt="" />
                    </div>
                </div>
            </div>
        </div>-->
    </div>
    <div class="full-width light-gray-bg">
        <img src="/img/bend-images/bend3-top.svg" />
        <div class="embedded-marketing on-scan-go">
            <?php require_once $lang_path . '/views/elems/embedded-marketing.tpl'; ?>
        </div>
    </div>
    <div class="full-width">
        <img class="bend" src="/img/bend-images/bend5.svg" alt="" />
        <div class="wrapper-block included-in-price">
            <div class="flex-wrapper center-items tablet-wrap">
                <div class="flex-block w60">
                    <h3>Included in commission:</h3>
                    <ul class="standart marker-abroad s24">
                        <li>Security app with purchase control function - CHEK-EP</li>
                        <li>History of purchases</li>
                        <li>Built-in acquiring</li>
                        <li>Technical support</li>
                        <li>Loyalty program</li>
                    </ul>
                </div>
                <div class="flex-block w40 image-wrapper">
                    <img src="/img/main-page-images/briskly-components-new.svg" alt="" />
                </div>
            </div>
            <div class="str-after on-scan-go">
                <div>
                    <span class="s24 middle align-center">Quick to buy, easy to pay</span>
                    <img src="/img/str-green.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $lang_path . '/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $lang_path . '/views/elems/work-steps-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom-gray.svg" alt="" />
    </div>


    <div class="full-width p85">
        <div class="wrapper-block">
            <h3 class="align-center">FAQ</h3>
            <div class="questions-wrapper">
                <div class="question-block js-opened-text">
                    <span>How does the integration work?</span>
                    <div class="text">
                        <p>The first step is a contract and an invoice. You fill out an application for connection in your account. Then accept the offer and pay the bill.</p>
                        <p>The second is integration and launch. The integration of the assortment into the platform and into the application occurs automatically for the most common inventory systems. And also through the API. If you wish, you can fill in the items manually.</p>
                        <p>Next, we automatically integrate all products into the Check-ER application for cashiers and B-Pay for customers.</p>
                        <p>Once a day, the store receives the earned funds to the current account (minus the Briskly commission).</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Does the connection really take one day?</span>
                    <div class="text">
                        <p>If data about your products is stored in most common inventory systems then we can really connect you in one day.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $lang_path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $lang_path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $lang_path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $lang_path . '/views/elems/map2.tpl'; ?>
    <?php require_once $lang_path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
