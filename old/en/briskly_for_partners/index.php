<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Briskly App Franchise | Become a partner';
$description = 'Briskly affiliate program. Connect restaurants and shops and get income from each outlet. B-Pay franchise.';
$keywords = 'Briskly, become a partner, b-pay, micromarket, smart refrigerator, vending, micromarket azbuka vkusa, franchise, greenbox, store without a cashier, store without staff';
require_once $lang_path . '/views/common/head.tpl';
?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $lang_path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-for-partners">
        <div class="w660 absolute-position">
            <div class="logo-top">
                <a href="/"><img src="/img/logo-dark-new.svg" alt="Technological platform for unmanned trading" /></a>
            </div>
            <div class="text-block">
                <h1>Partner Program</h1>
                <h4>Build a new business with Briskly. Connect shops and restaurants to the app and get regular profit</h4>
                <ul class="standart marker-abroad">
                    <li>With Briskly, stores and restaurants increase their income by 15% and serve twice as many customers</li>
                    <li>Customers pay for goods and order meals in the app. No queue, no staff, 24/7</li>
                </ul>
                <div class="btns-block">
                    <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Become a partner</a>
                    <a href="tel:+78006005096" class="tel btn btn-big btn-green-border full-green btn300">8-800-600-50-96</a>
                </div>
                <a href="#feedback" class="see-more to-feedback">More info</a>
            </div>
        </div>
        <div class="img-block-responsive">
            <div class="image-wrapper">
                <img class="main-image" src="/img/affiliate-program/content-images/leaf-photo.png" alt="Партнёрская программа Briskly, франшиза B-Pay." />
                <div class="phone-image-block">
                    <img src="/img/phone-screens/screen5.png" alt="" />
                    <img class="phone-frame" src="/img/phone-frame-dark.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dots-bottom-under-div">
        <div class="top-form-wrapper">
            <div class="wrapper-block">
                <div class="flex-wrapper center-items">
                    <div class="flex-block left-position" id="feedback">
                        <h3 class="color-white">We will send the presentation by mail or call you back</h3>
                        <p class="s18 color-white">Get it all now, decide later</p>
                        <div class="form-block-2">
                            <?php require_once $path . '/views/common/forms/41.tpl'; ?>
                        </div>
                    </div>
                    <div class="flex-block">
                        <div class="image-wrapper">
                            <div class="phone-image-block have-icon">
                                <img src="/img/phone-screens/screen6.png" alt="" />
                                <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                <img class="b-pay-icon" src="/img/icon-bpay-new.svg" alt="">
                            </div>
                            <span class="s24 color-white">Help restaurants and shops serve 2x more customers with the B-Pay app</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dots"></div>
    </div>
    <div class="full-width light-gray-bg first-bend-block on-affiliate-program">
        <div class="wrapper-block">
            <div class="flex-wrapper">
                <div class="flex-block vertical-flex left-position">
                    <h2>How it works?</h2>
                    <p>Briskly is a self-service platform for stores and restaurants that increases the number of orders by 30%, increases the average check by 15% and eliminates the need for customers to wait in lines.</p>
                    <p>This is possible using the B-Pay app, in which the point of sale organizes payment for goods and orders from a smartphone. The buyer scans the products or adds them from the menu and pays through the app.</p>
                </div>
                <div class="flex-block">
                    <div class="work-steps-wrapper vertical-view">
                        <div class="work-step-block">
                            <div class="icon-block">
                                <img class="green-shadow" src="/img/icons/restaurant-icon-new.svg" alt="" />
                            </div>
                            <div class="text-block">
                                <span class="s16 bold">The buyer selects a restaurant or store in the app</span>
                            </div>
                        </div>
                        <div class="work-step-block">
                            <div class="icon-block">
                                <img class="green-shadow" src="/img/icons/barcode-icon-new.svg" alt="" />
                            </div>
                            <div class="text-block">
                                <span class="s16 bold">Scans products or selects dishes from the menu</span>
                            </div>
                        </div>
                        <div class="work-step-block">
                            <div class="icon-block">
                                <img class="green-shadow" src="/img/icons/payment-icon-new.svg" alt="" />
                            </div>
                            <div class="text-block">
                                <span class="s16 bold">Adds to cart and pays for the order</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="">
    </div>
    <div class="full-width revenue-growth-block-2-wrapper">
        <div class="wrapper-block">
            <div class="revenue-growth-block-2">
                <h3 class="color-green align-center">Average check growth by 15%, twice as many buyers</h3>
                <div class="flex-wrapper">
                    <div class="flex-block">
                        <div class="text-block white-bg br-10 box-sh-gray">
                            <span class="s18 semibold">Point of Sale Benefit</span>
                            <ul class="standart">
                                <li>The ability to pre-order increases the number of customers served by 30-50%</li>
                                <li>The electronic queue allows you to take 2 times more orders and work twice as fast</li>
                                <li>Customers don't leave because of long waiting times</li>
                                <li>Growth of the average check by 15% due to promotions and discounts in the application</li>
                                <li>Unloading cashier and waiters during rush hours</li>
                                <li>Traffic is growing due to informing the audience of the app in the new shop</li>
                            </ul>
                        </div>
                    </div>
                    <div class="flex-block">
                        <div class="text-block white-bg br-10 box-sh-gray">
                            <span class="s18 semibold">Customer benefit</span>
                            <ul class="standart">
                                <li>You no longer need to wait for a waiter and stand in line at the checkout</li>
                                <li>Payment by card or Apple Pay always works due to the acquiring built into the app</li>
                                <li>The order amount is immediately visible on the screen, taking into account discounts and promotions, which allows you to spend the entire planned budget</li>
                                <li>Order readiness information appears as a push notification</li>
                                <li>Always a positive customer experience without the human factor</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block placements-wrapper swiper-container js-placements-block">
            <div class="placements-block have-page-dots bottom-dots swiper-wrapper">
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i1.jpg" alt="" />
                        <div class="text-block">
                            <h5>Food courts at festivals</h5>
                            <p>Help festivals, raves, open air and concerts get rid of queues.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i2.jpg" alt="" />
                        <div class="text-block">
                            <h5>Cafes, bars and restaurants</h5>
                            <p>During a business lunch, on a busy day, at rush hour - Briskly will save you from a long wait.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i3.jpg" alt="" />
                        <div class="text-block">
                            <h5>Convenience stores</h5>
                            <p>Eliminate queues at your store, supermarket or any retail outlet.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="placements-item white-bg br-10 box-sh-gray-mini">
                        <img src="/img/affiliate-program/placements-images/i4.jpg" alt="" />
                        <div class="text-block">
                            <h5>Stadiums <br />and event venues</h5>
                            <p>Help organize service without waiters and queues.</p>
                        </div>
                    </div>
                </div>
            </div>
            <h3 class="color-green align-center">No waiting. No queues. No mistakes</h3>
            <a href="#map" class="btn btn-green-border full-green btn-big center-position">See the Briskly store map</a>
        </div>
    </div>
    <!--<div class="full-width connection-cost-wrapper">
        <img class="bend" src="/img/bend-images/bend1_1.svg" alt="">
        <div class="wrapper-block">
            <h3 class="align-center">Стоимость подключения к&nbsp;Briskly для торговой точки</h3>
            <div class="flex-wrapper js-connection-cost have-page-dots bottom-dots">
                <div class="carousel-cell flex-block white-bg br-10 box-sh-gray-mini">
                    <span class="cost color-green bold">25 000 рублей</span>
                    <p>Основатель Briskly Глеб Харитонов рассказал Rusbase о&nbsp;создании магазинов на&nbsp;базе технологии Scan&amp;Go, где нет касс и&nbsp;очередей, а&nbsp;безопасность обеспечивает нейросеть. Инвесторы оценивают технологию в&nbsp;$10&nbsp;млн</p>
                </div>
                <div class="carousel-cell flex-block white-bg br-10 box-sh-gray-mini">
                    <span class="cost color-green bold">8% с транзакций</span>
                    <p>Основатель Briskly Глеб Харитонов рассказал Rusbase о&nbsp;создании магазинов на&nbsp;базе технологии Scan&amp;Go, где нет касс и&nbsp;очередей, а&nbsp;безопасность обеспечивает нейросеть. Инвесторы оценивают технологию в&nbsp;$10&nbsp;млн</p>
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend7.svg" alt="">
    </div>
    <div class="full-width dark-bg about-benefit-wrapper">
        <div class="wrapper-block">
            <div class="about-benefit-top">
                <h3 class="color-white align-center">Выгода для партнёра</h3>
                <span class="h3 color-green align-center bold align-center">Подключение торговых точек и&nbsp;доход с&nbsp;%&nbsp;с&nbsp;транзакций</span>
            </div>
            <h4 class="color-white bold">Ваш доход</h4>
            <div class="js-benefits benefits-block swiper-container">
                <div class="swiper-wrapper">
                    <div class="flex-wrapper swiper-slide">
                        <div class="flex-block narrow-block br-10">
                            <img src="/img/affiliate-program/content-images/benefit-1.jpg" alt="" />
                            <div class="text-block green-bg">
                                <h4 class="bold">С транзакций</h4>
                                <p class="s18">Гости платят через приложение, а&nbsp;вы&nbsp;получаете фиксированный транзакционный доход</p>
                                <span class="fs50 bold">%</span>
                            </div>
                        </div>
                        <div class="flex-block white-bg br-10">
                            <div class="text-block">
                                <h4 class="bold">Паушальный взнос партнёра</h4>
                                <p class="s18">Единоразовый взнос для партнёров программы в&nbsp;момент заключения договора. Стоимость платежа рассчитывается из&nbsp;численности населения вашего города.</p>
                                <a href="#feedback" class="btn btn-green-border full-green btn-medium to-feedback">Запросить расчет</a>
                            </div>
                        </div>
                    </div>
                    <div class="flex-wrapper swiper-slide">
                        <div class="flex-block narrow-block br-10">
                            <img src="/img/affiliate-program/content-images/benefit-2.jpg" alt="" />
                            <div class="text-block green-bg">
                                <h4 class="bold">За интеграцию мерчанта</h4>
                                <p class="s18">окупка лицензии на&nbsp;платформу Briskly обойдется заведению в&nbsp;25&nbsp;000&nbsp;₽, которые мы&nbsp;вам перечисляем.</p>
                                <span class="h3 bold">до 25 000 ₽</span>
                            </div>
                        </div>
                        <div class="flex-block white-bg br-10">
                            <div class="text-block">
                                <h4 class="bold">Окупаемость</h4>
                                <p class="s18">При подключении торговой точки вам перечисляется лицензионный взнос в&nbsp;размере 25&nbsp;000&nbsp;₽. Вы&nbsp;можете самостоятельно регулировать сумму взноса в&nbsp;меньшую сторону.</p>
                                <span class="h3 bold">2-3 месяца</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-green btn-big center-position btn300">Стать партнером</a>
        </div>
    </div>-->
    <div class="full-width light-gray-bg about-commission">
        <img class="bend bend-top" src="/img/bend-images/bend10-bottom-2.svg" alt="">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h3>Our service for the clients includes:</h3>
                    <ul class="standart">
                        <li>Acquiring</li>
                        <li>Online checkout with sending checks to the OFD and to the client</li>
                        <li>Marketing</li>
                        <li>Technical support</li>
                        <li>E-mail, Push notifications to users</li>
                    </ul>
                    <!--<a href="#" class="standart-gray">Изучить лендинг по&nbsp;подключению к&nbsp;платформе</a>-->
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper">
                        <img src="/img/affiliate-program/content-images/image1.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend8-bottom.svg" alt="">
    </div>
    <div class="full-width white-bg connection-steps-wrapper">
        <div class="wrapper-block">
            <h3 class="align-center">Three steps to connect a sales outlet:</h3>
            <div class="connection-steps">
                <div class="step-item">
                    <img class="green-shadow" src="/img/affiliate-program/steps-icons/step1-icon.svg" alt="" />
                    <h4 class="bold">Registration of the institution in the partner account</h4>
                    <p>Go to your office at start.briskly.online and start your store, restaurant, cafe, food court as a new partner.</p>
                </div>
                <div class="step-item">
                    <img class="blue-shadow" src="/img/affiliate-program/steps-icons/step2-icon.svg" alt="" />
                    <h4 class="bold">Filling out the questionnaire of the institution in the office</h4>
                    <p>Fill in the partner's data - details, type of outlet, categories, products.</p>
                </div>
                <div class="step-item">
                    <img class="purple-shadow" src="/img/affiliate-program/steps-icons/step3-icon.svg" alt="" />
                    <h4 class="bold">Launching an establishment in the B-Pay application</h4>
                    <p>Fill in menu information, add information brochures to tables, brief staff on how B-Pay works</p>
                </div>
            </div>
            <a href="#my-form" class="btn btn-green btn-big center-position btn300">Become a partner</a>
        </div>
    </div>
    <div class="full-width light-gray-bg for-partners">
        <img class="bend bend-top" src="/img/bend-images/bend9.svg" alt="">
        <div class="wrapper-block">
            <h3 class="align-center">We provide partners</h3>
            <ul class="standart two-columns-view">
                <li>Product training</li>
                <li>Cabinet on the Briskly platform for connecting retail outlets and tracking statistics</li>
                <li>Promotional materials for effective sales</li>
                <li>You adjust the cost of the connection fee so that you can make discounts</li>
            </ul>
        </div>
    </div>


    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top-gray.svg" alt="" />
        <?php require_once $lang_path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom-white.svg" alt="" />
    </div>


    <div class="full-width str-after-wrapper">
        <div class="str-after">
            <div>
                <span class="s24 middle align-center">With Briskly, the future gets closer and moves to the smartphone!</span>
                <img src="/img/str-green.svg" alt="" />
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $lang_path . '/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $lang_path . '/views/elems/work-steps-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $lang_path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $lang_path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/views/elems/map2.tpl'; ?>
    <?php require_once $lang_path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
