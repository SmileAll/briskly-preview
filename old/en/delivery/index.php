<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Connect delivery for your store or cafe';
$description = 'Connect ordering food for a restaurant and delivering groceries for the store via the B-Pay app in 15-40 minutes.';
$keywords = 'Briskley, scan and buy, connect delivery, store app, restaurant app, restaurant loyalty, micromarket, store without cashier, store without staff, micromarket at the entrance';
require_once $lang_path . '/views/common/head.tpl';
?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $lang_path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-delivery-page">
        <div class="wrapper-block flex-wrapper">
            <div class="flex-block w50">
                <div class="logo-top">
                    <a href="/"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала" /></a>
                </div>
                <div class="text-block">
                    <h3>We will connect urgent delivery for your store or cafe in 2 days</h3>
                    <ul class="standart marker-abroad">
                        <li>Online showcase of your products by reference and in the <span class="nowrap">B-Pay app</span></li>
                        <li>Delivery by courier or taxi</li>
                    </ul>
                    <div class="btns-block">
                        <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Подключить</a>
                    </div>
                </div>
            </div>
            <div class="flex-block auto-width center-position">
                <img class="main-image" src="/img/preorder-delivery/content-images/car-phone-bag.png" alt="" />
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend1.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg first-bend-block on-preorder-and-delivery-page">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items tablet-wrap">
                <div class="flex-block" id="feedback">
                    <h3>Your own delivery <br />in two days</h3>
                    <h4>Let's connect to the app for ordering goods with an audience of 50,000 buyers. We will provide a link to your own online store. Major shipping partners are already built in!</h4>
                    <div class="form-block white-bg br-20">
                        <p class="s18 semibold align-center">How much does it cost? <br />Get your presentation now</p>
                        <div class="form-wrapper">
                            <?php require_once $path . '/views/common/forms/43.tpl'; ?>
                        </div>
                    </div>
                </div>
                <div class="flex-block right-position">
                    <ul class="standart marker-abroad">
                        <li>Add your products to the <storng>B-Pay app</storng></li>
                        <li>We will provide an audience of <strong>50,000</strong> people</li>
                        <li>We will accept orders and provide a link to the online showcase with the option of payment</li>
                        <li>We will deliver goods in <strong>15-40</strong>minutes with the help of reliable partners</li>
                    </ul>
                    <h4><stroong>B-Pay</stroong> is your own <span class="nowrap">online store</span> in the app.</h4>
                    <h4>Let's connect for free. Payment&nbsp;—&nbsp;%&nbsp;from&nbsp;transactions performed.</h4>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="" />
    </div>
    <div class="full-width">
        <div class="wrapper-block preorder-process">
            <h3 class="align-center">What does the process look like for a buyer?</h3>
            <div class="flex-wrapper center-items">
                <div class="flex-block">
                    <ol class="special-decor">
                        <li><span class="big-text">The client chooses a store or cafe</span></li>
                        <li><span class="big-text">Adds items to cart</span></li>
                        <li><span class="big-text">Pays by card or Apple Pay</span></li>
                        <li><span class="big-text">Point of sale collects delivery</span></li>
                        <li><span class="big-text">In 20 minutes, a courier arrives, picks up the order and takes it to the buyer</span></li>
                    </ol>
                </div>
                <div class="flex-block image-wrapper">
                    <div class="image-wrapper-left">
                        <img src="/img/preorder-delivery/content-images/phone.jpg" alt="" />
                        <a class="standart-green" href="http://brskl.io/av37" target="_blank">View an example of an online store</a>
                    </div>
                    <div class="image-wrapper-right">
                        <a class="b-pay" href="https://b-pay.online/" target="_blank"><img src="/img/bpay-logo.svg" alt="Приложение B-Pay" /></a>
                        <p>Application <br />for quick shopping</p>
                        <div class="application-stores">
                            <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white-dark-border.svg" /></a>
                            <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white-dark-border.svg" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block delivery-organization-wrapper">
            <h3 class="align-center">Organize your turnkey delivery</h3>
            <ul class="delivery-organization">
                <li>
                    <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow">
                    <h4>Start your delivery <br />in&nbsp;just&nbsp;2&nbsp;days</h4>
                </li>
                <li>
                    <img src="/img/icons/setting-icon.svg" alt="" class="blue-shadow">
                    <h4>Free installation, integration and customization</h4>
                </li>
                <li>
                    <img src="/img/icons/statistics-icon.svg" alt="" class="blue-shadow">
                    <h4>Increase your business turnover through an additional sales channel</h4>
                </li>
            </ul>
        </div>
    </div>
    <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block about-delivery">
            <div class="flex-wrapper about-delivery-top-block tablet-wrap center-items">
                <div class="flex-block">
                    <h3>How does it work for a store or&nbsp;cafe?</h3>
                    <h4 class="semibold">Delivery of your goods in 15-40 minutes based on the Briskly platform.</h4>
                    <h4>24/7 delivery of prepaid purchases. Payment to Briskly account is possible once at the end of the month</h4>
                </div>
                <div class="flex-block">
                    <div class="image-wrapper">
                        <img class="ml40" src="/img/preorder-delivery/content-images/image2.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="flex-wrapper about-delivery-block">
                <div class="flex-block left-position">
                    <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow" />
                    <span class="s18 fatter">Pedestrian couriers</span>
                    <ul class="standart marker-abroad">
                        <li>Fixed shipping cost</li>
                        <li>Orders up to 5 kg and up to 80 cm</li>
                    </ul>
                    <p class="color-light-gray">On average, a pedestrian courier arrives in 15-20 minutes, a driver in 7-10 minutes. The courier is immediately sent to the buyer.</p>
                </div>
                <div class="flex-block right-position">
                    <img src="/img/icons/car-icon.svg" alt="" class="purple-shadow" />
                    <span class="s18 fatter">Drivers</span>
                    <ul class="standart marker-abroad">
                        <li>Shipping cost is calculated automatically</li>
                        <li>Orders up to 20 kg and up to 180 cm</li>
                    </ul>
                    <p class="color-light-gray">You can include payment for delivery entirely at the expense of the client, indicate a fixed cost or pay for delivery yourself.</p>
                </div>
            </div>
            <div class="our-delivery-service">
                <ul>
                    <li>
                        <p style="color: #6d818c;">Integration:</p>
                        <img class="mts-logo" src="/img/mts-logo.svg" alt="" />
                    </li>
                    <li>
                        <p style="color: #6d818c;">Delivery is carried out by:</p>
                        <img src="/img/delivery-logos/yandex-taxi-logo.svg" alt="" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="wrapper-block delivery-options-wrapper">
            <h3 class="align-center">Three options for working with Briskly delivery</h3>
            <div class="delivery-options-block">
                <div class="option-item">
                    <img src="/img/icons/delivery-option-1.svg" alt="" />
                    <span>The client pays 100% of the delivery himself </span>
                </div>
                <div class="option-item modile-row-reverse">
                    <span>The client pays for delivery at a fixed cost, the rest is paid by the company</span>
                    <img src="/img/icons/delivery-option-2.svg" alt="" />
                </div>
                <div class="option-item">
                    <img src="/img/icons/delivery-option-3.svg" alt="" />
                    <span>The company pays for delivery for the client 100%</span>
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend3-bottom.svg" />
    </div>
    <div class="full-width checker-screens-wrapper">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h3>To assemble an order, you will have the <span class="nowrap">"CHEK-ER" app</span>.</h3>
                    <ul class="standart marker-abroad">
                        <li>Your employee collects the order and marks it in the app</li>
                        <li>When the order is collected, the courier is notified and arrives to pick up the order</li>
                        <li>The employee gives the order to the courier</li>
                        <li>The courier takes the order to the client</li>
                    </ul>
                </div>
                <div class="flex-block w40">
                    <div class="swiper-container js-phone-screens">
                        <div class="phone-screens-wrapper swiper-wrapper">
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen8.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen9.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen10.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"><img src="/img/control.svg" /></div>
                        <div class="swiper-button-next"><img src="/img/control.svg" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block preorder-block">
            <div class="flex-wrapper">
                <div class="flex-block w60">
                    <h3>Pre-order</h3>
                    <h4>Customers pre-order on the phone and pick up the dish at the dispensing area</h4>
                    <ul class="standart marker-abroad">
                        <li>The restaurant connects in 1 day</li>
                        <li>No additional equipment required to start</li>
                        <li>Saves customer time</li>
                    </ul>
                    <h4 class="bottom-position">The buyer can easily order food in the <span class="nowrap">B-Pay app</span></h4>
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper">
                        <img src="/img/preorder-delivery/content-images/image3.png" alt="Доставка еды из магазинов и кафе через приложение B-Pay от Брискли" />
                    </div>
                </div>
            </div>
            <div class="horizontal-special-decor-list">
                <ol class="special-decor gray-counter">
                    <li>
                        <h4 class="bold">Choosing a shop</h4>
                        <div>
                            <p>Select a restaurant from the list, on the map or scan the <span class="nowrap">QR code</span></p>
                            <img src="/img/preorder-delivery/content-images/phone1-new.jpg" alt="" />
                        </div>
                    </li>
                    <li>
                        <h4 class="bold">Product selection</h4>
                        <div>
                            <p>Add dishes that will be given to you at the counter or brought</p>
                            <img src="/img/preorder-delivery/content-images/phone2-new.jpg" alt=""" />
                        </div>
                    </li>
                    <li>
                        <h4 class="bold">Payment</h4>
                        <div>
                            <p>Pay by card or Apple Pay. The check goes to the post office and to the OFD</p>
                            <img src="/img/preorder-delivery/content-images/phone3-new.jpg" alt=""" />
                        </div>
                    </li>
                </ol>
            </div>
            <div class="str-after on-scan-go">
                <div>
                    <span class="s24 semibold align-center">Quick to buy, easy to pay</span>
                    <img src="/img/str-green.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top-gray.svg" alt="" />
        <?php require_once $lang_path . '/views/elems/main-about-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom-gray.svg" alt="" />
    </div>
    <div class="full-width p85">
        <div class="wrapper-block">
            <h3 class="align-center">FAQ</h3>
            <div class="questions-wrapper">
                <div class="question-block js-opened-text">
                    <span>Where exactly does the employee place the order?</span>
                    <div class="text">
                        <p>The order is formed in the CHEK-EP app, the employee can download it to his own or work phone.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>What happens if one item has run out of an order? How to manage changes in the order content?</span>
                    <div class="text">
                        <p>You can replace the product when assembling the order and agree on the replacement with the customer before the final payment of the order.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>How does an employee receive an order notification?</span>
                    <div class="text">
                        <p>A notification about a new order comes in a push-notification of the CHEK-ER app. Or an employee can monitor the appearance of a new order in the app itself.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>How do I return payment to a customer?</span>
                    <div class="text">
                        <p>If you have already confirmed the order and money was debited from the client, then the buyer can write to the technical support of the application. The support specialists will agree on the solution with you and return the money to the client for the order.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>At what point is money debited from a client?</span>
                    <div class="text">
                        <p>The money is debited after the final assembly of the order and approval of all possible changes in the composition with the client.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>How do we know that the money has been debited from the client?</span>
                    <div class="text">
                        <p>In the CHEK-ER app, the employee receives a notification that the money has been debited from the client. If the notification is not received, then the employee can contact the client and ask to bind a new card or replenish the account.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $lang_path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $lang_path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $lang_path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $lang_path . '/views/elems/map2.tpl'; ?>
    <?php require_once $lang_path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
