<?php
$path = \dirname(__FILE__, 2);
$version = '100620_1';
$title = 'Подключи доставку для своего магазина или кафе';
$description = 'Подключи заказ еды для ресторана и доставку продуктов для магазина через приложение B-Pay за 15-40 минут.';
$keywords = 'Брискли, сканируй и покупай, подключить доставку, приложение для магазина, приложение для ресторана, лояльность для ресторана, микромаркет, магазин без кассира, магазин без персонала, микромаркет в подъезде';
?>
<?php require_once $path . '/views/common/head.tpl'; ?>

<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color on-scan-go">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper scan-go-top-block">
        <div class="wrapper-block flex-wrapper">
            <div class="flex-block w660">
                <div class="logo-top">
                    <a href="/"><img src="/img/logo-dark.svg" alt="Briskly - платформа для торговли без персонала" /></a>
                </div>
                <div class="text-block">
                    <h3>Предзаказ и&nbsp;доставка товаров из&nbsp;магазина и&nbsp;кафе через приложение&nbsp;<span class="nowrap">B-Pay</span></h3>
                    <ul class="standart marker-abroad">
                        <li>Покупки без кассира в&nbsp;супермаркетах</li>
                        <li>Доставка до&nbsp;двери из&nbsp;ближайших торговых точек</li>
                        <li>Предзаказ для заведений</li>
                    </ul>
                    <div class="btns-block">
                        <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Связаться с нами</a>
                    </div>
                </div>
            </div>
            <div class="flex-block auto-width center-position">
                <div class="phone-image-block">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 767px)" srcset="/img/phone-screens/screen11_small.png">
                        <img src="/img/phone-screens/screen11.png" alt="" />
                    </picture>
                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                    <img class="icon-image" src="/img/icons/restaurant-icon-red.svg" alt="" />
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend1.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg first-bend-block on-preorder-and-delivery-page">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items tablet-wrap">
                <div class="flex-block w60" id="feedback">
                    <h3>B-Pay&nbsp;&mdash; приложение для&nbsp;быстрых покупок без&nbsp;лишних контактов</h3>
                    <h4>Это касса в&nbsp;смартфоне, которая работает прямо в&nbsp;магазине или кафе. А&nbsp;когда нет возможности выйти&nbsp;из&nbsp;дома&nbsp;&mdash; это <span class="nowrap">интернет-магазин</span>.</h4>
                    <div class="form-block white-bg br-20">
                        <p class="s18 semibold align-center">Получите презентацию сейчас, решите потом</p>
                        <div class="form-wrapper">
                            <script id="bx24_form_inline" data-skip-moving="true">
                                (function (w, d, u, b) {
                                    w['Bitrix24FormObject'] = b;
                                    w[b] = w[b] || function () {
                                        arguments[0].ref = u;
                                        (w[b].forms = w[b].forms || []).push(arguments[0])
                                    };
                                    if (w[b]['forms'])
                                        return;
                                    var s = d.createElement('script');
                                    s.async = 1;
                                    s.src = u + '?' + (1 * new Date());
                                    var h = d.getElementsByTagName('script')[0];
                                    h.parentNode.insertBefore(s, h);
                                })(window, document, 'https://briskly.bitrix24.ru/bitrix/js/crm/form_loader.js', 'b24form');

                                b24form({"id": "25", "lang": "ru", "sec": "gjdf77", "type": "inline"});
                            </script>
                        </div>
                    </div>
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper">
                        <img src="/img/preorder-delivery/content-images/man-image.jpg" alt="" />
                    </div>
                </div>
            </div>
            <div class="flex-wrapper center-items">
                <div class="flex-block pluses-for-you-wrapper">
                    <ul class="pluses-for-you big-icons">
                        <li>
                            <img src="/img/icons/restaurant-icon.svg" alt="" class="green-shadow" />
                            <p class="s18"><strong>Касса в&nbsp;смартфоне покупателя для оплаты без кассира.</strong> Пользователь <span class="nowrap">B-Pay</span> сканирует штрихкод и&nbsp;оплачивает товары в&nbsp;приложении, картой или&nbsp;Apple&nbsp;Pay.</p>
                        </li>
                        <li>
                            <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow" />
                            <p class="s18"><strong>Доставка до&nbsp;двери из&nbsp;ближайших торговых точек.</strong> Покупатель добавляет товары в&nbsp;корзину, магазин собирает заказ, через 40&nbsp;минут курьер стучит в&nbsp;дверь.</p>
                        </li>
                        <li>
                            <img src="/img/icons/heart-icon-red.svg" alt="" class="pink-shadow" />
                            <p class="s18"><strong>Предзаказ для заведений через приложение.</strong> Пользователи B-Pay делают предзаказ любимых блюд в&nbsp;приложении. Далее&nbsp;&mdash; забирают готовую еду в&nbsp;кафе или заказывают доставку.</p>
                        </li>
                    </ul>
                </div>
                <div class="flex-block right-position">
                    <div class="image-wrapper have-small-icon-blocks">
                        <img class="main-image" src="/img/preorder-delivery/content-images/circle-image.svg" alt="" />
                        <div class="small-icon-block azs">
                            <img src="/img/small-icons/icon1.svg" alt="" />
                            <span class="t-up">10 мин</span>
                            <span>заправиться<br />на&nbsp;АЗС</span>
                        </div>
                        <div class="small-icon-block sup-m">
                            <img src="/img/small-icons/icon2.svg" alt="" />
                            <span class="t-up">5 мин</span>
                            <span>купить в&nbsp;супермаркете</span>
                        </div>
                        <div class="small-icon-block bac-green micro-m">
                            <img src="/img/small-icons/icon3.svg" alt="" />
                            <span class="t-up">90 сек</span>
                            <span>купить в&nbsp;микромаркете</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="" />
    </div>
    <div class="full-width">
        <div class="wrapper-block preorder-process">
            <h3 class="align-center">Как выглядит процесс?</h3>
            <div class="flex-wrapper center-items">
                <div class="flex-block">
                    <ol class="special-decor">
                        <li><span class="big-text">Клиент выбирает магазин или&nbsp;заведение</span></li>
                        <li><span class="big-text">Добавляет товары в&nbsp;корзину</span></li>
                        <li><span class="big-text">Оплачивает картой или&nbsp;Apple&nbsp;Pay</span></li>
                        <li><span class="big-text">Торговая точка собирает доставку</span></li>
                        <li><span class="big-text">Через 20&nbsp;минут приходит курьер, забирает заказ и&nbsp;отвозит покупателю</span></li>
                    </ol>
                </div>
                <div class="flex-block">
                    <div class="image-wrapper">
                        <img class="main-image" src="/img/preorder-delivery/content-images/process-image.jpg" alt="" />
                        <div class="phone-image-block">
                            <picture>
                                <source media="(min-width: 320px) and (max-width: 767px)" srcset="/img/phone-screens/screen11_small.png">
                                <img src="/img/phone-screens/screen11.png" alt="" />
                            </picture>
                            <img class="phone-frame" src="/img/phone-frame-dark.svg" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block about-delivery">
            <img class="absolute-position" src="/img/bag-image.png" alt="" />
            <div class="flex-wrapper about-delivery-top-block tablet-wrap center-items">
                <div class="flex-block">
                    <h3>Доставка ваших товаров за&nbsp;<span class="nowrap">15-40</span>&nbsp;минут через приложение <span class="nowrap">B-Pay</span></h3>
                    <h4>Круглосуточная доставка предоплаченных покупок. Возможна оплата на&nbsp;счет Briskly один раз в&nbsp;конце месяца</h4>
                    <img class="box-image" src="/img/preorder-delivery/content-images/box-image.jpg" alt="" />
                </div>
                <div class="flex-block">
                    <div class="image-wrapper">
                        <img class="ml40" src="/img/preorder-delivery/content-images/image1.jpg" alt="" />
                    </div>
                </div>
            </div>
            <div class="flex-wrapper about-delivery-block">
                <div class="flex-block left-position">
                    <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow" />
                    <span class="s18 fatter">Пешие курьеры</span>
                    <ul class="standart marker-abroad">
                        <li>Только в Москве в пределах ТТК</li>
                        <li>Фиксированная стоимость доставки</li>
                        <li>Заказы до 5 кг и до 80 см</li>
                    </ul>
                    <p class="color-light-gray">Пеший курьер в&nbsp;среднем прибывает за&nbsp;15-20&nbsp;минут, водитель&nbsp;&mdash; за&nbsp;7-10&nbsp;минут. Курьер сразу отправляется к&nbsp;покупателю.</p>
                </div>
                <div class="flex-block right-position">
                    <img src="/img/icons/car-icon.svg" alt="" class="purple-shadow" />
                    <span class="s18 fatter">Водители</span>
                    <ul class="standart marker-abroad">
                        <li>В более 700 городах, где есть Gett, Яндекс.Такси такси "Максим" и&nbsp;Dostavista</li>
                        <li>В Москве — до 30 км от МКАД</li>
                        <li>Стоимость доставки рассчитывается автоматически</li>
                        <li>Заказы до 20 кг и до 180 см</li>
                    </ul>
                    <p class="color-light-gray">Вы&nbsp;можете включить оплату доставки целиком за&nbsp;счёт клиента, обозначить фиксированную стоимость или самостоятельно платить за&nbsp;доставку.</p>
                </div>
            </div>
            <div class="flex-wrapper our-delivery-service">
                <p>Доставляют</p>
                <img src="/img/delivery-logos/yandex-taxi-logo.svg" alt="" />
                <img src="/img/delivery-logos/gett-logo.svg" alt="" />
                <img src="/img/delivery-logos/maxim-logo.svg" alt="" />
                <img src="/img/delivery-logos/dostavista-logo.svg" alt="" />
            </div>
        </div>
        <div class="wrapper-block delivery-options-wrapper">
            <h3 class="align-center">Три варианта работы с&nbsp;доставкой Briskly</h3>
            <div class="delivery-options-block">
                <div class="option-item">
                    <img src="/img/icons/delivery-option-1.svg" alt="" />
                    <span>Клиент оплачивает 100% доставки сам </span>
                </div>
                <div class="option-item modile-row-reverse">
                    <span>Клиент оплачивает доставку по&nbsp;фиксированной стоимости, остальное доплачивает компания</span>
                    <img src="/img/icons/delivery-option-2.svg" alt="" />
                </div>
                <div class="option-item">
                    <img src="/img/icons/delivery-option-3.svg" alt="" />
                    <span>Компания оплачивает доставку за&nbsp;клиента на&nbsp;100%</span>
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend3-bottom.svg" />
    </div>
    <div class="full-width checker-screens-wrapper">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h4>Для сборки заказа у&nbsp;вас будет приложение <span class="nowrap">&laquo;ЧЕК-ЕР&raquo;</span>.</h4>
                    <ul class="standart marker-abroad">
                        <li>Ваш сотрудник собирает заказ и&nbsp;отмечает это в&nbsp;приложении</li>
                        <li>Когда заказ собран, курьер получает уведомление и&nbsp;приезжает забирать заказ</li>
                        <li>Сотрудник отдаёт заказ курьеру</li>
                        <li>Курьер отвозит заказ клиенту</li>
                    </ul>
                </div>
                <div class="flex-block w40">
                    <div class="swiper-container js-phone-screens">
                        <div class="phone-screens-wrapper swiper-wrapper">
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen8.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen9.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                            <div class="phone-screen-item swiper-slide">
                                <div class="phone-image-block">
                                    <img src="/img/phone-screens/screen10.png" alt="" />
                                    <img class="phone-frame" src="/img/phone-frame-gray.svg" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-prev"><img src="/img/control.svg" /></div>
                        <div class="swiper-button-next"><img src="/img/control.svg" /></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block preorder-block">
            <div class="flex-wrapper">
                <div class="flex-block w60">
                    <h3>Предзаказ</h3>
                    <h4>Клиенты делают предзаказ в&nbsp;телефоне и&nbsp;забирают блюдо в&nbsp;зоне выдачи</h4>
                    <ul class="standart marker-abroad">
                        <li>Ресторан подключается за&nbsp;1&nbsp;день</li>
                        <li>Не&nbsp;требуется дополнительное оборудование для старта</li>
                        <li>Экономит время покупателя</li>
                    </ul>
                    <h4 class="bottom-position">Покупатель может легко заказать еду в&nbsp;приложении&nbsp;<span class="nowrap">B-Pay</span></h4>
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper">
                        <img src="/img/preorder-delivery/content-images/preorder-image.jpg" alt="Доставка еды из магазинов и кафе через приложение B-Pay от Брискли" />
                    </div>
                </div>
            </div>
            <div class="horizontal-special-decor-list">
                <ol class="special-decor gray-counter">
                    <li>
                        <h4 class="bold">Выбор заведения</h4>
                        <p>Выберите ресторан в&nbsp;списке, на&nbsp;карте или отсканируйте <span class="nowrap">QR-код</span></p>
                    </li>
                    <li>
                        <h4 class="bold">Выбор товаров</h4>
                        <p>Добавьте блюда, которые вам выдадут на&nbsp;стойке или принесут</p>
                    </li>
                    <li>
                        <h4 class="bold">Оплата</h4>
                        <p>Оплатите картой или Apple&nbsp;Pay. Чек уходит на&nbsp;почту и&nbsp;в&nbsp;ОФД</p>
                    </li>
                </ol>
            </div>
            <div class="str-after on-scan-go">
                <div>
                    <span class="s24 semibold align-center">Быстро покупать, легко оплачивать</span>
                    <img src="/img/str-green.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top-gray.svg" alt="" />
        <?php require_once $path . '/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $path . '/views/elems/work-steps-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom-gray.svg" alt="" />
    </div>
    <div class="full-width p85">
        <div class="wrapper-block">
            <h3 class="align-center">Частые вопросы</h3>
            <div class="questions-wrapper">
                <div class="question-block js-opened-text">
                    <span>Где именно сотрудник формирует заказ?</span>
                    <div class="text">
                        <p>Заказ формируется в&nbsp;приложении ЧЕК-ЕР, сотрудник может скачать его на&nbsp;собственный или&nbsp;рабочий телефон.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Что происходит, если в&nbsp;составе заказа один товар закончился? Как управлять изменением состава заказа?</span>
                    <div class="text">
                        <p>Вы&nbsp;можете заменить товар при сборке заказа и&nbsp;согласовать замену с&nbsp;клиентом до&nbsp;окончательной оплаты заказа.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Как сотрудник получает оповещение о&nbsp;заказе?</span>
                    <div class="text">
                        <p>Оповещение о&nbsp;новом заказе приходит в&nbsp;push-уведомлении приложения ЧЕК-ЕР. Или сотрудник может следить за&nbsp;появлением нового заказа в&nbsp;самом приложении.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Как возвращать оплату клиенту?</span>
                    <div class="text">
                        <p>Если вы&nbsp;уже подтвердили заказ и&nbsp;с&nbsp;клиента списались деньги, то&nbsp;покупатель может написать в&nbsp;техническую поддержку приложения. Специалисты службы поддержки согласуют решение с&nbsp;вами и&nbsp;вернут деньги клиенту за&nbsp;заказ.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>В&nbsp;какой момент с&nbsp;клиента списываются деньги?</span>
                    <div class="text">
                        <p>Деньги списываются после окончательной сборки заказа и&nbsp;согласования всех возможных изменений в&nbsp;составе с&nbsp;клиентом.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Как мы&nbsp;поймем, что деньги списались с&nbsp;клиента?</span>
                    <div class="text">
                        <p>В&nbsp;приложении ЧЕР-ЕР сотрудник получает уведомление о&nbsp;том, что деньги списались у&nbsp;клиента. Если уведомление не&nbsp;пришло, то&nbsp;сотрудник может связаться с&nbsp;клиентом и&nbsp;попросить привязать новую карту или пополнить счёт.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/views/elems/map2.tpl'; ?>
    <?php require_once $path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>