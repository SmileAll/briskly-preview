<script id="bx24_form_inline" data-skip-moving="true">
    (function() {
        var pastCallback = typeof window.roistatVisitCallback === "function" ? window.roistatVisitCallback : null;
        window.roistatVisitCallback = function (visitId) {
            if (pastCallback !== null) {
                pastCallback(visitId);
            }

            (function (w, d, u, b) {
        w['Bitrix24FormObject'] = b;
        w[b] = w[b] || function () {
            arguments[0].ref = u;
            (w[b].forms = w[b].forms || []).push(arguments[0])
        };
        if (w[b]['forms'])
            return;
        var s = d.createElement('script');
        s.async = 1;
        s.src = u + '?' + (1 * new Date());
        var h = d.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s, h);
    })(window, document, 'https://briskly.bitrix24.ru/bitrix/js/crm/form_loader.js', 'b24form');

    b24form({"id": "21", "lang": "ru", "sec": "vxxniz", "type": "inline", "presets": {"roistatID": visitId}});

    };
    })();
</script>