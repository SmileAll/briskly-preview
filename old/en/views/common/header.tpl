<div class="header">
    <div class="logo">
        <a href="/en" class="logo-dark"><img src="/img/logo-dark-new.svg" alt="Briskly - a platform for unmanned trading, cashier in a smartphone" /></a>
        <a href="/en" class="logo-white"><img src="/img/logo-white-new.svg" alt="" /></a>
    </div>
    <menu>
        <ul>
            <li>
                <a href="#" class="open-extra-menu only-desktop js-open-extra-menu" data-block="extra1">B-Pay App</a>
                <a href="https://b-pay.online/en/" class="only-mobile-view" target="_blank">B-Pay App</a>
            </li>
            <li>
                <a href="#" class="open-extra-menu js-open-extra-menu" data-block="extra2">Products</a>
                <ul class="only-mobile">
                    <li><a href="/en/briskly-for-shops/" target="_blank">Checkout in smartphone</a></li>
                    <!-- <li><a href="/en/delivery/" target="_blank">Pre-order and delivery</a></li> -->
                    <li><a href="/en/briskly-micro-market/" target="_blank">Micromarkets</a></li>
                    <li><a href="https://study.briskly.online/" target="_blank">Learning portal</a></li>
                </ul>
            </li>
            <li>
                <a href="/en/briskly_for_partners/" target="_blank">Referrer Program</a>
            </li>
            <li><a href="#" class="open-extra-menu js-open-extra-menu" data-block="extra3">About company</a>
                <ul class="only-mobile">
                    <li><a href="https://briskly.online/story/" target="_blank">History of Briskly</a></li>
                    <li><a href="https://briskly.online/media/" target="_blank">Briskly in the media</a></li>
                    <li><a href="https://briskly.online/vacancy/" target="_blank">Vacancies</a></li>
                    <li><a href="https://linnafrost.ru/" target="_blank">Linnafrost</a></li>
                    <li><a href="https://briskly.online/investors/" target="_blank">For investors</a></li>
                </ul>
            </li>
            <li><a href="https://bbo.briskly.online/" target="_blank">Account</a></li>
        </ul>
    </menu>
    <div class="header-right">
        <a href="tel:<?= $header_phone_url?>" class="tel"><?= $header_phone?></a>
        <ul class="languages js-languages">
            <? foreach ($langs_list as $lang) {?>
                <li<? if ($lang['active']) {?> class="active js-active-click"<? }?>><a  href="<?= $lang['code'] ? ('/' . $lang['code'].$url) : $url?>?location_code=<?= $lang['name']?>"><img src="/img/flags/<?= $lang['icon']?>.svg" alt="" /><span><?= $lang['name']?></span></a></li>
            <? }?>
        </ul>
        <a href="#" class="open-menu js-open-menu">
            <span></span>
            <span></span>
            <span></span>
        </a>
    </div>
    <!--<a href="/briskly-tariffs/" class="btn btn-green btn-tariffs" target="_blank">Тарифы</a>-->
</div>



<div class="extra-menu-wrapper only-mobile">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Download the app</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Write to us</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                    <a href="tel:+78006005096" class="tel">8-800-600-50-96</a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="extra-menu-wrapper" data-block="extra1">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Download the app</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block" style="flex-grow: 1; max-width: 100%;">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="https://b-pay.online/" target="_blank">
                            <span class="h-style">B-Pay mobile app</span>
                            <span class="standart-text">Pay without queues!</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl" target="_blank"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Write to us</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="extra-menu-wrapper" data-block="extra2">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Download the app</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="/en/briskly-for-shops/" target="_blank">
                            <span class="h-style">Checkout in smartphone</span>
                            <span class="standart-text">For stores</span>
                        </a>
                    </div>
                    <!-- <div class="ul-block-line">
                        <a href="/en/delivery/" target="_blank">
                            <span class="h-style">Pre-order and delivery</span>
                            <span class="standart-text">Fast shopping without unnecessary contacts</span>
                        </a>
                    </div> -->
                    <div class="ul-block-line">
                        <!--<a href="/briskly-tariffs/" class="btn btn-big btn-green btn-tariffs" target="_blank">Тарифы</a>-->
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="/en/briskly-micro-market/" target="_blank">
                            <span class="h-style">Micromarkets</span>
                            <span class="standart-text">Micromarket, vending</span>
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://study.briskly.online/" target="_blank">
                            <span class="h-style">Learning portal</span>
                            <span class="standart-text">study.briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl" target="_blank"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Write to us</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="extra-menu-wrapper" data-block="extra3">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Download the app</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="https://briskly.online/story/" target="_blank">
                            <span class="h-style">About company</span>
                            <span class="standart-text">History of Briskly</span>
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://briskly.online/media/" target="_blank">
                            <span class="h-style">Briskly in the media</span>
                            <span class="standart-text">Publications</span>
                        </a>
                    </div>

                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="https://linnafrost.ru/" target="_blank">
                            <span class="h-style" style="margin-left: 200px">Linnafrost</span>
                            <!--<span class="standart-text" style="margin-left: 200px">ВЗХТ</span>-->
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://briskly.online/vacancy/" target="_blank">
                            <span class="h-style" style="margin-top: -55px">Vacancies</span>
                            <span class="standart-text">Work in Briskly</span>
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://briskly.online/investors/" target="_blank">
                            <span class="h-style">For investors</span>
                            <span class="standart-text">Information for investors</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl" target="_blank"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Write to us</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
