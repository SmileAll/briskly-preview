<div class="about-terminal wrapper-block">
    <div class="flex-wrapper">
        <div class="flex-block">
            <h2>Payment without application</h2>
            <h4>An alternative way is to pay by card through the terminal</h4>
            <p>Installing a tablet with a terminal on a kiosk</p>
            <p>The user registers in the system once and can pay by applying the card to the built-in terminal</p>
            <p>This method reduces the time of interaction with the refrigerator to 30-60 seconds</p>
        </div>
        <div class="flex-block right-position">
            <div class="image-wrapper">
                <img class="main-image" src="/img/main-page-images/food-terminal-new.jpg" alt="" />
                <img class="card-image" src="/img/main-page-images/card.svg" alt="" />
            </div>
        </div>
    </div>
</div>