<div class="mass-media-about-us">
    <div class="h-block wrapper-block">
        <h2>Briskly in the media</h2>
    </div>
    <div class="swiper-container articles-wrapper js-articles">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://secretmag.ru/selfie/briskly.htm" target="_blank" class="img-wrapper"><img src="/files/mass-media/secretmag.svg" alt="" /></a>
                    <a href="https://secretmag.ru/selfie/briskly.htm" target="_blank" class="media-name">secretmag.ru</a>
                    <p>How to teach Russians to buy food without cashiers and security guards</p>
                    <a href="https://secretmag.ru/selfie/briskly.htm" target="_blank" class="btn btn-black btn-small">To read</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://rb.ru/opinion/magazin-buduschego/" target="_blank" class="img-wrapper"><img src="/files/mass-media/rb.svg" alt="" /></a>
                    <a href="https://rb.ru/opinion/magazin-buduschego/" target="_blank" class="media-name">rb.ru</a>
                    <p>How to grow from a prototype to a startup with an estimate of $ 10 million in a year</p>
                    <a href="https://rb.ru/opinion/magazin-buduschego/" target="_blank" class="btn btn-black btn-small">To read</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://vc.ru/trade/73620-kak-ya-borolsya-s-ocheredyami-v-svoem-produktovom-magazine-v-skolkovo-s-pomoshchyu-tehnologiy" target="_blank" class="img-wrapper"><img src="/files/mass-media/vc.svg" alt="" /></a>
                    <a href="https://vc.ru/trade/73620-kak-ya-borolsya-s-ocheredyami-v-svoem-produktovom-magazine-v-skolkovo-s-pomoshchyu-tehnologiy" target="_blank" class="media-name">vc.ru</a>
                    <p>How I fought queues at my grocery store at Skolkovo with B-Pay</p>
                    <a href="https://vc.ru/trade/73620-kak-ya-borolsya-s-ocheredyami-v-svoem-produktovom-magazine-v-skolkovo-s-pomoshchyu-tehnologiy" target="_blank" class="btn btn-black btn-small">To read</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://www.retail.ru/articles/kak-rossiyskiy-startap-briskly-zapuskaet-magaziny-bez-personala/" target="_blank" class="img-wrapper">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/mass-media/retail_small.png">
                            <img src="/files/mass-media/retail.png" alt="" />
                        </picture>
                    </a>
                    <a href="https://www.retail.ru/articles/kak-rossiyskiy-startap-briskly-zapuskaet-magaziny-bez-personala/" target="_blank" class="media-name">retail.ru</a>
                    <p>How Russian startup Briskly launches stores without staff</p>
                    <a href="https://www.retail.ru/articles/kak-rossiyskiy-startap-briskly-zapuskaet-magaziny-bez-personala/" target="_blank" class="btn btn-black btn-small">To read</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" target="_blank" class="img-wrapper"><img src="/files/mass-media/retailer.svg" alt="" /></a>
                    <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" target="_blank" class="media-name">retailer.ru</a>
                    <p>Micromarkets - a new format for selling prepared food in offices</p>
                    <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" target="_blank" class="btn btn-black btn-small">To read</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://www.vedomosti.ru/business/articles/2020/02/13/822968-rossiiskie-produktovie-magazini-vvodyat" target="_blank" class="img-wrapper"><img src="/files/mass-media/vedomosti.svg" alt="" /></a>
                    <a href="https://www.vedomosti.ru/business/articles/2020/02/13/822968-rossiiskie-produktovie-magazini-vvodyat" target="_blank" class="media-name">vedomosti.ru</a>
                    <p>Russian grocery stores introduce payment past the checkout</p>
                    <a href="https://www.vedomosti.ru/business/articles/2020/02/13/822968-rossiiskie-produktovie-magazini-vvodyat" target="_blank" class="btn btn-black btn-small">To read</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://www.the-village.ru/village/city/news-city/374417-av-bez-kass" target="_blank" class="img-wrapper"><img src="/files/mass-media/villadge.svg" alt="" /></a>
                    <a href="https://www.the-village.ru/village/city/news-city/374417-av-bez-kass" target="_blank" class="media-name">the-village.ru</a>
                    <p>"Azbuka vkusa" tests payment for purchases in the application. No more queuing</p>
                    <a href="https://www.the-village.ru/village/city/news-city/374417-av-bez-kass" target="_blank" class="btn btn-black btn-small">To read</a>
                </div>
            </div>
        </div>
        <div class="swiper-button-prev"><img src="/img/control.svg" /></div>
        <div class="swiper-button-next"><img src="/img/control.svg" /></div>
    </div>
    <a href="https://briskly.online/media/?roistat_visit=246168" class="btn btn-black btn-big center-position" style="margin-top: 20px; padding: 0 30px;" target="_blank">See all articles</a>
</div>