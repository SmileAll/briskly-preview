<div class="wrapper-block color-white align-center">
    <div class="have-questions">
        <h2>Still have questions?</h2>
        <h4>Call our manager or receive a presentation to your email</h4>
        <div class="btns-block">
            <a href="tel:+78006005096" class="btn btn-green btn-big btn300 tel">8-800-600-50-96</a>
            <a href="#feedback" class="btn btn-green-border btn-big btn300 to-feedback">Wanna get a presentation?</a>
        </div>
    </div>
</div>