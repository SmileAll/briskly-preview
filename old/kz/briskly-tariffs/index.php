<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
// $version = '300820-1';
$title = 'Briskly - тарифы';
?>
<?php require_once $path . '/views/common/head.tpl'; ?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-tariffs-page">
        <div class="wrapper-block">
            <span class="s18 semibold">Тарифы, действующие с 01.04.2022:</span>
            <h3>«7 000 ₽ в месяц»</h3>
            <p class="s14 bold">Что включено?</p>
            <p>Касса самообслуживания в смартфоне покупателя.</p>
            <p>Стандартная интеграция ассортимента товаров.</p>
            <p>Техническая поддержка покупателей.</p>
            <p>Добавление акций и скидок.</p>
            <p>Личный кабинет Briskly Business.</p>
        </div>
    </div>
    <div class="full-width footer-height">
        <img class="bend bend-bottom" src="/img/bend-images/map-bend-bottom.svg" alt="">
        <?php require_once $path . '/views/common/footer.tpl'; ?>
    </div>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
