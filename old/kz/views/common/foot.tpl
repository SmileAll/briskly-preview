©<script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
    $('.tel').on('click', function () {
        fbq('track', 'Lead', {content_name: 'phoneclick'});
    });
</script>
<script type="text/javascript" src="/js/swiper.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/js/app_new.js?v<?=$version?>"></script>
<script async id="bx24_form_link" data-skip-moving="true">
    (function() {
        var pastCallback = typeof window.roistatVisitCallback === "function" ? window.roistatVisitCallback : null;
        window.roistatVisitCallback = function (visitId) {
            if (pastCallback !== null) {
                pastCallback(visitId);
            }

            !function (r, e, t, i) {
        if (r.Bitrix24FormObject = i, r[i] = r[i] || function () {
            arguments[0].ref = t, (r[i].forms = r[i].forms || []).push(arguments[0])
        }, !r[i].forms) {
            var n = e.createElement("script");
            n.async = 1, n.src = t + "?" + 1 * new Date;
            var s = e.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(n, s)
        }
    }(window, document, "https://briskly.bitrix24.ru/bitrix/js/crm/form_loader.js", "b24form"), b24form({id: "4", lang: "ru", sec: "nzta7l", type: "link", click: "", "presets": {"roistatID": visitId}});

    };
    })();

</script>
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn-ru.bitrix24.ru/b6486705/crm/site_button/loader_2_vt2pul.js');
</script>
<!--ROISTAT BEGIN -->
<script async>
    (function() {
        var pastCallback = typeof window.roistatVisitCallback === "function" ? window.roistatVisitCallback : null;
        window.roistatVisitCallback = function (visitId) {
            if (pastCallback !== null) {
                pastCallback(visitId);
            }

   <!-- !function (e, t, r) {
        var n = t.createElement("script");
        n.async = !0, n.src = r + "?" + (Date.now() / 6e4 | 0);
        var a = t.getElementsByTagName("script")[0];
        a.parentNode.insertBefore(n, a)
    }(window, document, "https://cdn.bitrix24.ru/b6486705/crm/site_button/loader_2_vt2pul.js");
    setTimeout(function () {
        $('.b24-widget-button-wrapper').on('click', function () {
            fbq('track', 'Lead', {content_name: 'chat'});
        });
    }, 5000);
    window.Bitrix24WidgetObject = window.Bitrix24WidgetObject || {};
    window.Bitrix24WidgetObject.handlers = {
        'form-init': function (form) {
            form.presets = {
                'roistatID': visitId
            };
        }
    }; -->

        };
    })();
</script>
<!--ROISTAT END -->

<script async type="text/javascript">
    !function (e, t, a, n, c, r, i) {
        e[c] = e[c] || function () {
            (e[c].a = e[c].a || []).push(arguments)
        }, e[c].l = 1 * new Date, r = t.createElement(a), i = t.getElementsByTagName(a)[0], r.async = 1, r.src = n, i.parentNode.insertBefore(r, i)}(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"), ym(50899424, "init", {clickmap: !0, trackLinks: !0, accurateTrackBounce: !0, webvisor: !0});
</script>
<script async>
    !function (e, t, n, c, o, a, f) {
        e.fbq || (o = e.fbq = function () {
            o.callMethod ? o.callMethod.apply(o, arguments) : o.queue.push(arguments)
        }, e._fbq || (e._fbq = o), o.push = o, o.loaded = !0, o.version = "2.0", o.queue = [], a = t.createElement(n), a.async = !0, a.src = c, f = t.getElementsByTagName(n)[0], f.parentNode.insertBefore(a, f))}(window, document, "script", "https://connect.facebook.net/en_US/fbevents.js"), fbq("init", "245552079678638"), fbq("track", "PageView");
</script>
<script async type="text/javascript">!function () {
        var t = document.createElement("script");
        t.type = "text/javascript", t.async = !0, t.src = "https://vk.com/js/api/openapi.js?168", t.onload = function () {
            VK.Retargeting.Init("VK-RTRG-493502-avijj"), VK.Retargeting.Hit()
        }, document.head.appendChild(t)
    }();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-493502-avijj" style="position:fixed; left:-999px;" alt=""/></noscript>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=245552079678638&ev=PageView&noscript=1"/></noscript>
<noscript><div><img src="https://mc.yandex.ru/watch/50899424" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!--ROISTAT BEGIN -->
<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', 'b3046e7af14084a4dcbceb5ea37a094b');
</script>
<script>
    window.addEventListener('onBitrixLiveChat', function(event)
    {
        var widget = event.detail.widget;
        widget.subscribe({
            type: BX.LiveChatWidget.SubscriptionType.userForm,
            callback: function(data){
                let d = data.fields;
                let name = d.name;
                let email = d.email;
                let phone = d.phone;
                ym(50899424,'reachGoal','chat');
                ga('send', 'event', 'general', 'chat');
                roistatGoal.reach({leadName: "Новый лид с чата", name: name, phone: phone, email: email, is_skip_sending: '1' , fields: {}})
            }});
    });
</script>
<!--ROISTAT END -->
<script type="text/javascript">

  $(document).on('click','.b24-form-btn', function(){
    ym(50899424,'reachGoal','lead');
    gtag('event', 'lead', {'event_category' : 'general'});
    fbq('track', 'Lead');
  });

</script>
<? if (isset($_GET['form_success'])) {?>
    <style type="text/css">
        .pop-up-wrapper {
            position: fixed;
            align-items: center;
            justify-content: center;
            height: 100%;
            width: 100%;
            left: 0;
            top: 0;
            background: rgba(30, 30, 30, 0.6);
            z-index: 101;
            display: none; }
        .pop-up-wrapper.active {
            display: flex;
            display: -webkit-flex; }
        .pop-up-wrapper .enter-form-wrapper {
            background: #fff;
            border-radius: 15px;
            width: 90%;
            max-width: 600px;
            padding: 55px 70px;
            height: auto;
            max-height: 600px; }
        .pop-up-wrapper .enter-form-wrapper .enter-form-block {
            width: 100%; }
        .pop-up-wrapper .enter-form-wrapper .flex-wrapper {
            align-items: flex-end;
            -webkit-align-items: flex-end;
            padding-top: 20px; }
        .pop-up-wrapper .enter-form-wrapper .enter-form {
            font-size: 0;
            margin-right: 20px; }
        .pop-up-wrapper .enter-form-wrapper .image-wrapper img {
            width: 163px; }
        .pop-up-wrapper .enter-form-wrapper h1 {
            font-size: 31px;
            line-height: 36px;
            font-weight: 700;
            margin-bottom: 15px; }
        .pop-up-wrapper .enter-form-wrapper span {
            display: block;
            font-size: 20px;
            font-weight: 700;
            margin-bottom: 40px; }
        .pop-up-wrapper .enter-form-wrapper p {
            font-size: 14px;
            line-height: 18px;
            padding: 0 0 15px 0; }
        </style>
        <div class="pop-up-wrapper active js-popup-form">
        <div class="enter-form-wrapper">
            <div class="enter-form-block">
                <h1>Спасибо!</h1>
                <p>Еще больше знаний - на <a href="https://study.briskly.online/?skip_form">study.briskly.online</a></p>
                <a href="#" onclick="$('.js-popup-form').hide();
                        return false;" class="btn btn-green btn-small">Закрыть</a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function () {
            fbq('track', 'Lead', {content_name: 'mainform'});
            gtag('event', 'lead', {
                'event_category': 'general'
            });
            ym(50899424,'reachGoal','lead');
        }, 2000);
    </script>
<?}?>
