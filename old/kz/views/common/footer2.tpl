<div class="footer dark-bg color-white">
    <div class="wrapper-block">
        <div class="logo">
            <a href="/"><img src="/img/logo-white-new.svg" alt="" " /></a>
        </div>
        <div class="flex-wrapper">
            <div class="footer-block flex-column patent-info">
                <div class="footer-block-text">
                    <p>Продажа товаров без продавца</p>
                    <!--<p>Патент на технологию зарегистрирован <br />компанией Briskly за номером 2018117236</p>-->
                    <p>Патенты зарегистрированы: <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020619020&TypeFile=html" target="_blank">2020619020</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020619021&TypeFile=html" target="_blank">2020619021</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618645&TypeFile=html" target="_blank">2020618645</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618646&TypeFile=html" target="_blank">2020618646</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618899&TypeFile=html" target="_blank">2020618899</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618422&TypeFile=html" target="_blank">2020618422</a>.</p>
                </div>
                <div class="links-block">
                    <ul>
                        <li><a href="/user_doc" target="_blank">Пользовательское соглашение</a></li>
                        <li><a href="/wp-content/uploads/2019/08/%D0%94%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80-%D0%BE%D0%B1-%D0%BE%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD%D0%B8%D0%B8-%D1%83%D1%81%D0%BB%D1%83%D0%B3-%D0%BF%D0%BE-%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B5-Briskly-%D1%80%D0%B5%D0%B4.-2.2..pdf" target="_blank">Оферта для торговых точек</a></li>
                        <li><a href="/wp-content/uploads/2019/08/privacy.pdf" target="_blank">Обработка персональных данных</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-block links-block">
                <ul>
                    <li><a href="https://b-pay.online/" target="_blank">Приложение B-PAY</a></li>
                    <li><a href="//app.briskly.online" target="_blank">Скачать B-PAY</a></li>
                    <li><a href="/">Торговым точкам</a></li>
                    <li><a href="/briskly-for-shops/" target="_blank">Подключить магазин</a></li>
<!--                    <li><a href="/delivery/" target="_blank">Подключить заведение</a></li>-->
                    <li><a href="/briskly-micro-market/" target="_blank">Умный холодильник</a></li>
                    <!--li><a href="#">Магазин контейнер</a></li-->
                </ul>
            </div>
            <div class="footer-block links-block">
                <ul>
                    <li><a href="/briskly_for_partners/">Партнёрская программа</a></li>
                    <li><a href="/user_doc">Политика конфиденциальности</a></li>
                    <li><a href="/wp-content/uploads/2019/08/%D0%94%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80-%D0%BE%D0%B1-%D0%BE%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD%D0%B8%D0%B8-%D1%83%D1%81%D0%BB%D1%83%D0%B3-%D0%BF%D0%BE-%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B5-Briskly-%D1%80%D0%B5%D0%B4.-2.2..pdf">Публичная оферта</a></li>
                    <!--li><a href="#">Мы в СМИ</a></li-->
                    <li><a href="https://t.me/Brskl">Новости компании</a></li>
                    <li><a href="https://backoffice.briskly.online/company-authorization/">Кабинет</a></li>
                    <li><a href="https://briskly.online/contacts">Контакты</a></li>
                </ul>
            </div>
            <div class="footer-block flex-column foot-contacts">
                <div class="socila-networks">
                    <ul>
                        <li><a href="https://teleg.run/brskl"><img src="/img/social-networks/telegram-green.svg" alt="" /></a></li>
                        <li><a href="https://vk.com/briskly.online" target="_blank"><img src="/img/social-networks/vk-green.svg" alt="" /></a></li>
                    </ul>
                </div>
                <div class="footer-block-text">
                    <a href="tel:+77717555574" class="tel">+7771-755-55-74</a>
                    <a href="#feedback" class="standart-green request-call to-feedback">Заказать звонок</a>
                    <p class="copyright">© Briskly, <?= date('Y') ?></p>
                </div>
                <div class="pay-systems">
                    <ul>
                        <li><img src="/img/pay_systems/visa.svg" alt="visa"/></li>
                        <li><img src="/img/pay_systems/mastercard.svg" alt="mastercard"/></li>
                        <li><img src="/img/pay_systems/mir.svg" alt="mir"/></li>
                    </ul>
                </div>
            </div>
        </div>
        <p class="color-light-gray">Google Play и логотип Google Play являются товарными знаками корпорации Google Inc. Apple и логотип Apple являются зарегистрированными товарными знаками компании Apple Inc. в США и других странах. App Store является знаком обслуживания компании Apple Inc.</p>
        <a href="#firstblock" class="to-top"></a>
    </div>
    <!--<div class="mobile-bottom-btns">
        <a href="tel:+78006005096" class="tel btn btn-green">8-800-600-50-96</a>
        <a href="#feedback" class="to-mail btn btn-green-border to-feedback">Презентация</a>
    </div>-->
</div>