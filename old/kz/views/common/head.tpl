<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?= $title?></title>
        <meta name="description" content="<?= $description?>"/>
        <meta name="viewport" content="width=device-width">
        <meta name="robots" content="follow,index"/>
        <link rel="stylesheet" href="/css/style.css?v=<?= $version?>" />
        <link rel="stylesheet" href="/css/common-elems-styles.css?v=<?= $version?>" />
        <link rel="stylesheet" type="text/css" href="/css/tablet.css?v=<?= $version?>" media="only screen and (min-width: 768px) and (max-width: 1024px)" />
        <link rel="stylesheet" type="text/css" href="/css/common-elems-styles-tablet.css?v=<?= $version?>" media="only screen and (min-width: 768px) and (max-width: 1024px)" />
        <link rel="stylesheet" type="text/css" href="/css/mobile.css?v=<?= $version?>" media="only screen and (min-width: 320px) and (max-width: 767px)" />
        <link rel="stylesheet" type="text/css" href="/css/common-elems-styles-mobile.css?v=<?= $version?>" media="only screen and (min-width: 320px) and (max-width: 767px)" />
        <link rel="stylesheet" type="text/css" href="/css/en-style.css?v=<?= $version?>" />
        <link rel="stylesheet" href="/css/swiper.css" media="screen">
        <link rel="stylesheet" href="/css/jquery.fancybox.min.css" media="screen">

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-new.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5fda3e">
        <meta name="apple-mobile-web-app-title" content="Briskly">
        <meta name="application-name" content="Briskly">
        <meta name="msapplication-TileColor" content="#333333">
        <meta name="theme-color" content="#333333">

        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
        <link rel="canonical" href="https://briskly.online" />
        <meta property="og:locale" content="ru_RU">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Briskly">
        <meta property="og:description" content="<?= $description?>">
        <meta property="og:url" content="https://briskly.online">
        <meta property="og:site_name" content="briskly">
        <meta property="og:image" content="https://briskly.online/img/logo-briskly.png">
        <meta property="og:image:secure_url" content="https://briskly.online/img/logo-briskly.png">
        <meta property="og:image:width" content="1315">
        <meta property="og:image:height" content="599">
        <meta property="og:image:alt" content="Briskly">
        <meta property="og:image:type" content="image/png">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="Briskly">
        <meta name="twitter:description" content="<?= $description?>">
        <meta name="twitter:image" content="https://briskly.online/img/logo-briskly.png">
        <meta name="keywords" content="<?= $keywords?>" />
        <meta name="facebook-domain-verification" content="xsmkq0099hy8wskwkckxz6zxeryz30" />
        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174153759-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-174153759-1');
        </script>
    </head>
    <body>
