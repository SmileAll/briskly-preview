<div class="header">
    <div class="logo">
        <a href="/" class="logo-dark"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала, касса в смартфоне" /></a>
        <a href="/" class="logo-white"><img src="/img/logo-white-new.svg" alt="" /></a>
    </div>
    <menu>
        <ul>
            <li>
                <a href="#" class="open-extra-menu only-desktop js-open-extra-menu" data-block="extra1">B-Pay</a>
                <a href="https://b-pay.online/" class="only-mobile-view" target="_blank">B-Pay</a>
            </li>
            <li>
                <a href="#" class="open-extra-menu js-open-extra-menu" data-block="extra2">Продукты</a>
                <ul class="only-mobile">
                    <li><a href="/briskly-for-shops/" target="_blank">Касса в смартфоне</a></li>
                    <!-- <li><a href="/delivery/" target="_blank">Предзаказ и доставка</a></li> -->
                    <li><a href="/briskly-micro-market/" target="_blank">Микромаркеты</a></li>
                    <li><a href="https://study.briskly.online/" target="_blank">Обучающий портал</a></li>
                </ul>
            </li>
            <li>
                <a href="<?if($active_code === 'KZ'){?>https://kazakhstan.briskly.online<?} else {?>/briskly_for_partners/<?}?>" target="_blank">Стать партнёром</a>
            </li>
            <li><a href="#" class="open-extra-menu js-open-extra-menu" data-block="extra3">О компании</a>
                <ul class="only-mobile">
                    <li><a href="https://briskly.online/story/" target="_blank">История Briskly</a></li>
                    <li><a href="https://briskly.online/media/" target="_blank">Мы в СМИ</a></li>
                    <li><a href="https://briskly.online/vacancy/" target="_blank">Вакансии</a></li>
                    <li><a href="https://linnafrost.ru/" target="_blank">Linnafrost</a></li>
                    <li><a href="https://briskly.online/investors/" target="_blank">Инвесторам</a></li>
                </ul>
            </li>
            <li><a href="https://bbo.briskly.online/" target="_blank">Войти</a></li>
        </ul>
    </menu>
    <div class="header-right">
        <a href="tel:<?= $header_phone_url?>" class="tel"><?= $header_phone?></a>
        <ul class="languages js-languages">
            <? foreach ($langs_list as $lang) {?>
                <li<? if ($lang['active']) {?> class="active js-active-click"<? }?>><a  href="<?= $lang['code'] ? ('/' . $lang['code'].$url) : $url?>?location_code=<?= $lang['name']?>"><img src="/img/flags/<?= $lang['icon']?>.svg" alt="" /><span><?= $lang['name']?></span></a></li>
            <? }?>
        </ul>
        <a href="#" class="open-menu js-open-menu">
            <span></span>
            <span></span>
            <span></span>
        </a>
    </div>
    <!--<a href="/briskly-tariffs/" class="btn btn-green btn-tariffs" target="_blank">Тарифы</a>-->
</div>



<div class="extra-menu-wrapper only-mobile">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Скачайте приложение</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Пишите нам</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                    <a href="tel:<?=$header_phone_url?>" class="tel"><?=$header_phone?></a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="extra-menu-wrapper" data-block="extra1">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Скачайте приложение</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block" style="flex-grow: 1; max-width: 100%;">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="https://b-pay.online/" target="_blank">
                            <span class="h-style">Приложение B-Pay</span>
                            <span class="standart-text">Плати без очередей!</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl" target="_blank"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Пишите нам</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="extra-menu-wrapper" data-block="extra2">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Скачайте приложение</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="/briskly-for-shops/" target="_blank">
                            <span class="h-style">Касса в смартфоне</span>
                            <span class="standart-text">Магазинам</span>
                        </a>
                    </div>
                    <!-- <div class="ul-block-line">
                        <a href="/delivery/" target="_blank">
                            <span class="h-style">Предзаказ и доставка</span>
                            <span class="standart-text">Быстрые покупки без лишних контактов</span>
                        </a>
                    </div> -->
                    <div class="ul-block-line">
                        <!--<a href="/briskly-tariffs/" class="btn btn-big btn-green btn-tariffs" target="_blank">Тарифы</a>-->
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="/briskly-micro-market/" target="_blank">
                            <span class="h-style">Микромаркеты</span>
                            <span class="standart-text">Микромаркет, вендинг</span>
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://study.briskly.online/" target="_blank">
                            <span class="h-style">Обучающий портал</span>
                            <span class="standart-text">study.briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl" target="_blank"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Пишите нам</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="extra-menu-wrapper" data-block="extra3">
    <div class="wrapper-block">
        <div class="extra-menu-block">
            <div class="flex-block">
                <div class="download-app">
                    <span class="h-style">Скачайте приложение</span>
                    <div class="application-stores">
                        <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore" target="_blank"><img src="/img/appstore-white.svg" /></a>
                        <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><img src="/img/googleplay-white.svg" /></a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="https://briskly.online/story/" target="_blank">
                            <span class="h-style">О компании</span>
                            <span class="standart-text">История Briskly</span>
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://briskly.online/media/" target="_blank">
                            <span class="h-style">Мы в СМИ</span>
                            <span class="standart-text">Публикации</span>
                        </a>
                    </div>

                </div>
            </div>
            <div class="flex-block">
                <div class="ul-block">
                    <div class="ul-block-line">
                        <a href="https://linnafrost.ru/" target="_blank">
                            <span class="h-style" style="margin-left: 200px">Linnafrost</span>
                            <span class="standart-text" style="margin-left: 200px">ВЗХТ</span>
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://briskly.online/vacancy/" target="_blank">
                            <span class="h-style" style="margin-top: -85px">Вакансии</span>
                            <span class="standart-text">Работа в Briskly</span>
                        </a>
                    </div>
                    <div class="ul-block-line">
                        <a href="https://briskly.online/investors/" target="_blank">
                            <span class="h-style">Инвесторам</span>
                            <span class="standart-text">Информация инвесторам</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-block">
                <div class="contacts-block">
                    <div class="ul-block-line socila-networks">
                        <ul>
                            <li><a href="https://t.me/Brskl" target="_blank"><img src="/img/social-networks/telegram.svg" alt="" /></a></li>
                            <li><a href="https://vk.com/briskly.online"><img src="/img/social-networks/vk.svg" alt="" /></a></li>
                        </ul>
                    </div>
                    <div class="ul-block-line">
                        <a href="mailto:hello@briskly.online">
                            <span class="h-style">Пишите нам</span>
                            <span class="standart-text">hello@briskly.online</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
