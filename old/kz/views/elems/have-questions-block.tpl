<div class="wrapper-block color-white align-center">
    <div class="have-questions">
        <h2>Появились вопросы?</h2>
        <h4>Позвоните нашему менеджеру <br>или получите презентацию на почту</h4>
        <div class="btns-block">
            <a href="tel:+77717555574" class="btn btn-green btn-big btn300 tel">+7771-755-55-74</a>
            <a href="#feedback" class="btn btn-green-border btn-big btn300 to-feedback">Получить презентацию</a>
        </div>
    </div>
</div>