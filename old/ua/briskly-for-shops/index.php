<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Подключи приложение B-Pay от Briskly';
$description = 'Подключи магазин к приложению B-Pay, которое сканирует товары и помогает оплачивать их в смартфоне. Без очередей.';
$keywords = 'Брискли, сканируй и покупай, касса в смартфоне, микромаркет, scan&go, доставка, магазин без кассира, магазин без персонала, ритейл будущего';
require_once $path . '/views/common/head.tpl';
?>
<?= chunk('fb_chat', ['uk_UA']) ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color on-scan-go">
        <?php require_once $path . '/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper scan-go-top-block">
        <div class="wrapper-block flex-wrapper">
            <div class="flex-block w660">
                <div class="logo-top">
                    <a href="/"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала" /></a>
                </div>
                <div class="text-block">
                    <h3>Приложение B-Pay</h3>
                    <h3>Бесплатно подключим магазин к&nbsp;системе самообслуживания покупателя</h3>
                    <ul class="standart marker-abroad">
                        <li>Покупатели сканируют товары и оплачивают через смартфон</li>
                        <li>Магазин повышает продажи и сокращает очереди</li>
                        <!--<li>Работайте без кассира. Без очередей. 24/7</li>-->
                    </ul>
                    <div class="btns-block">
                        <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Связаться с нами</a>
                    </div>
                </div>
            </div>
            <div class="flex-block auto-width">
                <div class="phone-image-block">
                    <img src="/img/phone-screens/screen7.png" alt="" />
                    <img class="phone-frame" src="/img/phone-frame-gray-light.svg" alt="" />
                    <img class="icon-image" src="/img/icons/restaurant-icon-red.svg" alt="" />
                </div>
            </div>
        </div>
        <img class="bend bend-bottom" src="/img/bend-images/bend1.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg first-bend-block on-scan-go">
        <div class="wrapper-block">
            <div class="flex-wrapper center-items tablet-wrap" id="feedback">
                <div class="flex-block w40">
                    <div class="form-block white-bg br-20 left-alignment" id="my-form-1">
                        <p class="s18 semibold align-center">Получите презентацию о&nbsp;наших продуктах сейчас, решите потом</p>
                        <div class="form-wrapper">
                            <?php require_once $path . '/views/common/forms/19.tpl'; ?>
                        </div>
                    </div>
                </div>
                <div class="flex-block w60">
                    <h3>Что такое приложение <span class="nowrap">B-Pay?</span> <br />Касса в&nbsp;смартфоне покупателя.</h3>
                    <h4>Scan&amp;Go&nbsp;&mdash; это функция приложения B-Pay, где&nbsp;покупатели могут сканировать штрихкоды товаров&nbsp;и&nbsp;оплачивать их&nbsp;прямо в&nbsp;смартфоне.</h4>
                    <p class="s18">В приложение B-Pay встроены следующие функции:</p>
                    <ul class="standart marker-abroad">
                        <li>Приложение для охраны с&nbsp;функцией контроля покупок&nbsp;&mdash; <span class="nowrap">ЧЕК-ЕР</span></li>
                        <li>Отправка чеков в&nbsp;ОФД и&nbsp;клиенту</li>
                        <li>Встроенный эквайринг</li>
                        <li>Техническая поддержка</li>
                        <li>Маркетинг</li>
                    </ul>
                </div>
            </div>
            <div class="flex-wrapper">
                <div class="flex-block pluses-for-you-wrapper">
                    <span class="s24 middle" style="letter-spacing: -1px;">Ваша торговая точка может продавать на&nbsp;15%&nbsp;больше и&nbsp;работать в&nbsp;два раза быстрее. Без&nbsp;персонала, без&nbsp;очередей.</span>
                    <ul class="pluses-for-you">
                        <li>
                            <img src="/img/icons/chel×2-icon.svg" alt="" class="green-shadow" />
                            <p class="s18">Обслуживать <strong>в&nbsp;два раза больше клиентов</strong> за&nbsp;то&nbsp;же&nbsp;время работы, без расширения штата и&nbsp;торговых площадей.</p>
                        </li>
                        <li>
                            <img src="/img/icons/growth-icon.svg" alt="" class="blue-shadow" />
                            <p class="s18"><strong>Увеличить выручку на&nbsp;15% за&nbsp;3&nbsp;месяца</strong>, без&nbsp;расходов на&nbsp;кассиров, охрану, работу в&nbsp;круглосуточном режиме.</p>
                        </li>
                        <li>
                            <img src="/img/icons/compass-icon.svg" alt="" class="pink-shadow" />
                            <p class="s18"><strong>Получить новый канал продаж.</strong> Знать, что хочет покупатель и&nbsp;убеждать его тратить больше, быстрее и&nbsp;именно в&nbsp;вашем магазине.</p>
                        </li>
                    </ul>
                </div>
                <div class="flex-block right-position">
                    <div class="image-wrapper">
                        <img src="/img/scan-go/content-images/image1.png" alt="Приложение B-Pay от Briskly помогает сканировать товары и полачивать их через смартфон." />
                    </div>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="" />
        <div class="to-video">
            <a href="#video" class="to-video">
                <span>Смотреть, как<br />это работает</span>
                <img src="/img/triangle.svg" alt="" />
            </a>
        </div>
        <img class="bend special" src="/img/bend-images/bend2-special.svg" alt="" />
    </div>
    <div class="full-width scan-go-work" id="video">
        <div class="wrapper-block">
            <h2 class="align-center">Как работает Scan&Go</h2>
            <div class="video-wrapper">
                <div class="video-block">
                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/nhIEmHAKd4I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <!--<iframe width="100%" height="100%" src="https://www.youtube.com/embed/6YcvNIwSuDw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
                </div>
            </div>
            <div class="btns-block application-stores center-position">
                <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore"><img src="/img/appstore.svg" alt=""></a>
                <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="/img/googleplay.svg" alt=""></a>
            </div>
        </div>
        <div class="wrapper-block">
            <h3 class="align-center">Комиссия приложения&nbsp;&mdash;&nbsp;5%, включая эквайринг</h3>
            <h4 class="align-center semibold">Мы принимаем платежи:</h3>
                <div class="flex-table-view">
                    <div class="table-tr">
                        <div class="table-cell">
                            <div class="logos-block js-z-index">
                                <img src="/img/pay_systems/sbp.svg" alt="" />
                            </div>
                            <span>СБП</span>
                        </div>
                    </div>
                    <div class="table-tr">
                        <div class="table-cell">
                            <div class="logos-block">
                                <img src="/img/pay_systems/visa2.svg" alt="" />
                                <img src="/img/pay_systems/mir2.svg" alt="" />
                                <img src="/img/pay_systems/mastercard2.svg" alt="" />
                            </div>
                            <span>Банковские карты </span>
                        </div>
                    </div>
                    <div class="table-tr">
                        <div class="table-cell">
                            <div class="logos-block">
                                <img src="/img/pay_systems/apple-pay.svg" alt="" />
                            </div>
                            <span>Apple Pay</span>
                        </div>
                    </div>
                </div>
        </div>
        <div class="wrapper-block like-cashbox">
            <div class="flex-wrapper tablet-wrap">
                <div class="flex-block w60">
                    <h3>B-Pay&nbsp;&mdash; это касса и&nbsp;интернет-магазин в&nbsp;смартфоне покупателя</h3>
                    <ul class="standart marker-abroad">
                        <li>Магазин подключается за&nbsp;1&nbsp;день.</li>
                        <li>Не&nbsp;требуется дополнительное оборудование для старта.</li>
                        <li>Экономит время покупателя.</li>
                        <li>Низкая стоимость внедрения.</li>
                        <li>Ещё один канал дистрибуции в&nbsp;смартфоне покупателя.</li>
                    </ul>
                    <div class="flex-wrapper tab-no-wrap">
                        <div class="flex-block">
                            <span class="fs30 bold color-green">+15-19%</span>
                            <p class="s18">к&nbsp;среднему чеку при&nbsp;оплате в&nbsp;приложении</p>
                        </div>
                    </div>
                </div>
                <div class="flex-block w40">
                    <div class="image-wrapper have-small-icon-blocks">
                        <img class="main-image" src="/img/scan-go/content-images/image3.png" alt="" />
                        <div class="small-icon-block azs">
                            <img src="/img/small-icons/icon1.svg" alt="" />
                            <span class="t-up">10 мин</span>
                            <span>заправиться<br />на&nbsp;АЗС</span>
                        </div>
                        <div class="small-icon-block sup-m">
                            <img src="/img/small-icons/icon2.svg" alt="" />
                            <span class="t-up">5 мин</span>
                            <span>купить в&nbsp;супермаркете</span>
                        </div>
                        <div class="small-icon-block bac-green micro-m">
                            <img src="/img/small-icons/icon3.svg" alt="" />
                            <span class="t-up">90 сек</span>
                            <span>купить в&nbsp;микромаркете</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="full-width light-gray-bg">
        <img class="bend bend-top" src="/img/bend-images/bend3-top.svg" />
        <div class="wrapper-block about-delivery">
            <div class="flex-wrapper about-delivery-top-block tablet-wrap center-items">
                <div class="flex-block w60">
                    <h3>Доставка ваших товаров за&nbsp;40&nbsp;мин</h3>
                    <h4>Подключим ваш магазин или кафе за&nbsp;срок&nbsp;от&nbsp;1&nbsp;до&nbsp;7&nbsp;дней.</h4>
                    <div class="text-block">
                        <span class="s18 bold">Быстро</span>
                        <p>Подключим ваш магазин или кафе за срок от&nbsp;1&nbsp;до&nbsp;7&nbsp;дней. Автоматически интегрируем в&nbsp;приложение ассортимент товаров из&nbsp;1С, iiko и&nbsp;других систем товароучёта.</p>
                        <a href="#" class="standart-gray border-decor">Конкуренты</a> -->
    <!-- </div>
    <div class="text-block">
        <span class="s18 bold">Дёшево</span>
        <p>Мы&nbsp;подключим ваш магазин или кафе бесплатно. Средняя комиссия за&nbsp;операцию&nbsp;&mdash; 5%.</p>
        <a href="#" class="standart-gray border-decor">Конкуренты</a> -->
    <!-- </div>
</div>
<div class="flex-block w40">
    <div class="image-wrapper">
        <img src="/img/scan-go/content-images/image2.png" alt="" />
    </div>
</div>
</div>
<div class="flex-wrapper about-delivery-block">
<div class="flex-block left-position">
    <img src="/img/icons/men-icon.svg" alt="" class="blue-shadow" />
    <span class="s18 fatter">Пешие курьеры</span>
    <ul class="standart marker-abroad">
        <li>Только в Москве в пределах ТТК</li>
        <li>Фиксированная стоимость доставки</li>
        <li>Заказы до 5 кг и до 80 см</li>
    </ul>
    <p class="color-light-gray">Пеший курьер в&nbsp;среднем прибывает за&nbsp;15-20&nbsp;минут, водитель&nbsp;&mdash; за&nbsp;7-10&nbsp;минут. Курьер сразу отправляется к&nbsp;покупателю.</p>
</div>
<div class="flex-block right-position">
    <img src="/img/icons/car-icon.svg" alt="" class="purple-shadow" />
    <span class="s18 fatter">Водители</span>
    <ul class="standart marker-abroad">
        <li>В более 700 городах, где есть Gett, Яндекс.Такси такси "Максим" и&nbsp;Dostavista</li>
        <li>В Москве — до 30 км от МКАД</li>
        <li>Стоимость доставки рассчитывается автоматически</li>
        <li>Заказы до 20 кг и до 180 см</li>
    </ul>
    <p class="color-light-gray">Вы&nbsp;можете включить оплату доставки целиком за&nbsp;счёт клиента, обозначить фиксированную стоимость или самостоятельно платить за&nbsp;доставку.</p>
</div>
</div>
<div class="our-delivery-service">
<ul>
    <li>
        <p style="color: #6d818c;">Интеграции:</p>
        <img class="mts-logo" src="/img/mts-logo.svg" alt="" />
        <p>а так же с 1с, iiko, r-keeper</p>
    </li>
    <li>
        <p style="color: #6d818c;">Доставку осуществляет:</p>
        <img src="/img/delivery-logos/yandex-taxi-logo.svg" alt="" />
    </li>
</ul>
</div> -->
    <!--<div class="flex-wrapper our-delivery-service">
        <p>Доставляют</p>
        <img src="/img/delivery-logos/yandex-taxi-logo.svg" alt="" />
        <img src="/img/delivery-logos/gett-logo.svg" alt="" />
        <img src="/img/delivery-logos/maxim-logo.svg" alt="" />
        <img src="/img/delivery-logos/dostavista-logo.svg" alt="" />
    </div>-->
    <!-- </div>
    <img class="bend bend-bottom" src="/img/bend-images/bend3-bottom.svg" />
</div> -->
    <div class="full-width">
        <div class="wrapper-block about-discounts on-scan-go">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h2>Впервые на&nbsp;рынке:<br />скидки и&nbsp;акции предлагаем <br />в&nbsp;моменте покупки.</h2>
                </div>
                <div class="flex-block w40 align-center">
                    <img src="/img/main-page-images/discounts-phone.png" alt="Приложение B-Pay от Briskly - касса в смартфоне, сканируё и покупай" />
                </div>
            </div>
        </div>
        <!--<div class="wrapper-block about-discounts on-scan-go">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h3>Впервые на&nbsp;рынке: скидки&nbsp;и&nbsp;акции в&nbsp;момент покупки, в&nbsp;чате приложения.</h3>
                    <h4>В&nbsp;тот момент, когда это действительно нужно&nbsp;покупателю!</h4>
                    <p class="s18">Персональный подход к&nbsp;покупателю&nbsp;&mdash; больше не&nbsp;абстрактные слова.</p>
                    <p class="s18">Вы&nbsp;действительно знаете, что им&nbsp;нужно. Изучайте факты, которые платформа выявляет на&nbsp;основе анализа и&nbsp;сбора покупательских данных.</p>
                    <p class="s18">Предлагайте скидки и&nbsp;акции на&nbsp;базе доказанных поведенческих паттернов, а&nbsp;не&nbsp;догадок.</p>
                </div>
                <div class="flex-block w40 align-right">
                    <div class="phone-image-block">
                        <img src="/img/phone-screens/screen3.png" alt="" />
                        <img class="phone-frame" src="/img/phone-frame-dark.svg" alt="" />
                    </div>
                </div>
            </div>
        </div>-->
    </div>
    <div class="full-width light-gray-bg">
        <img src="/img/bend-images/bend3-top.svg" />
        <div class="embedded-marketing on-scan-go">
            <?php require_once $path . '/views/elems/embedded-marketing.tpl'; ?>
        </div>
    </div>
    <div class="full-width">
        <img class="bend" src="/img/bend-images/bend5.svg" alt="" />
        <div class="wrapper-block included-in-price">
            <div class="flex-wrapper center-items tablet-wrap">
                <div class="flex-block w60">
                    <h3>Включено в&nbsp;стоимость комиссии:</h3>
                    <ul class="standart marker-abroad s24">
                        <li>Приложение для охраны с&nbsp;функцией контроля покупок&nbsp;&mdash; ЧЕК-ЕР</li>
                        <li>Отправка чеков в&nbsp;ОФД и&nbsp;клиенту</li>
                        <li>Встроенный эквайринг</li>
                        <li>Техническая поддержка</li>
                        <li>Встроенная программа лояльности</li>
                    </ul>
                </div>
                <div class="flex-block w40 image-wrapper">
                    <img src="/img/main-page-images/briskly-components-new.svg" alt="" />
                </div>
            </div>
            <div class="str-after on-scan-go">
                <div>
                    <span class="s24 middle align-center">Быстро покупать, легко оплачивать</span>
                    <img src="/img/str-green.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $path . '/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $path . '/views/elems/work-steps-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom-gray.svg" alt="" />
    </div>


    <div class="full-width p85">
        <div class="wrapper-block">
            <h3 class="align-center">Частые вопросы</h3>
            <div class="questions-wrapper">
                <div class="question-block js-opened-text">
                    <span>Как происходит интеграция?</span>
                    <div class="text">
                        <p>Первый шаг&nbsp;&mdash; это договор и&nbsp;счёт. Вы&nbsp;заполняете заявку на&nbsp;подключение в&nbsp;кабинете. Затем акцептируйте оферту и&nbsp;оплачиваете счет.</p>
                        <p>Второй&nbsp;&mdash; интеграция и&nbsp;запуск. Интеграция ассортимента на&nbsp;платформу и&nbsp;в&nbsp;приложение происходит автоматически для&nbsp;1C, iiko, Супермаг и&nbsp;других систем товароучёта. А&nbsp;также через API. При желании, вы&nbsp;можете заполнить товары вручную.</p>
                        <p>Далее, мы&nbsp;автоматически интегрируем все товары в&nbsp;приложение ЧЕК-ЕР для кассиров и&nbsp;B-Pay для покупателей.</p>
                        <p>Раз в&nbsp;сутки магазин получает заработанные средства на&nbsp;расчетный счет (за&nbsp;вычетом комиссии Briskly).</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Подключение действительно занимает один день?</span>
                    <div class="text">
                        <p>Если данные о&nbsp;ваших товарах хранятся в&nbsp;1С, iiko или Excel-таблице, то&nbsp;мы&nbsp;действительно можем подключить вас за&nbsp;один день.</p>
                    </div>
                </div>
                <div class="question-block js-opened-text">
                    <span>Как работает продажа алкоголя и&nbsp;сигарет через приложение?</span>
                    <div class="text">
                        <p>Для покупки данных товарных категорий пользователь должен быть верифицирован по&nbsp;паспорту в&nbsp;нашем приложении. При попытке купить табак или алкоголь, B-Pay запрашивает фото паспорта и&nbsp;фото покупателя с&nbsp;паспортом. Данные паспорта проверяет сотрудник Briskly и,&nbsp;после успешной верификации, товарная категория доступна для покупки.</p>
                        <p>Паспорт можно запрашивать как при регистрации, мотивируя возможностью покупки алкоголя и&nbsp;табака, так и&nbsp;в&nbsp;моменте&nbsp;&mdash; при попытке сканирования.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/views/elems/map2.tpl'; ?>
    <?php require_once $path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
