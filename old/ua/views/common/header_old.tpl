<div class="logo">
    <a href="/" class="logo-dark"><img src="/img/logo-dark-new.svg" alt="" /></a>
    <a href="/" class="logo-white"><img src="/img/logo-white-new.svg" alt="" /></a>
</div>
<div class="header">
    <menu>
        <ul>
            <li><a href="https://b-pay.online/">Приложение B-Pay</a></li>
            <li>
                <a href="#" class="open-extra-menu" onclick="return false;">Продукты</a>
                <ul class="extra-menu">
                    <li><a href="/briskly-for-shops/" target="_blank">Scan&Go</a></li>
                    <li><a href="/briskly-micro-market/" target="_blank">Умный холодильник</a></li>
<!--                    <li><a href="/delivery/" target="_blank">Предзаказ в ресторанах</a></li>-->
                </ul>
            </li>
            <li><a href="/briskly-for-partners/" target="_blank">Стать партнёром</a></li>
            <!--<li><a href="#">О компании</a></li>-->
            <li><a href="https://backoffice.briskly.online/company-authorization/">Кабинет</a></li>
            <li><a href="https://briskly.online/contacts">Контакты</a></li>
        </ul>
    </menu>
    <div class="header-right">
        <a href="tel:+78006005096" class="tel">8-800-600-50-96</a>
        <!--<ul class="languages">
            <li><a class="active" href="#"><img src="/img/flags/rus.svg" alt="" /><span>ru</span></a></li>
            <li><a href="#"><img src="/img/flags/eng.svg" alt="" /><span>en</span></a></li>
        </ul>-->
        <a href="#" class="open-menu js-open-menu">
            <span></span>
            <span></span>
            <span></span>
        </a>
    </div>
</div>