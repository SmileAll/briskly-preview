<div class="our-clients">
    <div class="wrapper-block">
        <h2>В этих компаниях работает Briskly</h2>
    </div>
    <div class="swiper-container js-clients-carousel-line-1">
        <div class="swiper-wrapper"  style="transition-timing-function: linear;">
            <div class="swiper-slide">
                <a href="http://sk.ru" target="_blank" class="carousel-image"><img src="/files/clients-logos/skolkovo.svg" alt="" /></a>
                <a href="https://ru.semrush.com/" target="_blank" class="carousel-image"><img src="/files/clients-logos/semrush.svg" alt="" /></a>
                <a href="https://unecon.ru/" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/unecon_small.jpg">
                        <img src="/files/clients-logos/unecon.jpg" alt="" />
                    </picture>
                </a>
                <a href="https://www.waveaccess.ru/" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/waveaccess_small.png">
                        <img src="/files/clients-logos/waveaccess.png" alt="" />
                    </picture>
                </a>
                <a href="https://softline.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/softline.svg" alt="" /></a>
                <a href="https://korusconsulting.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/korus.svg" alt="" /></a>
            </div>
        </div>
    </div>
    <div class="swiper-container js-clients-carousel-line-2">
        <div class="swiper-wrapper"  style="transition-timing-function: linear;">
            <div class="swiper-slide">
                <a href="https://dataart.ua/" target="_blank" class="carousel-image"><img src="/files/clients-logos/dataart.svg" alt="" /></a>
                <a href="https://www.invitro.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/invitro.svg" alt="" /></a>
                <a href="https://www.rosbank.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/rosbank.svg" alt="" /></a>
                <a href="https://sperasoft.ru/" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/sperasoft_small.png">
                        <img src="/files/clients-logos/sperasoft.png" alt="" />
                    </picture>
                </a>
                <a href="https://digdes.ru/" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/digital-design_small.png">
                        <img src="/files/clients-logos/digital-design.png" alt="" />
                    </picture>
                </a>
                <a href="https://zoloto585.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/585gold.svg" alt="" /></a>
                <a href="https://www.realweb.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/realweb.svg" alt="" /></a>
                <a href="https://msk.tele2.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/tele2.svg" alt="" /></a>
            </div>
        </div>
    </div>
    <div class="swiper-container js-clients-carousel-line-3">
        <div class="swiper-wrapper"  style="transition-timing-function: linear;">
            <div class="swiper-slide">
                <a href="https://www.ibs.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/ibs.svg" alt="" /></a>
                <a href="https://aeromar.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/aeromar_rus_180.png" alt="" /></a>
                <a href="http://www.titan2.ru/" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/titan2_small.png">
                        <img src="/files/clients-logos/titan2.png" alt="" />
                    </picture>
                </a>
                <a href="https://trace-studio.com/" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/trace-studio_small.jpg">
                        <img src="/files/clients-logos/trace-studio.jpg" alt="" />
                    </picture>
                </a>
                <a href="https://www.speechpro.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/crt.svg" alt="" /></a>
                <a href="https://scout-gps.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/scout.svg" alt="" /></a>
                <a href="https://selectel.ru/" target="_blank" class="carousel-image"><img src="/files/clients-logos/selectel.svg" alt="" /></a>
                <a href="http://alfa1eads.ru" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/alfaleads_small.png">
                        <img src="/files/clients-logos/alfaleads.png" alt="" />
                    </picture>
                </a>
                <a href="https://biocad.ru/" target="_blank" class="carousel-image">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/clients-logos/biocad_small.png">
                        <img src="/files/clients-logos/biocad.png" alt="" />
                    </picture>
                </a>
            </div>
        </div>
    </div>
</div>