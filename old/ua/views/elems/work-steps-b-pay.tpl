<div class="wrapper-block p85">
    <div class="flex-wrapper center-items">
        <div class="flex-block w30 on-mobile-none">
            <div class="phone-image-block tab-w100">
                <picture>
                    <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/phone-screens/screen-scan-basket_small.png">
                    <img src="/img/phone-screens/screen-scan-basket.png" alt="" />
                </picture>
                <img class="phone-frame" src="/img/phone-frame-green-border.svg" alt="" />
            </div>
        </div>
        <div class="flex-block w70 steps-wrapper">
            <h2>Как работает B-Pay?</h2>
            <div class="work-steps-wrapper">
                <div class="work-step-block">
                    <div class="icon-block">
                        <img class="green-shadow" src="/img/icons/restaurant-icon-new.svg" alt="" />
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Выберите торговую точку</span>
                        <p>Торговые точки показаны списком по геолокации или на карте</p>
                    </div>
                </div>
                <div class="work-step-block">
                    <div class="icon-block">
                        <img class="green-shadow" src="/img/icons/barcode-icon-new.svg" alt="" />
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Добавьте продукты</span>
                        <p>Сканируйте штрихкод товара и он автоматически добавится в корзину</p>
                    </div>
                </div>
                <div class="work-step-block">
                    <div class="icon-block">
                        <img class="green-shadow" src="/img/icons/payment-icon-new.svg" alt="" />
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Оплатите</span>
                        <p>Доступна оплата привязанной картой или Apple Pay</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>