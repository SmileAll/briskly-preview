<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Briskly Micromarkets | Smart stand-alone refrigerators';
$description = 'A smart refrigerator by Briskly where you can sell groceries and ready meals. Opens via the B-Pay app. Install a micromarket in your city and start making money! From 1300 $.';
$keywords = 'Briskley, scan and buy, micromarket, smart refrigerator, vending, scan&go, shop without a cashier, shop without staff, micromarket at the entrance';
require_once $path . '/us/views/common/head.tpl';
?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/us/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-micromarket-page">
        <div class="top-block-left-spec-bg">
            <div class="w660 absolute-position">
                <div class="logo-top">
                    <a href="/"><img src="/img/logo-dark-new.svg" alt="Briskly - платформа для торговли без персонала" /></a>
                </div>
                <div class="text-block">
                    <h1>Micromarket Briskly <br />with your goods for 2100 $ <br /><span class="nowrap"></span></h1>
                    <ul class="standart marker-abroad">
                        <li>Ready sales point for your goods;</li>
                        <li>Payments by card or through the mobile app;</li>
                        <li>Sale products under your brand logo</li>
                    </ul>
                    <div class="btns-block">
                        <a href="#feedback" class="btn btn-big btn-green btn300 to-feedback">Apply</a>
                    </div>
                    <a href="#feedback" class="see-more to-feedback">Details below</a>
                </div>
            </div>
            <div class="img-block-responsive">
                <div class="image-wrapper">
                    <img class="main-image max-height" src="/img/М4_1.png" alt="Умный холодильник от Briskly открывается через приложение B-Pay, магазин без кассира" />
                </div>
            </div>
        </div>
    </div>
    <a name="form" id="form"></a>
    <div class="form-block-wrapper">
        <div class="form-block-wrapper-special-bg">
            <div class="wrapper-block" id="feedback">
                <h2 class="color-white">Receive the presentation by mail <br />or leave your phone</h2>
                <div class="flex-wrapper color-white">
                    <div class="flex-block left-position">
                        <h4 class="middle">Get it all now, make a decision later</h4>
                        <div class="form-block-1">
                            <?php require_once $path . '/us/views/common/forms/49.tpl'; ?>
                        </div>
                    </div>
                    <div class="flex-block">
                        <div class="video-block">
                            <a href="https://youtu.be/wHyvyqfE0vo" class="fancybox" data-fancybox>
                                <img src="/img/micro-market/content-images/video-preview1.jpg" alt="" />
                                <img class="video-icon" src="/img/play-icon.svg" alt="" />
                            </a>
                        </div>
                    </div>
                    <div class="flex-block full-width">
                        <p class="s18">Smart fridges are a new way of delivering fresh, ready-made food to an office, business center, or any other place. You will get a ready-made refrigerating kiosk with a possibility of payment via the application with a network security for 2100 $.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="dots"></div>
    </div>
    <div class="gallery-wrapper">
        <div class="wrapper-block">
            <h3>#briskly <span class="color-light-gray">#needBriskly #brisklyisthefuture</span></h3>
        </div>
        <div class="swiper-container js-micromarket-gallery">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img1_small.jpg">
                        <img src="/img/micro-market/gallery-images/img1.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img2_small.jpg">
                        <img src="/img/micro-market/gallery-images/img2.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img3_small.jpg">
                        <img src="/img/micro-market/gallery-images/img3.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img4_small.jpg">
                        <img src="/img/micro-market/gallery-images/img4.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img5_small.jpg">
                        <img src="/img/micro-market/gallery-images/img5.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img6_small.jpg">
                        <img src="/img/micro-market/gallery-images/img6.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img7_small.jpg">
                        <img src="/img/micro-market/gallery-images/img7.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img8_small.jpg">
                        <img src="/img/micro-market/gallery-images/img8.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img9_small.jpg">
                        <img src="/img/micro-market/gallery-images/img9.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img12_small.jpg">
                        <img src="/img/micro-market/gallery-images/img12.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img13_small.jpg">
                        <img src="/img/micro-market/gallery-images/img13.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img14_small.jpg">
                        <img src="/img/micro-market/gallery-images/img14.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img15_small.jpg">
                        <img src="/img/micro-market/gallery-images/img15.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img16_small.jpg">
                        <img src="/img/micro-market/gallery-images/img16.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img17_small.jpg">
                        <img src="/img/micro-market/gallery-images/img17.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img18_small.jpg">
                        <img src="/img/micro-market/gallery-images/img18.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img19_small.jpg">
                        <img src="/img/micro-market/gallery-images/img19.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img20_small.jpg">
                        <img src="/img/micro-market/gallery-images/img20.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img21_small.jpg">
                        <img src="/img/micro-market/gallery-images/img21.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img22_small.jpg">
                        <img src="/img/micro-market/gallery-images/img22.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img23_small.jpg">
                        <img src="/img/micro-market/gallery-images/img23.jpg" alt="" />
                    </picture>
                </div>
                <div class="swiper-slide">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/gallery-images/img24_small.jpg">
                        <img src="/img/micro-market/gallery-images/img24.jpg" alt="" />
                    </picture>
                </div>
            </div>
        </div>
    </div>
    <div class="for-whom-wrapper">
        <div class="wrapper-block">
            <a name="forwho" id="forwho"></a>
            <h2 class="align-center">Who prefer micromarkets for buisness?</h2>
            <div class="flex-wrapper mobile-reverse for-whom">
                <div class="flex-block for-image left-position">
                    <div class="dots green-dots"></div>
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 767px)" srcset="/img/micro-market/content-images/wide-icebox-new.jpg">
                        <img src="/img/micro-market/content-images/wide-icebox-new.png" alt="" />
                    </picture>
                </div>
                <div class="flex-block">
                    <div class="for-whom-block">
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/chef-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Kitchen factories and manufacturers<br /> of ready-made dishes</h4>
                                <p>Grow your business, connect new distribution channels through micromarkets. </p>
                            </div>
                        </div>
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/restaurant-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Restaurants and Cafes</h4>
                                <p>Grow your network with the new format. Deliver fresh meals to offices and public institutions.</p>
                            </div>
                        </div>
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/invest-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Investors</h4>
                                <p>Invest in a clear and transparent niche with high returns. Minimum risks due to lack of staff.</p>
                            </div>
                        </div>
                        <div class="icon-text-item">
                            <div class="icon-image">
                                <img src="/img/micro-market/icons/calculator-icon.svg" alt="" />
                            </div>
                            <div class="icon-text">
                                <h4 class="bold">Businessmen</h4>
                                <p>Try yourself in a new field - we will provide you with all the necessary tools. Low entry threshold:  2100 $.</p>
                            </div>
                        </div>
                        <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" class="standart-dark" target="_blank">Show examples</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="installation-options-wrapper">
        <div class="wrapper-block">
            <h2 class="align-center">Installation options</h2>
            <div class="installation-options-carousel swiper-container js-installation-options-block">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/box-new.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Installing the system to your refrigerator</h4>
                                    <p>Installing an electronic lock</p>
                                    <p>Connect to the B-Pay app</p>
                                    <p>Assortment Integration</p>
                                    <p>Temperature sensors</p>
                                    <p>Surveillance Camera </p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">1300 $</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper spec-type">
                            <span class="popular">Popular option</span>
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micromarketnews2.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Smart turnkey refrigerator</h4>
                                    <p>Fridge included</p>
                                    <p>Micromarket Branding</p>
                                    <p>Electronic lock</p>
                                    <p>Connection to the B-Pay app</p>
                                    <p>Assortment Integration</p>
                                    <p>Temperature sensors</p>
                                    <p>Surveillance Camera</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">2100 $</span>
                                    <a href="#feedback" class="btn btn-green btn-big to-feedback">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/mm_slide.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Large turnkey kiosk</h4>
                                    <p>Double-leaf refrigerator</p>
                                    <p>Micromarket Branding</p>
                                    <p>Electronic lock</p>
                                    <p>Connection to the B-Pay app</p>
                                    <p>Assortment Integration</p>
                                    <p>Temperature sensors</p>
                                    <p>Surveillance Camera</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">2800 $</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="align-center">Additional modules:</h2>
            <div class="installation-options-carousel swiper-container js-installation-options-block-2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/coffee-machine.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Coffee machine</h4>
                                    <p>Ordering drinks via the B-Pay mobile app</p>
                                    <p>Customizing your assortment</p>
                                    <!-- <p>Built-in acquiring</p> -->
                                    <p>Customer support</p>
                                    <p>Online platform for merchant management</p>
                                    <p>Mobile service maintenance</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">3500 $</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/freezer-new.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Freezer cabinet</h4>
                                    <p>Temperature range: from -18 to -24</p>
                                    <p>Integration with accounting system</p>
                                    <p>Electronic lock</p>
                                    <p>Videocams</p>
                                    <p>B-Pay mobile app</p>
                                    <p>Technical support</p>
                                </div>
                                <div class="price">
                                    <span class="fs40 fatter service-cost">3400 $</span>
                                    <a href="#feedback" class="btn btn-white btn-big to-feedback">Apply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="installation-type-wrapper">
                            <div class="installation-type">
                                <div class="img-block">
                                    <img src="/img/micro-market/content-images/terminal-new1.png" alt="" />
                                </div>
                                <div class="installation-type-text">
                                    <h4 class="bold">Card payment terminal</h4>
                                    <p>Additional terminal for payment without an application</p>
                                    <p><strong>Includes:</strong></p>
                                    <p>Product barcode scanner</p>
                                    <p>Acceptance of bank cards</p>
                                    <p>Issuance of electronic checks</p>
                                    <p>Technical support</p>
                                </div>
                                <div class="price">
                                    <!-- <span class="fs40 fatter service-cost">1000 $</span> -->
                                    <p><strong>Only for Russia</strong></p>
                                    <!-- <a href="#feedback" class="btn btn-white btn-big to-feedback">Apply</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a name="where" id="where"></a>
    <div class="places-of-use">
        <div class="wrapper-block">
            <h2>Where to put a micromarket?</h2>
        </div>
        <div class="places-carousel swiper-container js-places-block">
            <div class="swiper-wrapper">
                <div class="swiper-slide first-elem-1"></div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img1_small.jpg">
                            <img src="/img/micro-market/places-images/img1.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>To the offices</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img2_small.jpg">
                            <img src="/img/micro-market/places-images/img2.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>To business centers</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img3_small.jpg">
                            <img src="/img/micro-market/places-images/img3.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>To outlets</span>
                            <div class="food-icons-active">
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                                <div class="food-icon non-food"></div>
                                <div class="food-icon freezing"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img4_small.jpg">
                            <img src="/img/micro-market/places-images/img4.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>To the manufacturing enterprises</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img5_small.jpg">
                            <img src="/img/micro-market/places-images/img5.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>To the educational institution</span>
                            <div class="food-icons-active">
                                <div class="food-icon non-food"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img6_small.jpg">
                            <img src="/img/micro-market/places-images/img6.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>To the gyms</span>
                            <div class="food-icons-active">
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snacks"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="place-item">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/micro-market/places-images/img7_small.jpg">
                            <img src="/img/micro-market/places-images/img7.jpg" alt="" />
                        </picture>
                        <div class="place-item-info">
                            <span>To the administrative institutions</span>
                            <div class="food-icons-active">
                                <div class="food-icon salad"></div>
                                <div class="food-icon sandwich"></div>
                                <div class="food-icon coffee"></div>
                                <div class="food-icon snaks"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block">
            <ul class="food-types">
                <li>
                    <img src="/img/micro-market/food-icons/salad.svg" alt="" />
                    <span>Fresh dishes</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/sandwich.svg" alt="" />
                    <span>Lunches</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/freezing.svg" alt="" />
                    <span>Freezing</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/coffee.svg" alt="" />
                    <span>Beverages</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/snacks.svg" alt="" />
                    <span>Snacks</span>
                </li>
                <li>
                    <img src="/img/micro-market/food-icons/non-food.svg" alt="" />
                    <span>Non-food</span>
                </li>
            </ul>
        </div>
    </div>



    <div class="full-width">
        <img class="bend" src="/img/bend-images/bend10-top.svg" alt="">
        <a name="how" id="how"></a>
        <div class="white-bg">
            <div class="wrapper-block icebox-work-steps-wrapper">
                <h2 class="align-center">How does it work?</h2>
                <div class="icebox-work-steps-block">
                    <div class="work-step-block">
                        <img src="/img/micro-market/drawing-icons/icebox.svg" alt="" />
                        <h4 class="bold">Installation</h4>
                        <p class="s18">Micromarket is put to the office, gym or any other place. 1 square. meter and A socket are required.</p>
                    </div>
                    <div class="work-step-block">
                        <img src="/img/micro-market/drawing-icons/girl.svg" alt="" />
                        <h4 class="bold">Fill the goods</h4>
                        <p class="s18">The courier replenishes the fridge with fresh goods and picks up the leftoverse veryday. There is a special access for him via an app.</p>
                    </div>
                    <div class="work-step-block">
                        <img src="/img/micro-market/drawing-icons/men.svg" alt="" />
                        <h4 class="bold">Transferring your profit</h4>
                        <p class="s18">Once a day you get money for the sold goods to your company’s account. Acquiring is included in Briskly commission.</p>
                    </div>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend10-bottom.svg" alt="">
    </div>

    <a name="what" id="what"></a>
    <div class="offers-wrapper">
        <div class="wrapper-block">
            <h2 class="align-center">Our features</h2>
            <div class="offer-blocks-wrapper">
                <div class="offer-block">
                    <h4 class="bold">B-Pay app</h4>
                    <ul class="standart">
                        <li>Shows the nearest kiosk</li>
                        <li>Unlocks the refrigerator</li>
                        <li>Scans goods</li>
                        <li>Payment by card or Apple Pay</li>
                        <li>CRF check is available in the mail or in the application</li>
                    </ul>
                </div>
                <div class="offer-block">
                    <h4 class="bold">Turnkey refrigerator</h4>
                    <ul class="standart">
                        <li>Lock control unit</li>
                        <li>Application and platform connection module</li>
                        <li>MXM refrigerator, Linnafrost or another manufacturer</li>
                        <li>Temperature and humidity sensors</li>
                        <li>Kiosk branding</li>
                    </ul>
                </div>
                <div class="offer-block">
                    <h4 class="bold">Secure sales</h4>
                    <ul class="standart">
                        <li>Camera records all operations with the refrigerator</li>
                        <li>Doubtful operations are displayed in a special section</li>
                        <li>Users are identified by phone and card, optionally by passport</li>
                        <li>In case of fraud, money is debited from the linked card</li>
                    </ul>
                </div>
                <div class="offer-block">
                    <h4 class="bold">CRM, analytics and marketing</h4>
                    <ul class="standart">
                        <li>Product matrix integration with 1C, iiko or manually (.xls, .csv)</li>
                        <li>Loyalty program: discounts, promotions, card binding</li>
                        <li>Sales reports and event logging</li>
                        <li>Calculation of profitability, popularity of goods</li>
                        <li>Suspicious transaction history</li>
                        <li>User Technical Support</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="markets-in-num-wrapper">
        <div class="wrapper-block">
            <h2 class="align-center color-white">Briskly Micromarket in numbers</h2>
            <div class="flex-wrapper about-nums">
                <div class="flex-block">
                    <div class="about-nums-block">
                        <h4 class="bold">Starting investment</h4>
                        <p><strong>Turnkey kit</strong></p>
                        <p>Refrigerator, lock control unit, B-Pay application, surveillance camera and neural network, temperature and humidity sensors, kiosk branding.</p>
                        <p>Choose one of food supplier from our partners.</p>
                        <span class="fs40 bold color-green">$2100</span>
                    </div>
                    <div class="about-nums-block">
                        <h4 class="bold">B-Pay App</h4>
                        <p>Technical support, online checkout</p>
                        <!-- <p>Acquiring, online cashier, user support.</p> -->
                        <span class="fs40 bold color-green">$65 per month</span>
                    </div>
                </div>
                <div class="flex-block">
                    <div class="about-nums-block">
                        <h4 class="bold">Monthly Turnover</h4>
                        <p>Placing a kiosk in the office of a company of 130 people or more</p>
                        <span class="fs40 bold color-green">$1800</span>
                    </div>
                    <div class="about-nums-block">
                        <h4 class="bold">Payback period</h4>
                        <p>Excluding logistics and freight costs</p>
                        <span class="fs40 bold color-green">3 months</span>
                    </div>
                    <div class="about-nums-block">
                        <h4 class="bold">Net profit</h4>
                        <p><strong></strong></p>
                        <p>Average value for one kiosk per month</p>
                        <span class="fs40 bold color-green">from $500</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-block green-dots-decoration align-center color-white have-questions">
            <h2>Have any questions?</h2>
            <span class="s24 middle">Call our manager or receive a presentation in the mail</span>
            <div class="btns-block">
                <a href="tel:+78006005096" class="btn btn-green btn-big btn300 tel">8-800-600-50-96</a>
                <a href="#feedback" class="btn btn-green-border btn-big btn300 to-feedback">Wanna get a presentation?</a>
            </div>
        </div>
    </div>

    <div class="food-suppliers-wrapper">
        <div class="wrapper-block h-block">
            <h2>Do not have your own food production?</h2>
            <p class="s24 middle">Choose a trustworthy supplier from our partners</p>
        </div>
        <div class="full-width-block">
            <div class="dots"></div>
            <img class="absolute-position" src="/img/micro-market/content-images/bag.png" alt="" />
            <div class="swiper-container food-suppliers-carousel js-food-suppliers">
                <div class="swiper-wrapper">
                    <div class="swiper-slide first-elem-2"></div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://kylinarium.ru/" class="logo-block"><img src="/files/suppliers-logos/kulinarium.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Kulinarium</h4>
                                <p class="color-gray middle">Moscow</p>
                            </div>
                            <p>Ready-to-eat food production in Moscow</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="http://st-pk.ru/" class="logo-block"><img src="/files/suppliers-logos/slav-trapeza-new.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Slavianskaya trapeza</h4>
                                <p class="color-gray middle">Moscow</p>
                            </div>
                            <p>Ready-to-eat food production in Moscow</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://www.justfood.pro/" class="logo-block"><img src="/files/suppliers-logos/just-food.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Just Food Pro</h4>
                                <p class="color-gray middle">Moscow</p>
                            </div>
                            <p>Ready-to-eat food production in Moscow</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://www.gyrmanov.com/" class="logo-block"><img src="/files/suppliers-logos/gurmanov.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Hurmanow</h4>
                                <p class="color-gray middle">Moscow</p>
                            </div>
                            <p>Production of catering</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <div class="logo-block"><img src="/files/suppliers-logos/my-fruit.jpg" alt="" /></div>
                            <div class="supplier-name">
                                <h4 class="bold">MyFruit</h4>
                                <p class="color-gray middle">Moscow and St. Petersburg</p>
                            </div>
                            <p>Production of fresh fruit desserts</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://cityfood.pro/" class="logo-block"><img src="/files/suppliers-logos/city-food.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">City food</h4>
                                <p class="color-gray middle">Moscow</p>
                            </div>
                            <p>Production of sandwiches, Salads and sweets</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://uppetit.ru/" class="logo-block"><img src="/files/suppliers-logos/uppetit.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Uppetit</h4>
                                <p class="color-gray middle">St. Petersburg</p>
                            </div>
                            <p>Ready-to-eat food production in St. Petersburg</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://olimpfood.com/" class="logo-block"><img src="/files/suppliers-logos/olimpfood.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">olimpfood</h4>
                                <p class="color-gray middle">Nizhny Novgorod and Krasnoyarsk</p>
                            </div>
                            <p>Ready-to-eat food production in Nizhny Novgorod and Krasnoyarsk</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://naturovo.ru/" class="logo-block"><img src="/files/suppliers-logos/naturovo.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Naturovo</h4>
                                <p class="color-gray middle">Kaliningrad</p>
                            </div>
                            <p>Ready-to-eat food production in Kaliningrad</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="http://gastronomist.pro/" class="logo-block"><img src="/files/suppliers-logos/gastronomist.jpg" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Gastronomist</h4>
                                <p class="color-gray middle">St. Petersburg</p>
                            </div>
                            <p>Ready-to-eat food production in St. Petersburg</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="http://obedov66.ru" class="logo-block"><img src="/files/suppliers-logos/obedov.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">Obedov</h4>
                                <p class="color-gray middle">Ekaterinburg</p>
                            </div>
                            <p>Ready-to-eat food production in Ekaterinburg</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="supplier-card">
                            <a href="https://myfiteat.ru/" class="logo-block"><img src="/files/suppliers-logos/myfiteat.png" alt="" /></a>
                            <div class="supplier-name">
                                <h4 class="bold">My Fit Eat</h4>
                                <p class="color-gray middle">St. Petersburg</p>
                            </div>
                            <p>Ready-to-eat food production in St. Petersburg</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $path . '/us/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom-white.svg" alt="" />
    </div>
    <div class="full-width str-after-wrapper">
        <div class="str-after">
            <div>
                <span class="s24 middle align-center">With Briskly, the future gets closer and moves to the smartphone!</span>
                <img src="/img/str-green.svg" alt="" />
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $path . '/us/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $path . '/us/views/elems/work-steps-b-pay.tpl'; ?>
        <?php require_once $path . '/us/views/elems/about-terminal.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/us/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/us/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/us/views/elems/map2.tpl'; ?>
    <?php require_once $path . '/us/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
