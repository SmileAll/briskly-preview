<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
// $version = '300820-1';
$title = 'Briskly - тарифы';
?>
<?php require_once $path . '/us/views/common/head.tpl'; ?>
<?= chunk('fb_chat') ?>
<div class="container">
    <div id="firstblock"></div>
    <header class="header-dark-color">
        <?php require_once $path . '/us/views/common/header.tpl'; ?>
    </header>
    <div class="top-block-wrapper on-tariffs-page">
        <div class="wrapper-block">
            <span class="s18 semibold">Current tariffs:</span>
            <h3>«135 $ per month»</h3>
            <p class="s14 bold">What's included?</p>
            <p>Self-service express checkout in the buyer's smartphone.</p>
            <p>Integration of assortment of goods.</p>
            <p>Technical customer support.</p>
            <p>Adding promotions and discounts.</p>
            <p>Personal account in Briskly Business Back Office (BBO)</p>
        </div>
    </div>
    <div class="full-width footer-height">
        <img class="bend bend-bottom" src="/img/bend-images/map-bend-bottom.svg" alt="">
        <?php require_once $path . '/us/views/common/footer.tpl'; ?>
    </div>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
