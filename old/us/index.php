<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Briskly - trading platform without staff';
$description = 'A platform for unmanned trading in shops, cafes, gas stations and micromarkets. The author of the B-Pay payment application with the scan-and-buy function.';
$keywords = 'Briskly, scan and buy, checkout in a smartphone, micromarket, scan&go, delivery, b-pay, b-pay app, briskley online, shop without a cashier';
require_once $path . '/us/views/common/head.tpl';
?>
        <header>
            <?php require_once $path . '/us/views/common/header.tpl'; ?>
        </header>
        <div class="main-top-info">
            <div class="logo"><a href="/"><img src="/img/logo-white-new.svg" alt="" /></a></div>
            <div class="main-top-info-text">
                <h1>Technological platform for&nbsp;unmanned trading</h1>
                <ul class="standart marker-abroad">
                    <li>B-Pay app for scanning and&nbsp;paying for goods</li>
                    <li>Technology for autonomous trading</li>
                    <li>Smart refrigerators</li>
                </ul>
                <a href="#feedback" class="btn btn-green btn-big btn300 to-feedback">Contact us</a>
            </div>
        </div>
    </div>
    <div class="full-width light-gray-bg first-bend-block">
        <div class="wrapper-block">
            <a name="form" id="form"></a>
            <h3>What is Briskly?</h3>
            <div class="flex-wrapper">
                <div class="flex-block pr100">
                    <p class="s14">Briskly is the technology of the future at the service of retail. The core of our service is the B-Pay application, thanks to which customers scan product barcodes and pay for them directly on their smartphone. You can buy without a cashier at 11,000 points throughout Russia.</p>
                    <p class="s14" id="feedback">We also produce hardware solutions for the autonomous operation of stores, smart refrigerators and cafes: a module for connecting to a platform and a security system.</p>
                    <div class="form-block white-bg br-20">
                        <p class="s18 semibold align-center">Get your presentation now, <br />decide later</p>
                        <div class="form-wrapper">
                            <?php require_once $path . '/views/common/forms/39.tpl'; ?>
                        </div>
                    </div>
                </div>
                <div class="flex-block pluses-for-you-wrapper">
                    <span class="s24 middle">With the Briskly platform, your store can sell 15% more and work twice as fast. No staff, no queues.</span>
                    <ul class="pluses-for-you">
                        <li>
                            <img src="/img/icons/chel×2-icon.svg" alt="" class="green-shadow" />
                            <p class="s18">Serve twice as many clients in the same time, without expanding staff and retail space.</p>
                        </li>
                        <li>
                            <img src="/img/icons/growth-icon.svg" alt="" class="blue-shadow" />
                            <p class="s18">Increase revenue by 15% in 3 months, without the cost of cashiers, security, work around the clock.</p>
                        </li>
                        <li>
                            <img src="/img/icons/compass-icon.svg" alt="" class="pink-shadow" />
                            <p class="s18">Get a new sales channel. Know what the customer wants and convince him to spend more, faster and in your store.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="" />
    </div>
    <div class="full-width second-bend-block">
        <div class="wrapper-block about-products">
            <h2>Our products</h2>
            <div class="swiper-container js-about-products">
                <div class="swiper-wrapper about-products-wrapper">
                    <div class="swiper-slide about-product-block about-phone-app">
                        <div class="image-wrapper have-icon">
                            <img src="/img/main-page-images/phone-app-new.png" alt="" />
                            <img class="b-pay-icon" src="/img/icon-bpay-new.svg" alt="" />
                        </div>
                        <div class="product-info white-bg br-20 box-sh-gray">
                            <div class="product-info-text">
                                <span class="s18 semibold">B-Pay app</span>
                                <!--<h3>7 000 ₽/мес.</h3>-->
                                <h3>65 $ <span>per month</span></h3>
                                <div class="text-block">
                                    <p class="s14 bold">Features</p>
                                    <p class="s14">Shopping without a cashier in stores.</p>
                                    <p class="s14">Payment for goods in smart refrigerators.</p>
                                    <p class="s14">Delivery from shops and cafes</p>
                                    <p class="s14">Payment for fuel at a gas station without a cashier.</p>
                                </div>
                                <div class="text-block">
                                    <p class="s14 bold">Includes</p>
                                    <p class="s14">Self-service express checkout in the buyer's smartphone.</p>
                                    <p class="s14">History of purchases.</p>
                                    <p class="s14">Integration of assortment of goods.</p>
                                    <p class="s14">Technical customer support.</p>
                                    <p class="s14">Adding promotions and discounts.</p>
                                    <p class="s14">Merchant's personal account.</p>
                                </div>
                            </div>
                            <a href="#feedback" class="btn btn-green btn-big to-feedback">Connect store</a>
                        </div>
                    </div>
                    <div class="swiper-slide about-product-block about-micromarket">
                        <div class="image-wrapper"><img src="/img/micromarketnews.png" alt="" /></div>
                        <div class="product-info white-bg br-20 box-sh-gray">
                            <div class="product-info-text">
                                <span class="s18 semibold">Micromarket Briskly</span>
                                <h3>2100 $</h3>
                                <div class="text-block">
                                    <p class="s14 bold">What's included?</p>
                                    <p class="s14">Refrigerator.</p>
                                    <p class="s14">Briskly module for connecting to the platform.</p>
                                    <p class="s14">Electronic lock.</p>
                                    <p class="s14">Integration into the B-Pay app.</p>
                                    <p class="s14">Micromarket pasting in <span class="nowrap">B-Pay design</span>.</p>
                                </div>
                                <div class="text-block">
                                    <p class="s14"><span class="bold">Included: B-Pay app, - </span>functionality of the selected tariff, 65 $ transactional fee per month.</p>
                                </div>
                                <div class="text-block">
                                    <p class="s14"><span class="bold">If you already have a refrigerator,</span> you can buy the Briskly Module for 1,300 $.</p>
                                </div>
                            </div>
                            <a href="http://briskly.online/briskly-micro-market/#feedback" class="btn btn-green btn-big to-feedback" target="_blank">Learn more</a>
                        </div>
                    </div>
                    <div class="swiper-slide about-product-block about-briskly-go">
                        <div class="image-wrapper">
                            <picture>
                                <source media="(min-width: 320px) and (max-width: 767px)" srcset="/img/main-page-images/briskly-go_small-new.jpg">
                                <img src="/img/main-page-images/briskly-go-new.jpg" alt="" />
                            </picture>
                            <img class="only-mobile" src="/img/main-page-images/briskly-go-tab-new.jpg" alt="" />
                        </div>
                        <div class="product-info white-bg br-20 box-sh-gray">
                            <div class="product-info-text">
                                <span class="s18 semibold">Briskly Go</span>
                                <h3>Оn demand</h3>
                                <div class="text-block">
                                    <p class="s14 bold">What it is?</p>
                                    <p class="s14">Shop without a cashier, no security, 24/7.</p>
                                    <p class="s14">Opens and accepts payment via the B-Pay app. </p>
                                    <p class="s14"></p>
                                </div>
                                <div class="text-block">
                                    <p class="s14 bold">What's included?</p>
                                    <p class="s14">Technical equipment for your turnkey unmanned store.</p>
                                    <p class="s14">Security system, surveillance cameras, sensors and doors.</p>
                                </div>
                                <div class="text-block">
                                    <p class="s14"><span class="bold">B-Pay app - </span>functionality of the selected tariff, 65 $ transactional fee per month.</p>
                                </div>
                            </div>
                            <a href="#feedback" class="btn btn-green btn-big to-feedback">Project application</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width bg-gray briskly-components-wrapper tab-no-wrap">
        <div class="wrapper-block">
            <h2 class="align-center">What does the Briskly platform consist of?</h2>
            <div class="flex-wrapper briskly-components-block">
                <div class="flex-block image-wrapper">
                    <img src="/img/main-page-images/briskly-components-new.svg" alt="" />
                </div>
                <div class="flex-block text-wrapper pl100">
                    <div class="text-block">
                        <span class="s16 bold">B-Pay app</span>
                        <p>Mobile application with a scanning module for purchases without queues, the function of fast delivery and payment of fuel at gas stations without a cashier.</p>
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Store business cabinet</span>
                        <p>A platform for our partners to manage retail outlets.</p>
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Safety</span>
                        <p>A security system that monitors user authorization and the purchase process reports suspicious activity.</p>
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Briskly Module</span>
                        <p>Electronic module for managing the outlet that synchronizes it with the B-Pay app and the platform. Suitable for micromarkets and stores.</p>
                    </div>
                </div>
            </div>
            <div class="str-after">
                <div>
                    <span class="s24 middle align-center">With Briskly, the future gets closer and moves to the smartphone.</span>
                    <img src="/img/str-green.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $path . '/us/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $path . '/us/views/elems/work-steps-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg embedded-marketing">
        <?php require_once $path . '/us/views/elems/embedded-marketing.tpl'; ?>
    </div>
    <div class="full-width">
        <img class="bend" src="/img/bend-images/bend5.svg" alt="" />
        <div class="wrapper-block about-discounts on-main-page">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h2>Pioneered feature: <br />discounts and promotions <br /> at the time of purchase.</h2>
                </div>
                <div class="flex-block w40 align-center">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/main-page-images/discounts-phone_small2.jpg">
                        <img src="/img/main-page-images/discounts-phone-new.png" alt="Приложение B-Pay, касса в смартфоне" />
                    </picture>
                </div>
            </div>
        </div>
        <div class="wrapper-block about-security">
            <div class="flex-wrapper center-items tab-no-wrap">
                <div class="flex-block align-center">
                    <div class="image-wrapper">
                        <img src="/img/main-page-images/camera-new.svg" alt="" />
                    </div>
                </div>
                <div class="flex-block">
                    <h2>Security without protection</h2>
                    <h4 class="semibold">One of the key tasks of contactless shopping is to ensure the protection of our partners' goods.</h4>
                    <p>On the Briskly platform, a customer behavior control system is used, which takes into account many parameters: the validity of the user's payment data, authorization at the point of sale, and actions performed by him. All data, together with a video recording of the session at the point of sale, are saved in the web-backoffice of the store</p>
                    <!-- <p>Thus, we see who makes a purchase in a store or smart refrigerator. In case of suspicious actions - for example, lack of payments during the session, the outlet owner receives an urgent notification.</p> -->
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $path . '/us/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/us/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/us/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/us/views/elems/map2.tpl'; ?>
    <?php require_once $path . '/us/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
</body>
</html>
