<div class="footer dark-bg color-white">
    <div class="wrapper-block">
        <div class="logo">
            <a href="/"><img src="/img/logo-white-new.svg" alt="" " /></a>
        </div>
        <div class="flex-wrapper">
            <div class="footer-block flex-column patent-info">
                <div class="footer-block-text">
                    <p>Sale of goods without a seller</p>
                    <p>Technology patent registered <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020619020&TypeFile=html" target="_blank">2020619020</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020619021&TypeFile=html" target="_blank">2020619021</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618645&TypeFile=html" target="_blank">2020618645</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618646&TypeFile=html" target="_blank">2020618646</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618899&TypeFile=html" target="_blank">2020618899</a>, <a href="https://www1.fips.ru/registers-doc-view/fips_servlet?DB=EVM&DocNumber=2020618422&TypeFile=html" target="_blank">2020618422</a>.</p>
                    <p class="copyright">© Briskly, <?= date('Y') ?></p>
                </div>
                <div class="links-block">
                    <ul>
                        <li><a href="/user_doc" target="_blank">Terms of use</a></li>
                        <li><a href="/wp-content/uploads/2019/08/privacy.pdf" target="_blank">Personal data processing</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-block links-block">
                <ul>
                    <li><a href="https://b-pay.online/" target="_blank">B-PAY app</a></li>
                    <li><a href="//app.briskly.online" target="_blank">Download B-PAY</a></li>
                    <li><a href="/">For outlets</a></li>
                    <li><a href="/us/briskly-for-shops/" target="_blank">Connection of store</a></li>
                    <li><a href="/us/delivery/" target="_blank">Connection of place</a></li>
                    <li><a href="/us/briskly-micro-market/" target="_blank">Smart refrigerator</a></li>
                </ul>
            </div>
            <div class="footer-block links-block">
                <ul>
                    <li><a href="/us/briskly_for_partners/" target="_blank">Referrer Program</a></li>
                    <li><a href="/user_doc" target="_blank">Privacy policy</a></li>
                    <li><a href="https://t.me/Brskl" target="_blank">News of the company</a></li>
                    <li><a href="https://bbo.briskly.online/" target="_blank">Account</a></li>
                </ul>
            </div>
            <div class="footer-block flex-column foot-contacts">
                <div class="socila-networks">
                    <ul>
                        <li><a href="https://t.me/Brskl" target="_blank"><img src="/img/social-networks/telegram-green.svg" alt="" /></a></li>
                        <li><a href="https://vk.com/briskly.online" target="_blank"><img src="/img/social-networks/vk-green.svg" alt="" /></a></li>
                    </ul>
                </div>
                <div class="footer-block-text">
                    <a href="tel:+78006005096" class="tel">8-800-600-50-96</a>
                    <a href="#feedback" class="standart-green request-call to-feedback">Request a call</a>
                    <a href="tel:+442045771195" class="tel">+442045771195</a>
                    <p>Great Britain</p>
                    <a href="tel:+13235405719" class="tel">+13235405719</a>
                    <p>USA</p>
                </div>
                <div class="pay-systems">
                    <ul>
                        <li><img src="/img/pay_systems/visa.svg" alt="visa"/></li>
                        <li><img src="/img/pay_systems/mastercard.svg" alt="mastercard"/></li>
                        <li><img src="/img/pay_systems/mir.svg" alt="mir"/></li>
                        <li><img src="/img/pay_systems/sbp2.svg" alt="sbp"/></li>
                    </ul>
                </div>
            </div>
        </div>
        <p class="color-light-gray">Google Play and the Google Play logo are trademarks of Google Inc. Apple and the Apple logo are registered trademarks of Apple Inc. in the United States and other countries. App Store is a service mark of Apple Inc.</p>
        <a href="#firstblock" class="to-top"></a>
    </div>
    <div class="mobile-bottom-btns">
        <a href="tel:+78006005096" class="tel btn btn-green">8-800-600-50-96</a>
        <a href="#feedback" class="to-mail btn btn-green-border to-feedback">Presentation</a>
    </div>
</div>