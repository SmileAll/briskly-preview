<div class="about-b-pay">
    <div class="wrapper-block">
        <div class="flex-wrapper tab-no-wrap">
            <div class="flex-block">
                <h2>B-Pay, <span class="s31">pay without queues</span></h2>
                <p class="s18 color-light-gray">Application for buying goods from your micromarket</p>
                <div class="btns-block application-stores">
                    <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore"><img src="/img/appstore.svg" alt=""></a>
                    <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="/img/googleplay.svg" alt=""></a>
                </div>
                <div class="text-block">
                    <span class="s24 bold">Payment by card or Apple Pay</span>
                    <p>User downloads the app, entres the phone, binds the card - and that’s it! CRF check is available in the app or by email</p>
                </div>
                <div class="text-block">
                    <span class="s24 bold">Scan module</span>
                    <p>Use your standard barcodes – they will be added to our database taking into account pricing and discounts.</p>
                </div>
                <div class="text-block">
                    <span class="s24 bold">Technical support</span>
                    <p>Help users with the purchase, payment. Analyze complex cases with expired products.</p>
                </div>
            </div>
            <div class="flex-block align-right">
                <div class="phone-image-block have-icon">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/phone-screens/screen-map_small.png">
                        <img src="/img/phone-screens/screen-map.png" alt="" />
                    </picture>
                    <img class="phone-frame" src="/img/phone-frame-green-border.svg" alt="" />
                    <img class="b-pay-icon" src="/img/icon-bpay-new.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
</div>