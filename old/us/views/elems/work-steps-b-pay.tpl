<div class="wrapper-block p85">
    <div class="flex-wrapper center-items">
        <div class="flex-block w30 on-mobile-none">
            <div class="phone-image-block tab-w100">
                <picture>
                    <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/phone-screens/screen-scan-basket_small.png">
                    <img src="/img/phone-screens/screen-scan-basket.png" alt="" />
                </picture>
                <img class="phone-frame" src="/img/phone-frame-green-border.svg" alt="" />
            </div>
        </div>
        <div class="flex-block w70 steps-wrapper">
            <h2>How does B-Pay work?</h2>
            <div class="work-steps-wrapper">
                <div class="work-step-block">
                    <div class="icon-block">
                        <img class="green-shadow" src="/img/icons/restaurant-icon-new.svg" alt="" />
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Select a retail outlet</span>
                        <p>Outlets are shown in a list by geolocation or on a map</p>
                    </div>
                </div>
                <div class="work-step-block">
                    <div class="icon-block">
                        <img class="green-shadow" src="/img/icons/barcode-icon-new.svg" alt="" />
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Add products</span>
                        <p>Scan the product barcode and it will be automatically added to the cart</p>
                    </div>
                </div>
                <div class="work-step-block">
                    <div class="icon-block">
                        <img class="green-shadow" src="/img/icons/payment-icon-new.svg" alt="" />
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Pay</span>
                        <p>Payment by linked card or Apple Pay is available</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>