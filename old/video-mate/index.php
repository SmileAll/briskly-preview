<?php
require $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
$title = 'Briskly | Платформа для торговли без персонала';
$description = 'Платформа для торговли без персонала в магазинах, кафе, на АЗС и в микромаркетах. Автор платежного приложения B-Pay с функцией «сканируй и покупай».';
$keywords = 'Брискли, сканируй и покупай, касса в смартфоне, микромаркет, scan&go, доставка, b-pay, приложение b-pay, брискли онлайн, магазин без кассира';
require_once $path . '/views/common/head.tpl';
?>

<div class="container">
    <div id="firstblock"></div>
    <div class="main-top-block">
        <a name="firstblock"></a>
        <div class="main-top-slider-wrapper">
            <img class="bend" src="/img/bend-images/bend1.svg" alt="" />
            <div class="main-slider-bacs swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><img src="/files/slider-images/slide1.jpg" alt="" /></div>
                </div>
            </div>
            <div class="main-slider-images swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="slide-image-block-wrapper">
                            <div class="wrapper-block">
                                <div class="slide-image-block">
                                    <div class="phone-image-block">
                                        <picture>
                                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/phone-screens/screen1_small.png">
                                            <img src="/img/phone-screens/screen1.png" alt="" />
                                        </picture>
                                        <img class="phone-frame" src="/img/phone-frame-dark.svg" alt="" />
                                    </div>
                                    <span class="caption">Беспилотный магазин с оплатой в приложении</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <header>
            <?php require_once $path . '/views/common/header.tpl'; ?>
        </header>
        <div class="main-top-info">
            <div class="logo"><a href="/"><img src="/img/logo-white-new.svg" alt="" /></a></div>
            <div class="main-top-info-text">
                <h1>Технологическая платформа для торговли без персонала</h1>
                <ul class="standart marker-abroad">
                    <li>Приложение B-Pay для сканирования и&nbsp;оплаты&nbsp;товаров</li>
                    <li>Технология для автономной торговли</li>
                    <li>Умные холодильники</li>
                </ul>
                <a href="#feedback" class="btn btn-green btn-big btn300 to-feedback">Связаться с нами</a>
            </div>
        </div>
    </div>
    <div class="full-width light-gray-bg first-bend-block">
        <div class="wrapper-block">
            <a name="form" id="form"></a>
            <h3>Что такое Briskly?</h3>
            <div class="flex-wrapper">
                <div class="flex-block pr100" id="feedback">
                    <p class="s14">Briskly&nbsp;&mdash; это технологии будущего на&nbsp;службе у&nbsp;ритейла. Ядро нашего сервиса&nbsp;&mdash; приложение B-Pay, благодаря которому покупатели сканируют штрихкоды товаров и&nbsp;оплачивают их&nbsp;прямо в&nbsp;смартфоне. Покупать без кассира можно уже в&nbsp;11&nbsp;000 точках по&nbsp;всей России.</p>
                    <p class="s14">Мы&nbsp;также производим железные решения для автономной работы магазинов, умных холодильников и&nbsp;кафе: модуль для подключения к&nbsp;платформе и&nbsp;систему безопасности.</p>
                    <div class="form-block white-bg br-20">
                        <p class="s18 semibold align-center">Получите презентацию сейчас, решите потом</p>
                        <div class="form-wrapper">
                            <?php require_once $path . '/views/common/forms/19.tpl'; ?>
                        </div>
                    </div>
                </div>
                <div class="flex-block pluses-for-you-wrapper">
                    <span class="s24 middle">С платформой Briskly ваша торговая точка может продавать на&nbsp;15%&nbsp;больше и&nbsp;работать в&nbsp;два раза быстрее. Без&nbsp;персонала, без&nbsp;очередей.</span>
                    <ul class="pluses-for-you">
                        <li>
                            <img src="/img/icons/chel×2-icon.svg" alt="" class="green-shadow" />
                            <p class="s18">Обслуживать <strong>в&nbsp;два раза больше клиентов</strong> за&nbsp;то&nbsp;же время работы, без&nbsp;расширения штата и&nbsp;торговых площадей.</p>
                        </li>
                        <li>
                            <img src="/img/icons/growth-icon.svg" alt="" class="blue-shadow" />
                            <p class="s18"><strong>Увеличить выручку на&nbsp;15% за&nbsp;3&nbsp;месяца</strong>, без&nbsp;расходов на&nbsp;кассиров, охрану, работу в&nbsp;круглосуточном режиме.</p>
                        </li>
                        <li>
                            <img src="/img/icons/compass-icon.svg" alt="" class="pink-shadow" />
                            <p class="s18"><strong>Получить новый канал продаж.</strong> Знать, что хочет покупатель и&nbsp;убеждать его тратить больше, быстрее и&nbsp;именно в&nbsp;вашем магазине.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <img class="bend" src="/img/bend-images/bend2.svg" alt="" />
    </div>
    <div class="full-width second-bend-block">
        <div class="wrapper-block about-products">
            <h2>Наши продукты</h2>
            <div class="swiper-container js-about-products">
                <div class="swiper-wrapper about-products-wrapper">
                    <div class="swiper-slide about-product-block about-phone-app">
                        <div class="image-wrapper have-icon">
                            <img src="/img/main-page-images/phone-app.png" alt="" />
                            <img class="b-pay-icon" src="/img/icon-bpay.svg" alt="" />
                        </div>
                        <div class="product-info white-bg br-20 box-sh-gray">
                            <div class="product-info-text">
                                <span class="s18 semibold">Приложение B-Pay</span>
                                <!--<h3>7 000 ₽/мес.</h3>-->
                                <h3>Бесплатно <span>на 3 мес.</span></h3>
                                <div class="text-block">
                                    <p class="s14 bold">Что может?</p>
                                    <p class="s14">Покупки без кассира в магазинах.</p>
                                    <p class="s14">Оплата товаров в умных холодильниках.</p>
                                    <p class="s14">Доставка из магазинов и кафе за&nbsp;40&nbsp;минут.</p>
                                    <p class="s14">Оплата топлива на АЗС без кассира.</p>
                                </div>
                                <div class="text-block">
                                    <p class="s14 bold">Что включено?</p>
                                    <p class="s14">Касса самообслуживания в смартфоне покупателя.</p>
                                    <p class="s14">Эквайринг.</p>
                                    <p class="s14">Отправка чеков в ОФД.</p>
                                    <p class="s14">Интеграция ассортимента товаров.</p>
                                    <p class="s14">Техническая поддержка покупателей.</p>
                                    <p class="s14">Добавление акций и скидок.</p>
                                    <p class="s14">Личный кабинет мерчанта.</p>
                                </div>
                            </div>
                            <a href="#feedback" class="btn btn-green btn-big to-feedback">Подключить магазин</a>
                        </div>
                    </div>
                    <div class="swiper-slide about-product-block about-micromarket">
                        <div class="image-wrapper"><img src="/img/main-page-images/micromarket.jpg" alt="" /></div>
                        <div class="product-info white-bg br-20 box-sh-gray">
                            <div class="product-info-text">
                                <span class="s18 semibold">Микромаркет Briskly</span>
                                <h3>99 000 ₽</h3>
                                <div class="text-block">
                                    <p class="s14 bold">Что включено?</p>
                                    <p class="s14">Холодильник.</p>
                                    <p class="s14">Модуль Briskly для подключения к платформе.</p>
                                    <p class="s14">Электронный замок.</p>
                                    <p class="s14">Интеграция в приложение B-Pay.</p>
                                    <p class="s14">Оклейка микромаркета в дизайне <span class="nowrap">B-Pay</span>.</p>
                                </div>
                                <div class="text-block">
                                    <p class="s14"><span class="bold">Включено: приложение B-Pay,</span>&nbsp;&mdash; функционал выбранного тарифа, 8%&nbsp;комиссии с&nbsp;транзакций.</p>
                                </div>
                                <div class="text-block">
                                    <p class="s14"><span class="bold">Если у вас уже есть холодильник,</span> вы можете докупить Модуль Briskly за&nbsp;62&nbsp;000 рублей.</p>
                                </div>
                            </div>
                            <a href="http://briskly.online/briskly-micro-market/#feedback" class="btn btn-green btn-big to-feedback" target="_blank">Узнать подробности</a>
                        </div>
                    </div>
                    <div class="swiper-slide about-product-block about-briskly-go">
                        <div class="image-wrapper">
                            <picture>
                                <source media="(min-width: 320px) and (max-width: 767px)" srcset="/img/main-page-images/briskly-go_small.jpg">
                                <img src="/img/main-page-images/briskly-go.jpg" alt="" />
                            </picture>
                            <!--<img src="/img/main-page-images/briskly-go.jpg" alt="" />-->
                            <img class="only-mobile" src="/img/main-page-images/briskly-go-tab.jpg" alt="" />
                        </div>
                        <div class="product-info white-bg br-20 box-sh-gray">
                            <div class="product-info-text">
                                <span class="s18 semibold">Briskly Go</span>
                                <h3>2 000 000 ₽</h3>
                                <div class="text-block">
                                    <p class="s14 bold">Что это такое?</p>
                                    <p class="s14">Магазин без кассира, без охраны, 24/7.</p>
                                    <p class="s14">Открывается и принимает оплату через приложение B-Pay. </p>
                                    <p class="s14"></p>
                                </div>
                                <div class="text-block">
                                    <p class="s14 bold">Что включено?</p>
                                    <p class="s14">Техническое оборудование для&nbsp;вашего беспилотного магазина под&nbsp;ключ.</p>
                                    <p class="s14">Система безопасности, камеры наблюдения, датчики и двери. </p>
                                </div>
                                <div class="text-block">
                                    <p class="s14"><span class="bold">Приложение B-Pay</span>&nbsp;&mdash; функционал выбранного тарифа, 8%&nbsp;комиссии с&nbsp;транзакций.</p>
                                </div>
                            </div>
                            <a href="#feedback" class="btn btn-green btn-big to-feedback">Заявка на проект</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width bg-gray briskly-components-wrapper tab-no-wrap">
        <div class="wrapper-block">
            <h2 class="align-center">Из чего состоит платформа Briskly?</h2>
            <div class="flex-wrapper briskly-components-block">
                <div class="flex-block image-wrapper">
                    <img src="/img/main-page-images/briskly-components.svg" alt="" />
                </div>
                <div class="flex-block text-wrapper pl100">
                    <div class="text-block">
                        <span class="s16 bold">Приложение B-Pay</span>
                        <p>Мобильное приложение с&nbsp;модулем сканирования для покупок без очередей, функцией быстрой доставки и оплаты топлива на АЗС без кассира.</p>
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Бизнес-кабинет магазина</span>
                        <p>Платформа для наших партнёров для управления торговыми точками.</p>
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Безопасность</span>
                        <p>Система безопасности, которая отслеживает авторизацию пользователя и процесс покупки, сообщает о подозрительных действиях.</p>
                    </div>
                    <div class="text-block">
                        <span class="s16 bold">Briskly Модуль</span>
                        <p>Электронный модуль управления торговой точкой, который синхронизирует её с приложением B-Pay и платформой. Подходит для микромаркетов и магазинов. </p>
                    </div>
                </div>
            </div>
            <div class="str-after">
                <div>
                    <span class="s24 middle align-center">С Briskly будущее становится ближе и перемещается в смартфон.</span>
                    <img src="/img/str-green.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg color-white">
        <img class="bend bend-top" src="/img/bend-images/bend4-top.svg" alt="" />
        <?php require_once $path . '/views/elems/main-about-b-pay.tpl'; ?>
        <?php require_once $path . '/views/elems/work-steps-b-pay.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend4-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg embedded-marketing">
        <?php require_once $path . '/views/elems/embedded-marketing.tpl'; ?>
    </div>
    <div class="full-width">
        <img class="bend" src="/img/bend-images/bend5.svg" alt="" />
        <div class="wrapper-block about-discounts on-main-page">
            <div class="flex-wrapper center-items">
                <div class="flex-block w60">
                    <h2>Впервые на&nbsp;рынке: <br />скидки и&nbsp;акции предлагаем <br />в&nbsp;моменте покупки.</h2>
                </div>
                <div class="flex-block w40 align-center">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/main-page-images/discounts-phone_small2.jpg">
                        <img src="/img/main-page-images/discounts-phone.png" alt="Приложение B-Pay, касса в смартфоне" />
                    </picture>
                </div>
            </div>
        </div>
        <div class="wrapper-block about-security">
            <div class="flex-wrapper center-items tab-no-wrap">
                <div class="flex-block align-center">
                    <div class="image-wrapper">
                        <img src="/img/main-page-images/camera.svg" alt="" />
                    </div>
                </div>
                <div class="flex-block">
                    <h2>Безопасность без охраны</h2>
                    <h4 class="semibold">Одна из&nbsp;ключевых задач бесконтактных покупок&nbsp;&mdash; обеспечить защиту товаров наших партнёров.</h4>
                    <p>На&nbsp;платформе Briskly используется система контроля за&nbsp;поведением покупателя, которая учитывает множество параметров: валидность платёжных данных пользователя, авторизацию в&nbsp;торговой точке, совершённые им&nbsp;действия. Все данные вместе с&nbsp;видеозаписью сессии на&nbsp;торговой точке сохраняются в&nbsp;бизнес-кабинете магазина </p>
                    <p>Таким образом, мы&nbsp;видим, кто делает покупку в&nbsp;магазине или умном холодильнике. В&nbsp;случае подозрительных действий&nbsp;&mdash; например, отсутствии оплат во&nbsp;время сессии, владелец точки получает срочное уведомление.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width dark-bg">
        <img class="bend bend-top" src="/img/bend-images/bend6-top.svg" alt="" />
        <?php require_once $path . '/views/elems/have-questions-block.tpl'; ?>
        <img class="bend bend-bottom" src="/img/bend-images/bend6-bottom.svg" alt="" />
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/articles.tpl'; ?>
    </div>
    <div class="full-width light-gray-bg">
        <?php require_once $path . '/views/elems/clients-logos.tpl'; ?>
    </div>
    <?php require_once $path . '/views/common/footer.tpl'; ?>
</div>
<?php require_once $path . '/views/common/foot.tpl'; ?>
  <div id="bvwgt"></div>
  <script src="wgt.js" type="text/javascript"></script>
  <script type="text/javascript">
    var source =
      "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4?_=1";
    bvwgt.init("bvwgt", "bl", source);
  </script>
</body>
</html>