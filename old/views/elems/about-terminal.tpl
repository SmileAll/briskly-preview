<div class="about-terminal wrapper-block">
    <div class="flex-wrapper">
        <div class="flex-block">
            <h2>Оплата без приложения</h2>
            <h4>Альтернативный способ — оплата картой через терминал</h4>
            <p>Устанавливаем планшет с терминалом на киоск</p>
            <p>Пользователь регистрируется в системе один раз и может платить прикладывая карту к встроенному терминалу</p>
            <p>Данный способ сокращает время взаимодействия с холодильником до 30-60 секунд</p>
        </div>
        <div class="flex-block right-position">
            <div class="image-wrapper">
                <img class="main-image" src="/img/main-page-images/food-terminal-new.jpg" alt="" />
                <img class="card-image" src="/img/main-page-images/card.svg" alt="" />
            </div>
        </div>
    </div>
</div>