<div class="mass-media-about-us">
    <div class="h-block wrapper-block">
        <h2>О нас пишут в СМИ</h2>
    </div>
    <div class="swiper-container articles-wrapper js-articles">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://secretmag.ru/selfie/briskly.htm" target="_blank" class="img-wrapper"><img src="/files/mass-media/secretmag.svg" alt="" /></a>
                    <a href="https://secretmag.ru/selfie/briskly.htm" target="_blank" class="media-name">secretmag.ru</a>
                    <p>Как научить россиян покупать еду без кассиров и охранников</p>
                    <a href="https://secretmag.ru/selfie/briskly.htm" target="_blank" class="btn btn-black btn-small">Читать</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://rb.ru/opinion/magazin-buduschego/" target="_blank" class="img-wrapper"><img src="/files/mass-media/rb.svg" alt="" /></a>
                    <a href="https://rb.ru/opinion/magazin-buduschego/" target="_blank" class="media-name">rb.ru</a>
                    <p>Как за год вырасти из прототипа в стартап с оценкой в $10 млн</p>
                    <a href="https://rb.ru/opinion/magazin-buduschego/" target="_blank" class="btn btn-black btn-small">Читать</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://vc.ru/trade/73620-kak-ya-borolsya-s-ocheredyami-v-svoem-produktovom-magazine-v-skolkovo-s-pomoshchyu-tehnologiy" target="_blank" class="img-wrapper"><img src="/files/mass-media/vc.svg" alt="" /></a>
                    <a href="https://vc.ru/trade/73620-kak-ya-borolsya-s-ocheredyami-v-svoem-produktovom-magazine-v-skolkovo-s-pomoshchyu-tehnologiy" target="_blank" class="media-name">vc.ru</a>
                    <p>Как я боролся <br />с очередями в своем <br />продуктовом магазине <br />в “Сколково” с B-Pay</p>
                    <a href="https://vc.ru/trade/73620-kak-ya-borolsya-s-ocheredyami-v-svoem-produktovom-magazine-v-skolkovo-s-pomoshchyu-tehnologiy" target="_blank" class="btn btn-black btn-small">Читать</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://www.retail.ru/articles/kak-rossiyskiy-startap-briskly-zapuskaet-magaziny-bez-personala/" target="_blank" class="img-wrapper">
                        <picture>
                            <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/files/mass-media/retail_small.png">
                            <img src="/files/mass-media/retail.png" alt="" />
                        </picture>
                    </a>
                    <a href="https://www.retail.ru/articles/kak-rossiyskiy-startap-briskly-zapuskaet-magaziny-bez-personala/" target="_blank" class="media-name">retail.ru</a>
                    <p>Как российский стартап Briskly запускает магазины без персонала</p>
                    <a href="https://www.retail.ru/articles/kak-rossiyskiy-startap-briskly-zapuskaet-magaziny-bez-personala/" target="_blank" class="btn btn-black btn-small">Читать</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" target="_blank" class="img-wrapper"><img src="/files/mass-media/retailer.svg" alt="" /></a>
                    <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" target="_blank" class="media-name">retailer.ru</a>
                    <p>Микромаркеты - новый формат торговли готовой едой в офисах</p>
                    <a href="https://retailer.ru/kak-i-zachem-sozdat-svoju-franshizu-umnyh-holodilnikov-5-voprosov-na-kotorye-sleduet-otvetit-pered-startom/" target="_blank" class="btn btn-black btn-small">Читать</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://www.vedomosti.ru/business/articles/2020/02/13/822968-rossiiskie-produktovie-magazini-vvodyat" target="_blank" class="img-wrapper"><img src="/files/mass-media/vedomosti.svg" alt="" /></a>
                    <a href="https://www.vedomosti.ru/business/articles/2020/02/13/822968-rossiiskie-produktovie-magazini-vvodyat" target="_blank" class="media-name">vedomosti.ru</a>
                    <p>Российские продуктовые магазины вводят оплату мимо кассы</p>
                    <a href="https://www.vedomosti.ru/business/articles/2020/02/13/822968-rossiiskie-produktovie-magazini-vvodyat" target="_blank" class="btn btn-black btn-small">Читать</a>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="article-item">
                    <a href="https://www.the-village.ru/village/city/news-city/374417-av-bez-kass" target="_blank" class="img-wrapper"><img src="/files/mass-media/villadge.svg" alt="" /></a>
                    <a href="https://www.the-village.ru/village/city/news-city/374417-av-bez-kass" target="_blank" class="media-name">the-village.ru</a>
                    <p>«Азбука вкуса» тестирует оплату покупок в приложении. Стоять в очередях больше не нужно</p>
                    <a href="https://www.the-village.ru/village/city/news-city/374417-av-bez-kass" target="_blank" class="btn btn-black btn-small">Читать</a>
                </div>
            </div>
        </div>
        <div class="swiper-button-prev"><img src="/img/control.svg" /></div>
        <div class="swiper-button-next"><img src="/img/control.svg" /></div>
    </div>
    <a href="https://about.briskly.online/media/?roistat_visit=246168" class="btn btn-black btn-big center-position" style="margin-top: 20px; padding: 0 30px;" target="_blank">Смотреть все статьи</a>
</div>