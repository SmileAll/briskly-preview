<div class="wrapper-block">
    <div class="spec-text">
        <h2 class="leaf-after">Кабинет владельца торговой точки с встроенным маркетингом</h2>
        <p>В личном кабинете на платформе bbo.briskly.online можно настраивать работу магазина: часы работы, его отображение в приложении, радиус доступности и другие детали. Можно пополнять ассортимент, вводить новые товарные позиции и снимать отчёты по продажам. Смотреть записи с камеры микромаркета и историю взаимодействий с ним.</p>
        <p>Также, благодаря приложению, вы держите связь со своей аудиторией и поэтому - можете стимулировать продажи с помощью скидок, акций и промокодов. </p>
    </div>
</div>
<div class="laptop-bg">
    <div class="spec-flex-wrapper">
        <div class="flex-block">
            <div class="icon-block have-tail"><img class="green-shadow" src="/img/icons/bell-icon.svg" alt="" /></div>
            <div class="text-block">
                <span class="s18 bold">Push-уведомления</span>
                <p>скидки, привязанные к&nbsp;геолокации</p>
            </div>
        </div>
        <div class="flex-block">
            <div class="icon-block"><img class="blue-shadow" src="/img/icons/mail-icon.svg" alt="" /></div>
            <div class="text-block">
                <span class="s18 bold">E-mail и&nbsp;SMS</span>
                <p>для рассылки акций и&nbsp;новостей магазина</p>
            </div>
        </div>
        <div class="flex-block">
            <div class="icon-block"><img class="pink-shadow" src="/img/icons/heart-icon.svg" alt="" /></div>
            <div class="text-block">
                <span class="s18 bold">Любимые товары</span>
                <p>B-Pay запоминает любимые товары и&nbsp;предлагает их&nbsp;в&nbsp;процессе покупок</p>
            </div>
        </div>
        <div class="flex-block">
            <div class="icon-block"><img class="purple-shadow" src="/img/icons/percent-icon.svg" alt="" /></div>
            <div class="text-block">
                <span class="s18 bold">Кэшбек и&nbsp;скидки</span>
                <p>программы лояльности и&nbsp;карты привязаны прямо в&nbsp;приложении</p>
            </div>
        </div>
    </div>
</div>