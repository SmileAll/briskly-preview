<div class="about-b-pay">
    <div class="wrapper-block">
        <div class="flex-wrapper tab-no-wrap">
            <div class="flex-block">
                <h2>Приложение B-Pay <span class="s31">Плати без очередей!</span></h2>
                <p class="s18 color-light-gray">Приложение для покупки любых товаров из&nbsp;вашего магазина или микромаркета за&nbsp;<span class="nowrap">2-3</span>&nbsp;минуты</p>
                <div class="btns-block application-stores">
                    <a href="https://apps.apple.com/ru/app/briskly-pay/id1441121786?mt=8" class="appstore"><img src="/img/appstore.svg" alt=""></a>
                    <a href="https://play.google.com/store/apps/details?id=online.briskly&amp;utm_source=app.link&amp;utm_campaign=organic&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="/img/googleplay.svg" alt=""></a>
                </div>
                <div class="text-block">
                    <span class="s24 bold">Оплата картой или Apple&nbsp;Pay</span>
                    <p>Пользователь скачивает приложение, указывает телефон, привязывает карту&nbsp;&mdash; и&nbsp;всё. Чек ОФД доступен в&nbsp;приложении или&nbsp;на&nbsp;почте.</p>
                </div>
                <div class="text-block">
                    <span class="s24 bold">Модуль сканирования</span>
                    <p>Используйте свои стандартные штрихкоды&nbsp;&mdash; мы&nbsp;внесём их&nbsp;в&nbsp;нашу базу данных с&nbsp;учётом ценообразования и&nbsp;скидок.</p>
                </div>
                <div class="text-block">
                    <span class="s24 bold">Техническая поддержка</span>
                    <p>Мы&nbsp;помогаем пользователям с&nbsp;покупкой, оплатой, разбираем сложные случаи с&nbsp;просроченными продуктами.</p>
                </div>
            </div>
            <div class="flex-block align-right">
                <div class="phone-image-block have-icon">
                    <picture>
                        <source media="(min-width: 320px) and (max-width: 1024px)" srcset="/img/phone-screens/screen-map_small.png">
                        <img src="/img/phone-screens/screen-map.png" alt="" />
                    </picture>
                    <img class="phone-frame" src="/img/phone-frame-green-border.svg" alt="" />
                    <img class="b-pay-icon" src="/img/icon-bpay-new.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
</div>