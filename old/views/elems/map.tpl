<div class="full-width map-block-wrapper js-load-map" id="map">
    <div class="map-block">
        <iframe src="https://briskly.online/map3" width="100%" height="100%"></iframe>
    </div>
    <img class="bend bend-top" src="/img/bend-images/map-bend-top.svg" alt="" />
    <img class="bend bend-bottom" src="/img/bend-images/map-bend-bottom.svg" alt="" />
</div>
