# ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### How to use it web components?

<html>
  <!-- Add vue and axios -->
  <script src="https://unpkg.com/vue"></script>
  <script src="https://unpkg.com/axios"></script>
  <!-- Add script of library -->
  <script src="./components.min.js"></script>

  <!-- Choose to use components of library -->
  <components-share-header logo="logo.svg" classes="is-bordered" url="/header.json"></components-share-header>
  <components-share-footer url="/footer.json"></components-share-footer>
</html>