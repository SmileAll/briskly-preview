import ShareHeader from './components/ShareHeader'
import ShareFooter from './components/ShareFooter'
import ShareSprite from './components/ShareSprite/index'

export {
  ShareHeader,
  ShareFooter,
  ShareSprite
}